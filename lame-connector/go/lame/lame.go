package lame

/*
#cgo LDFLAGS: -L ../../bin -llame
#cgo CFLAGS: -I../../../include
#include <stdio.h>
#include <stdlib.h>
#include "lame.v2.h"
uint32_t LocalScanEnterFile(char* fileName, uint32_t depth, void* userData);
void LocalScanLeaveFile(char* fileName, uint32_t depth, void* userData, int l);
uint32_t LocalScanAlarm(char* fileName, lame_scan_result* info, void* userData);
static lame_scan_feedback newScanFeedback() {
	lame_scan_feedback feedback = {
		.enter_file = (lame_scan_enter_file)&LocalScanEnterFile,
		.leave_file = (lame_scan_leave_file)&LocalScanLeaveFile,
		.alarm = (lame_scan_alarm)&LocalScanAlarm
	};
	return feedback;
}

#include "lame.extract.h"
uint32_t LocalExtractEnterFile(char* fileName, char* format, uint32_t depth, uint64_t FCLS, void* hanlde, void* userData);
void LocalExtractLeaveFile(char* fileName, char* format, uint32_t depth, uint64_t FCLS, void* hanlde, void* userData, int l);
static lame_extract_feedback newExtractFeedback() {
	lame_extract_feedback feedback = {
		.enter_file = (lame_extract_enter_file)&LocalExtractEnterFile,
		.leave_file = (lame_extract_leave_file)&LocalExtractLeaveFile
	};
	return feedback;
}
*/
import "C"
import (
	"sync"
	"sync/atomic"
	"unsafe"
)

const (
	LAME_SEEK_SET	= 0
	LMAE_SEEK_CUT	= 1
	LMAE_SEEK_TAIL	= 2

	LSCT_CONTINUE	= 1
	LSCT_ABORT		= 2

	LCT_CONTINUE	= 1
	LCT_DO_EXTRACT	= 2
	LCT_ABORT		= 3

	LXLVT_NORMAL	= 1
	LXLVT_ABORT 	= 2
	LXLVT_ERROR		= 3
)

type scan_enter_file func(file_name string, depth uint32, usr_data interface{}) uint32
type scan_leave_file func(file_name string, depth uint32, usr_data interface{}, l uint32)
type scan_alarm func(file_name string, info *DetectResult, usr_data interface{}) uint32

type ScanCallBack struct {
	EnterFile scan_enter_file
	LeaveFile scan_leave_file
	Alarm scan_alarm
}

type goScanCallBack struct {
	Callback ScanCallBack
	UserData interface{}
}

var goScanCallbackMap = make(map[int32]interface{})
var goScanCallbackMapLock = sync.RWMutex{}
var goScanKeySeed int32

func NewScanCallback(callback ScanCallBack, userdata interface{}) unsafe.Pointer {
	key := atomic.AddInt32(&goScanKeySeed, 1)
	goScanCallbackMapLock.Lock()
	defer goScanCallbackMapLock.Unlock()
	goScanCallbackMap[key] = &goScanCallBack{Callback: callback, UserData: userdata}
	return unsafe.Pointer(uintptr(key))
}

func DeleteScanCallback(key unsafe.Pointer) {
	goScanCallbackMapLock.Lock()
	defer goScanCallbackMapLock.Unlock()
	delete(goScanCallbackMap, int32(uintptr(key)))
}

//export LocalScanEnterFile
func LocalScanEnterFile(fileName *C.char, depth uint32, userData unsafe.Pointer) uint32 {
	goScanCallbackMapLock.RLock()
	realCallback := goScanCallbackMap[int32(uintptr(userData))].(*goScanCallBack)
	goScanCallbackMapLock.RUnlock()
	return realCallback.Callback.EnterFile(C.GoString(fileName), depth, realCallback.UserData)
}

//export LocalScanLeaveFile
func LocalScanLeaveFile(fileName *C.char, depth uint32, userData unsafe.Pointer, l C.int) {
	goScanCallbackMapLock.RLock()
	realCallback := goScanCallbackMap[int32(uintptr(userData))].(*goScanCallBack)
	goScanCallbackMapLock.RUnlock()
	realCallback.Callback.LeaveFile(C.GoString(fileName), depth, realCallback.UserData, uint32(l))
}

//export LocalScanAlarm
func LocalScanAlarm(fileName *C.char, result *C.struct_lame_scan_result, userData unsafe.Pointer) uint32 {
	goScanCallbackMapLock.RLock()
	realCallback := goScanCallbackMap[int32(uintptr(userData))].(*goScanCallBack)
	goScanCallbackMapLock.RUnlock()
	var dr DetectResult
	dr.convert(result)
	return realCallback.Callback.Alarm(C.GoString(fileName), &dr, realCallback.UserData)
}

type extract_enter_file func(file_name string, format string, depth uint32, FCLS uint64, handle unsafe.Pointer, userdata interface{}) uint32
type extract_leave_file func(file_name string, format string, depth uint32, FCLS uint64, handle unsafe.Pointer, userdata interface{}, l uint32)

type ExtractCallBack struct {
	EnterFile extract_enter_file
	LeaveFile extract_leave_file
}

type goExtractCallBack struct {
	Callback ExtractCallBack
	UserData interface{}
}

var goExtractCallbackMap = make(map[int32]interface{})
var goExtractCallbackMapLock = sync.RWMutex{}
var goExtractKeySeed int32

func NewExtractCallback(callback ExtractCallBack, userdata interface{}) unsafe.Pointer {
	key := atomic.AddInt32(&goExtractKeySeed, 1)
	goExtractCallbackMapLock.Lock()
	defer goExtractCallbackMapLock.Unlock()
	goExtractCallbackMap[key] = &goExtractCallBack{Callback: callback, UserData: userdata}
	return unsafe.Pointer(uintptr(key))
}

func DeleteExtractCallback(key unsafe.Pointer) {
	goExtractCallbackMapLock.Lock()
	defer goExtractCallbackMapLock.Unlock()
	delete(goExtractCallbackMap, int32(uintptr(key)))
}

//export LocalExtractEnterFile
func LocalExtractEnterFile(fileName *C.char, format *C.char, depth C.uint32_t, FCLS C.uint64_t, handle unsafe.Pointer, userData unsafe.Pointer) C.uint32_t {
	goExtractCallbackMapLock.RLock()
	realCallback := goExtractCallbackMap[int32(uintptr(userData))].(*goExtractCallBack)
	goExtractCallbackMapLock.RUnlock()
	return C.uint32_t(realCallback.Callback.EnterFile(C.GoString(fileName), C.GoString(format), uint32(depth), uint64(FCLS), handle, realCallback.UserData))
}

//export LocalExtractLeaveFile
func LocalExtractLeaveFile(fileName *C.char, format *C.char, depth C.uint32_t, FCLS C.uint64_t, handle unsafe.Pointer, userData unsafe.Pointer, l C.int) {
	goExtractCallbackMapLock.RLock()
	realCallback := goExtractCallbackMap[int32(uintptr(userData))].(*goExtractCallBack)
	goExtractCallbackMapLock.RUnlock()
	realCallback.Callback.LeaveFile(C.GoString(fileName), C.GoString(format), uint32(depth), uint64(FCLS), handle, realCallback.UserData, uint32(l))
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

type DetectResult struct {
	Mklass, Vid32, Treat uint32
	Vid40 uint64
	Kclass, Kclass_desc_a, Engid, Vname, Hitag string
	Kclass_desc_w string
}

func (dr *DetectResult) convert(result *C.lame_scan_result) *DetectResult {
	if nil == result {
		return nil
	}
	dr.Mklass = uint32(result.mklass)
	dr.Kclass = C.GoString(&result.kclass[0])
	dr.Kclass_desc_a = C.GoString(&result.kclass_desc_a[0])
	dr.Kclass_desc_w = "" //C.GoString(&result.kclass_desc_w[0])
	dr.Engid = C.GoString(&result.engid[0])
	dr.Vname = C.GoString(&result.vname[0])
	dr.Vid32 = uint32(result.vid32)
	dr.Vid40 = uint64(result.vid40)
	dr.Hitag = C.GoString(&result.hitag[0])
	dr.Treat = uint32(result.treat)
	return dr
}

type LameVersion struct {
	Engv, Vdbv string
}

func (lv *LameVersion) convert(info *C.lame_info) *LameVersion {
	if nil == info {
		return nil
	}
	lv.Engv = C.GoString(&info.engv[0])
	lv.Vdbv = C.GoString(&info.vdbv[0])
	return lv
}

type LicenceInfo struct {
	Version, Owner, Date, Authm, Data string
}

func (li *LicenceInfo) convert(info *C.rx_licence_info) *LicenceInfo {
	if nil == info {
		return nil
	}
	li.Version = C.GoString(&info.Version[0])
	li.Owner = C.GoString(&info.Owner[0])
	li.Date = C.GoString(&info.Date[0])
	li.Authm = C.GoString(&info.Authm[0])
	li.Data = C.GoString(&info.Data[0])
	return li
}

type Lame struct{
	vdb unsafe.Pointer
	engine unsafe.Pointer
}

func (l *Lame) OpenVdb(vlib string) unsafe.Pointer {
	vdb := C.lame_open_vdb(C.CString(vlib))
	l.vdb = vdb
	return vdb
}

func (l *Lame) CloseVdb() {
	if nil == l.vdb {
		return
	}
	C.lame_close_vdb(l.vdb)
}

func (l *Lame) Create(vdb unsafe.Pointer) unsafe.Pointer {
	if nil == vdb {
		return nil
	}
	engine := C.lame_create(vdb)
	if nil == engine {
		return nil
	}
	l.engine = engine
	return engine
}

func (l *Lame) Destory() {
	if nil == l.engine {
		return
	}
	C.lame_destroy(l.engine)
}

func (l *Lame) Init() bool {
	if nil == l.engine {
		return false
	}
	hr := C.lame_init(l.engine)
	if 0 != hr {
		return false
	}
	return true
}

func (l *Lame) SetParam(param string) bool {
	if nil == l.engine {
		return false
	}
	cs_param := C.CString(param)
	defer C.free(unsafe.Pointer(cs_param))
	return 0 != C.lame_param_set(l.engine, cs_param)
}

func (l *Lame) ScanFile(path string) *DetectResult {
	var result C.lame_scan_result
	if nil == l.engine {
		return nil
	}
	cs_path := C.CString(path)
	defer C.free(unsafe.Pointer(cs_path))
	hr := C.lame_scan_file(l.engine, cs_path, &result)
	if 0 == hr {
		var dr DetectResult
		return dr.convert(&result)
	}
	return nil
}

func (l *Lame) ScanMem(data [] byte, size int) *DetectResult {
	var result C.lame_scan_result
	if nil == l.engine {
		return nil
	}
	hr := C.lame_scan_mem(l.engine, (*C.uchar)(unsafe.Pointer(&data[0])), C.uint(size), &result)
	if 0 == hr {
		var dr DetectResult
		return dr.convert(&result)
	}
	return nil
}

func (l *Lame) ScanFileWithCallback(path string, cb ScanCallBack, userdata interface{}) bool {
	if nil == l.engine {
		return false
	}
	feedback := C.newScanFeedback()
	gocallback := NewScanCallback(cb, userdata)
	defer DeleteScanCallback(gocallback)
	cs_path := C.CString(path)
	defer C.free(unsafe.Pointer(cs_path))
	return 0 == C.lame_scan_file_with_callback(l.engine, cs_path, &feedback, gocallback)
}

func (l *Lame) ScanMemWithCallback(data [] byte, size int, cb ScanCallBack, userdata interface{}) bool {
	if nil == l.engine {
		return false
	}
	feedback := C.newScanFeedback()
	gocallback := NewScanCallback(cb, userdata)
	defer DeleteScanCallback(gocallback)
	return 0 == C.lame_scan_mem_with_callback(l.engine, (*C.uchar)(unsafe.Pointer(&data[0])), C.uint(size), &feedback, gocallback)
}

func (l *Lame) GetVersion() *LameVersion {
	var info C.lame_info
	hr := C.lame_get_version(&info)
	if 0 == hr {
		var lv LameVersion
		return lv.convert(&info)
	}
	return nil
}

func (l *Lame) GetVersionEx(vlibf string) *LameVersion {
	var info C.lame_info
	cs_vlibf := C.CString(vlibf)
	defer C.free(unsafe.Pointer(cs_vlibf))
	hr := C.lame_get_version_ex(cs_vlibf, &info)
	if 0 == hr {
		var lv LameVersion
		return lv.convert(&info)
	}
	return nil
}

func (l *Lame) GetLicenceInfo() *LicenceInfo {
	var info C.rx_licence_info
	hr := C.lame_get_licence_info(&info)
	if 0 == hr {
		var li LicenceInfo
		return li.convert(&info)
	}
	return nil
}

func (l *Lame) GetLicenceInfoEx(licence string) *LicenceInfo {
	var info C.rx_licence_info
	cs_licence := C.CString(licence)
	defer C.free(unsafe.Pointer(cs_licence))
	hr := C.lame_get_licence_info_ex(cs_licence,&info)
	if 0 == hr {
		var li LicenceInfo
		return li.convert(&info)
	}
	return nil
}

func (l *Lame) ExtractFile(filename string, password string, cb ExtractCallBack, userdata interface{}) bool {
	if nil == l.engine {
		return false
	}
	feedback := C.newExtractFeedback()
	gocallback := NewExtractCallback(cb, userdata)
	defer DeleteExtractCallback(gocallback)
	cs_filename := C.CString(filename)
	defer C.free(unsafe.Pointer(cs_filename))
	cs_password := C.CString(password)
	defer C.free(unsafe.Pointer(cs_password))
	return 0 == C.lame_extract_file(l.engine, cs_filename, cs_password, &feedback, gocallback)
}

func (l *Lame) ExtractGetSize(handle unsafe.Pointer) int {
	if nil == l.engine || nil == handle {
		return 0
	}
	return int(C.lame_file_get_size(handle))
}

func (l *Lame) ExtractSeek(handle unsafe.Pointer, offset int, method int32) bool {
	if nil == l.engine || nil == handle {
		return false
	}
	return 0 == C.lame_file_seek(handle, C.int(offset), C.LSMETHOD(method))
}

func (l *Lame) ExtractTell(handle unsafe.Pointer) int32 {
	if nil == l.engine || nil == handle {
		return -1
	}
	return int32(C.lame_file_tell(handle))
}

func (l *Lame) ExtractRead(handle unsafe.Pointer, buf [] byte, size uint32) uint32 {
	if nil == l.engine || nil == handle {
		return 0
	}
	return uint32(C.lame_file_read(handle, (*C.uchar)(unsafe.Pointer((&buf[0]))), C.uint(size)))
}
