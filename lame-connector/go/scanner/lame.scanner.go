package scanner

import (
	"errors"
	"fmt"
	"io/ioutil"
	"lame"
	"os"
	"unsafe"
)

type Scanner struct {
	_lame *lame.Lame
	paramlist []string
}

func (s *Scanner) Init() bool {
	s._lame = new(lame.Lame)
	return true
}

func (s *Scanner) ShowVersion(vlbf string) {
	if nil == s._lame {
		return
	}
	ver := s._lame.GetVersionEx(vlbf)
	if nil != ver {
		fmt.Printf("engine version: %v\n", ver.Engv)
		fmt.Printf("virusdb version: %v\n", ver.Vdbv)
	} else {
		fmt.Println("GetLameInfo failed.")
	}
}

func (s *Scanner) ShowLicence(licence string) {
	if nil == s._lame {
		return
	}
	info := s._lame.GetLicenceInfoEx(licence)
	if nil != info {
		fmt.Printf("Version: %v\n", info.Version)
		fmt.Printf("Owner: %v\n", info.Owner)
		fmt.Printf("Date: %v\n", info.Date)
		fmt.Printf("Authm: %v\n", info.Authm)
		fmt.Printf("Data: %v\n", info.Data)
	} else {
		fmt.Println("GetLicenceInfo failed.")
	}
}

func (s *Scanner) Load(vlibf string) (bool, error) {
	vdb := s._lame.OpenVdb(vlibf)
	if nil == vdb {
		return false, errors.New("Open viruslib failed.")
	}
	engine := s._lame.Create(vdb)
	if nil == engine {
		s.Unload()
		return false, errors.New("create engine failed.")
	}
	for i := 0; i < len(s.paramlist); i++ {
		s._lame.SetParam(s.paramlist[i])
	}
	if !s._lame.Init() {
		s.Unload()
		return false, errors.New("Init engine failed.")

	}
	return true, errors.New("")
}

func (s *Scanner) SetParameters(param string) {
	s.paramlist = append(s.paramlist, param)
}

func (s *Scanner) Unload() {
	s._lame.CloseVdb()
	s._lame.Destory()
}

func (s *Scanner) ScanFile(path string) *lame.DetectResult {
	if nil == s._lame {
		return nil
	}
	return s._lame.ScanFile(path)
}

func (s *Scanner) ScanFileWithCallback(path string, cb lame.ScanCallBack, userdata interface{}) bool {
	if nil == s._lame {
		return false
	}
	return s._lame.ScanFileWithCallback(path, cb, userdata)
}

func (s *Scanner) ScanMem(data []byte, size int) *lame.DetectResult {
	if nil != s._lame && nil != data && size > 0 {
		return s._lame.ScanMem(data, size)
	}
	return nil
}

func (s *Scanner) ScanMemWithCallback(data []byte, size int, cb lame.ScanCallBack, userdata interface{}) bool {
	if nil != s._lame && nil != data && size > 0 {
		return s._lame.ScanMemWithCallback(data, size, cb, userdata)
	}
	return false
}

func (s *Scanner) ReadPos(_lame *lame.Lame, handle unsafe.Pointer, data []byte, pos int, size uint32) uint32 {
	if !_lame.ExtractSeek(handle, pos, lame.LAME_SEEK_SET) {
		return 0
	}
	return _lame.ExtractRead(handle, data, size)
}

func (s *Scanner) DumpFile(handle unsafe.Pointer, filename string) bool {
	var size uint32 = uint32(s._lame.ExtractGetSize(handle))
	const block = 0x1000
	data := make([]byte, block)
	var offset uint32 = 0
	var bytes uint32

	fd, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return false
	}
	defer fd.Close()

	for ; size>0; {
		if block < size {
			bytes = block
		} else {
			bytes = uint32(size)
		}
		if bytes != s.ReadPos(s._lame, handle, data[:], int(offset), bytes) {
			return false
		}
		fd.Write(data)
		offset += bytes
		size -= bytes
	}
	return true
}

func (s *Scanner) ReadFile(path string) ([]byte, error) {
	fh, ferr := os.Open(path)
	if ferr != nil {
		fmt.Printf("An error occurred on opening the inputfile\n" +
			"Does the file exist?\n" +
			"Have you got acces to it?\n")
		return nil, ferr
	}
	defer fh.Close()
	return ioutil.ReadAll(fh)
}

func (s *Scanner) ExtractFile(filename string, password string, cb lame.ExtractCallBack, userdata interface{}) bool {
	if nil == s._lame {
		return false
	}
	return s._lame.ExtractFile(filename, password, cb, userdata)
}

func (s *Scanner) ExtGetSize(handle unsafe.Pointer) int {
	return s._lame.ExtractGetSize(handle)
}

func (s *Scanner) ExtSeek(handle unsafe.Pointer, offset int, method int32) bool {
	return s._lame.ExtractSeek(handle, offset, method)
}

func (s *Scanner) ExtTell(handle unsafe.Pointer) int32 {
	return s._lame.ExtractTell(handle)
}

func (s *Scanner) ExtRead(handle unsafe.Pointer, buf []byte, size uint32) uint32 {
	return s._lame.ExtractRead(handle, buf, size)
}
