* [工程说明](#工程说明)
  * [lame](#lame)
  * [scanner](#scanner)
  * [sample](#sample)


# 工程说明
___

- **lame**

   本工程是C转go的接口。

- **scanner**

   本工程是go封装的接口，利用lame.go封装的go接口封装成Lame类供用户使用。

- **sample**

   本工程是测试用例代码，利用lame.go封装的go接口，使用了两种方式扫描文件，一种是直接扫描一种是使用回调函数扫描。



## lame
___

Lame：扫描文件接口类。

```go
func (l *Lame) OpenVdb(vlib string) unsafe.Pointer
func (l *Lame) CloseVdb()
func (l *Lame) Create(vdb unsafe.Pointer) unsafe.Pointer
func (l *Lame) Destory()
func (l *Lame) Init() bool
func (l *Lame) SetParam(param string) bool
func (l *Lame) ScanFile(path string) *DetectResult
func (l *Lame) ScanMem(data [] byte, size int) *DetectResult
func (l *Lame) ScanFileWithCallback(path string, cb ScanCallBack, userdata interface{}) bool
func (l *Lame) ScanMemWithCallback(data [] byte, size int, cb ScanCallBack, userdata interface{}) bool
func (l *Lame) GetVersion() *LameVersion
func (l *Lame) GetVersionEx(vlibf string) *LameVersion
func (l *Lame) GetLicenceInfo() *LicenceInfo
func (l *Lame) GetLicenceInfoEx(licence string) *LicenceInfo
func (l *Lame) ExtractFile(filename string, password string, cb ExtractCallBack, userdata interface{}) bool
func (l *Lame) ExtractGetSize(handle unsafe.Pointer) int
func (l *Lame) ExtractSeek(handle unsafe.Pointer, offset int, method int32) bool
func (l *Lame) ExtractTell(handle unsafe.Pointer) int32
func (l *Lame) ExtractRead(handle unsafe.Pointer, buf [] byte, size uint32) uint32
```

## scanner
___

Scanner：扫描器类

```go
func (s *Scanner) Init() bool
func (s *Scanner) ShowVersion(vlbf string)
func (s *Scanner) ShowLicence(licence string)
func (s *Scanner) Load(vlibf string) (bool, error)
func (s *Scanner) SetParameters(param string)
func (s *Scanner) Unload()
func (s *Scanner) ScanFile(path string) *lame.DetectResult
func (s *Scanner) ScanFileWithCallback(path string, cb lame.ScanCallBack, userdata interface{}) bool
func (s *Scanner) ScanMem(data [] byte, size int) *lame.DetectResult
func (s *Scanner) ScanMemWithCallback(data [] byte, size int, cb lame.ScanCallBack, userdata interface{}) bool
func (s *Scanner) ReadPos(_lame *lame.Lame, handle unsafe.Pointer, data [] byte, pos int, size uint32) uint32
func (s *Scanner) DumpFile(handle unsafe.Pointer, filename string) bool
func (s *Scanner) ReadFile(path string)( []byte, error)
func (s *Scanner) ExtractFile(filename string, password string, cb lame.ExtractCallBack, userdata interface{}) bool
func (s *Scanner) ExtGetSize(handle unsafe.Pointer) int
func (s *Scanner) ExtSeek(handle unsafe.Pointer, offset int, method int32) bool
func (s *Scanner) ExtTell(handle unsafe.Pointer) int32
func (s *Scanner) ExtRead(handle unsafe.Pointer, buf [] byte, size uint32) uint32
```

## sample
___


扫描器示例

```go
func scan_enter_file(file_name string, depth uint32, usr_data interface{}) uint32 {
	println(file_name, usr_data.(string))
	return lame.LSCT_CONTINUE
}

func scan_leave_file(file_name string, depth uint32, usr_data interface{}, l uint32) {
	println(file_name, usr_data.(string))
}

func scan_alarm(file_name string, info *lame.DetectResult, usr_data interface{}) uint32 {
	println(file_name, info.Vname, usr_data.(string))
	return lame.LSCT_CONTINUE
}

func Mkdir(path string) {
	_, err := os.Stat(path)
	if err != nil {
		os.Mkdir(path, os.ModePerm)
	}
}

func AssertPath(path string, filename string) {
	strings.Replace(filename, "\\", "/", -1)
	Mkdir(path)
	slips := strings.Split(filename, "/")
	for i := 0; i < len(slips)-1; i++ {
		path += "/" + slips[i]
		Mkdir(path)
	}
}

func extract_enter_file(file_name string, format string, depth uint32, FCLS uint64, handle unsafe.Pointer, usr_data interface{}) uint32 {
	if 0 != depth {
		sample := usr_data.(*Sample)
		AssertPath(sample.outdir, file_name)
		path := sample.outdir + "/" + file_name
		sample.scan.DumpFile(handle, path)
	}
	println(file_name, format)
	return lame.LCT_DO_EXTRACT
}

func extract_leave_file(file_name string, format string, depth uint32, FCLS uint64, handle unsafe.Pointer, usr_data interface{}, l uint32) {
	println(file_name, format)
}

type Sample struct {
	scan       *scanner.Scanner
	usecallbak bool
	outdir     string
}

func (s *Sample) Init(vibf string, licence string, usecallbak bool) {
	s.scan = new(scanner.Scanner)
	s.usecallbak = usecallbak

	s.scan.Init()
	s.scan.ShowVersion(vibf)
	s.scan.ShowLicence(licence)
	s.scan.SetParameters("precise-format=0")
	if runtime.GOOS == "windows" {
		s.scan.SetParameters("license-path=" + licence)
	} else {
		s.scan.SetParameters("license-path=#" + licence)
	}
	s.scan.Load(vibf)
}

func (s *Sample) Uninit() {
	s.scan.Unload()
}

func (s *Sample) ScanFile(path string) {
	if s.usecallbak {
		scb := lame.ScanCallBack{scan_enter_file, scan_leave_file, scan_alarm}
		s.scan.ScanFileWithCallback(path, scb, "scanfile_callback")
	} else {
		dr := s.scan.ScanFile(path)
		if dr != nil {
			println(path, dr.Vname, dr.Engid)
		} else {
			println(path)
		}
	}
}

func (s *Sample) ScanMem(path string) {
	data, _ := s.scan.ReadFile(path)
	if s.usecallbak {
		mcb := lame.ScanCallBack{scan_enter_file, scan_leave_file, scan_alarm}
		s.scan.ScanMemWithCallback(data, len(data), mcb, "scanmem_callback")
	} else {
		dr := s.scan.ScanMem(data, len(data))
		if dr != nil {
			println(dr.Vname, dr.Engid)
		}
	}
}

func (s *Sample) ExtractFile(path string, outdir string) {
	ecb := lame.ExtractCallBack{extract_enter_file, extract_leave_file}
	s.outdir = outdir
	s.scan.ExtractFile(path, "", ecb, s)
}

const (
	SCAN_TYPE_FILE   = 1
	SCAN_TYPE_MEMORY = 2
)

func (s *Sample) ScanDir(path string, scantype int) {
	err := filepath.Walk(path, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		if SCAN_TYPE_FILE == scantype {
			s.ScanFile(path)
		} else if SCAN_TYPE_MEMORY == scantype {
			s.ScanMem(path)
		}
		return nil
	})
	if err != nil {
		fmt.Printf("filepath.Walk() returned %v\n", err)
	}
}

func Help() {
	println("invalidate arguments. please input again")
	println("e.g.:")
	println("\tscan file or directory:")
	println("\t\t-s: go run lame.sample.go [-s] [vlib path] [licence path] [path]")
	println("\tscan file path or directory by pass memory:")
	println("\t\t-m: go run lame.sample.go [-m] [vlib path] [licence path] [path]")
	println("\textract file:")
	println("\t\t-e: go run lame.sample.go [-e] [vlib path] [licence path] [path] [outdir]")
}

func Detect() {
	if 5 > len(os.Args) {
		Help()
		return
	}

	switch os.Args[1] {
	case "-s":
		{
			sample := new(Sample)
			sample.Init(os.Args[2], os.Args[3], true)
			sample.ScanDir(os.Args[4], SCAN_TYPE_FILE)
			sample.Uninit()
		}
		break
	case "-m":
		{
			sample := new(Sample)
			sample.Init(os.Args[2], os.Args[3], true)
			sample.ScanDir(os.Args[4], SCAN_TYPE_MEMORY)
			sample.Uninit()
		}
		break
	case "-e":
		{
			sample := new(Sample)
			sample.Init(os.Args[2], os.Args[3], true)
			sample.ExtractFile(os.Args[4], os.Args[3])
			sample.Uninit()
		}
		break
	}
}

func main() {
	println("-------- start -------")
	Detect()
	println("-------- end -------")
}
```
