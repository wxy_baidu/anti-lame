package main

import (
	"fmt"
	"lame"
	"os"
	"path/filepath"
	"runtime"
	"scanner"
	"strings"
	"unsafe"
)

func scan_enter_file(file_name string, depth uint32, usr_data interface{}) uint32 {
	println(file_name, usr_data.(string))
	return lame.LSCT_CONTINUE
}

func scan_leave_file(file_name string, depth uint32, usr_data interface{}, l uint32) {
	println(file_name, usr_data.(string))
}

func scan_alarm(file_name string, info *lame.DetectResult, usr_data interface{}) uint32 {
	println(file_name, info.Vname, usr_data.(string))
	return lame.LSCT_CONTINUE
}

func Mkdir(path string) {
	_, err := os.Stat(path)
	if err != nil {
		os.Mkdir(path, os.ModePerm)
	}
}

func AssertPath(path string, filename string) {
	strings.Replace(filename, "\\", "/", -1)
	Mkdir(path)
	slips := strings.Split(filename, "/")
	for i := 0; i < len(slips)-1; i++ {
		path += "/" + slips[i]
		Mkdir(path)
	}
}

func extract_enter_file(file_name string, format string, depth uint32, FCLS uint64, handle unsafe.Pointer, usr_data interface{}) uint32 {
	if 0 != depth {
		sample := usr_data.(*Sample)
		AssertPath(sample.outdir, file_name)
		path := sample.outdir + "/" + file_name
		sample.scan.DumpFile(handle, path)
	}
	println(file_name, format)
	return lame.LCT_DO_EXTRACT
}

func extract_leave_file(file_name string, format string, depth uint32, FCLS uint64, handle unsafe.Pointer, usr_data interface{}, l uint32) {
	println(file_name, format)
}

type Sample struct {
	scan       *scanner.Scanner
	usecallbak bool
	outdir     string
}

func (s *Sample) Init(vibf string, licence string, usecallbak bool) {
	s.scan = new(scanner.Scanner)
	s.usecallbak = usecallbak

	s.scan.Init()
	s.scan.ShowVersion(vibf)
	s.scan.ShowLicence(licence)
	s.scan.SetParameters("precise-format=0")
	if runtime.GOOS == "windows" {
		s.scan.SetParameters("license-path=" + licence)
	} else {
		s.scan.SetParameters("license-path=#" + licence)
	}
	s.scan.Load(vibf)
}

func (s *Sample) Uninit() {
	s.scan.Unload()
}

func (s *Sample) ScanFile(path string) {
	if s.usecallbak {
		scb := lame.ScanCallBack{scan_enter_file, scan_leave_file, scan_alarm}
		s.scan.ScanFileWithCallback(path, scb, "scanfile_callback")
	} else {
		dr := s.scan.ScanFile(path)
		if dr != nil {
			println(path, dr.Vname, dr.Engid)
		} else {
			println(path)
		}
	}
}

func (s *Sample) ScanMem(path string) {
	data, _ := s.scan.ReadFile(path)
	if s.usecallbak {
		mcb := lame.ScanCallBack{scan_enter_file, scan_leave_file, scan_alarm}
		s.scan.ScanMemWithCallback(data, len(data), mcb, "scanmem_callback")
	} else {
		dr := s.scan.ScanMem(data, len(data))
		if dr != nil {
			println(dr.Vname, dr.Engid)
		}
	}
}

func (s *Sample) ExtractFile(path string, outdir string) {
	ecb := lame.ExtractCallBack{extract_enter_file, extract_leave_file}
	s.outdir = outdir
	s.scan.ExtractFile(path, "", ecb, s)
}

const (
	SCAN_TYPE_FILE   = 1
	SCAN_TYPE_MEMORY = 2
)

func (s *Sample) ScanDir(path string, scantype int) {
	err := filepath.Walk(path, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		if SCAN_TYPE_FILE == scantype {
			s.ScanFile(path)
		} else if SCAN_TYPE_MEMORY == scantype {
			s.ScanMem(path)
		}
		return nil
	})
	if err != nil {
		fmt.Printf("filepath.Walk() returned %v\n", err)
	}
}

func Help() {
	println("invalidate arguments. please input again")
	println("e.g.:")
	println("\tscan file or directory:")
	println("\t\t-s: go run lame.sample.go [-s] [vlib path] [licence path] [path]")
	println("\tscan file path or directory by pass memory:")
	println("\t\t-m: go run lame.sample.go [-m] [vlib path] [licence path] [path]")
	println("\textract file:")
	println("\t\t-e: go run lame.sample.go [-e] [vlib path] [licence path] [path] [outdir]")
}

func Detect() {
	if 5 > len(os.Args) {
		Help()
		return
	}

	switch os.Args[1] {
	case "-s":
		{
			sample := new(Sample)
			sample.Init(os.Args[2], os.Args[3], true)
			sample.ScanDir(os.Args[4], SCAN_TYPE_FILE)
			sample.Uninit()
		}
		break
	case "-m":
		{
			sample := new(Sample)
			sample.Init(os.Args[2], os.Args[3], true)
			sample.ScanDir(os.Args[4], SCAN_TYPE_MEMORY)
			sample.Uninit()
		}
		break
	case "-e":
		{
			sample := new(Sample)
			sample.Init(os.Args[2], os.Args[3], true)
			sample.ExtractFile(os.Args[4], os.Args[3])
			sample.Uninit()
		}
		break
	}
}

func main() {
	println("-------- start -------")
	Detect()
	println("-------- end -------")
}