package com.rising.engine.scanner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.rising.engine.lame.*;

public class Scan extends Lame implements ScanCallback, ExtractCallback {

	public void detectFile(String filename) {
		setConfig("precise-format=0");
		long vdb = openVirusLib(null);
		if(0 == vdb) {
			return;
		}
		if(!loadEngine(vdb)){
			unloadEngine();
			return;
		}
		ScanResult sr = scanFile(filename);
		if(null != sr){
			System.out.println(filename + "\t" + sr.vname + "!" + sr.vid32);
			releaseObject(sr);
		} else {
			System.out.println(filename);
		}
		unloadEngine();
	}
	public void printVersion(String vlibf) {
		LameInfo lv = getLameVersion(vlibf);
		if(null != lv){
			System.out.println("sdk version info");
			System.out.println("engine: " + lv.engine_version);
			System.out.println("malware: " + lv.virus_db_version);
			releaseObject(lv);
		}
	}
	public void printLicence(String licence) {
		LicenceInfo li = getLicenceInfo(licence);
		if(null != li){
			System.out.println("licence info");
			System.out.println("version: " + li.Version);
			System.out.println("Owner: " + li.Owner);
			System.out.println("Date: " + li.Date);
			System.out.println("Authm: " + li.Authm);
			System.out.println("Data: " + li.Data);
			releaseObject(li);
		}
	}
	//callbcak
	public long enterScFile(String file_name, long depth, Object usr_data) {
		System.out.println("[callback] ++ " + file_name + " && " + depth + " && " + usr_data);
		return 0;
	}
	public void leaveScFile(String file_name, long depth, Object usr_data, long l) {
		System.out.println("[callback] -- " + file_name + " && " + depth + " && " + usr_data + " && " + l);	
	}
	public long alarm(String file_name, ScanResult sr, Object usr_data) {
		System.out.println("[callback] ** " + file_name + " && " + sr.vname + "!" + sr.vid32 + " && " + usr_data);
		return 1;
	}
	public boolean judgeFileExist(String filename) {
		File file = new File(filename);
		if (file.exists()) {
			file.delete();
		}
		try {
			file.createNewFile();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	public void dumpFile(long handle, String filename) {
		OutputStream os = null;
		try {
			int size = (int)getFileSize(handle);
			int offset = 0;
			if (!judgeFileExist(filename)) {
				return;
			}
			os = new FileOutputStream(filename, true);
			while (size > 0) {
				if(seekFile(handle,offset,0)) {
					int bytesRead = 0x1000 < size ? 0x1000 : size;
					byte [] buffer = new byte [(int)bytesRead];
					if (bytesRead != readFile(handle, buffer, size)) {
						return;
					}
					os.write(buffer);
					offset += bytesRead;
					size -= bytesRead;	
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				os.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public long enterExtFile(String file_name, String format, long depth, long FCLS, long handle, Object usr_data) {
		System.out.println("[callback] ++ " + file_name + " && " + format + " && " + depth + " && " + FCLS + " && " + handle + " && " + usr_data);
		if(depth >= 1) {
			String filename = System.getProperty("user.dir") + "/" + file_name;
			dumpFile(handle, filename);	
		}
		return 1;
	}
	public void leaveExtFile(String file_name, String format, long depth, long FCLS, long handle, Object usr_data, long l) {
		System.out.println("[callback] -- " + file_name + " && " + format + " && " + depth + " && " + FCLS + " && "  + handle + " && " + usr_data + " && " + l);
	}
	public void dectectFile(String path, boolean usecb) {
		if(usecb) {
			scanFile(path, this, "__test_scan_callback__");
		}
		else {
			ScanResult sr = scanFile(path);
			if(null != sr) {
				System.out.println("[normal] " + path + "\tInfected: " + sr.vname);
			} else {
				System.out.println("[normal] " + path);
			}
		}
	}
	public void travelPath(String path, boolean useCallback) {
		File or = new File(path);
		if(!or.exists()) return;
		
		if(or.isDirectory()) {
			File [] files = or.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						dectectFile(file.getAbsolutePath(), useCallback);
					} else if (file.isDirectory()) {
						travelPath(file.getAbsolutePath(), useCallback);
					}
				}
			}
		} else if(or.isFile()) {
			dectectFile(or.getAbsolutePath(), useCallback);
		}
	}
	void detect(String vlibf, String licence, String fdPath, boolean useCallback) {
		File file = new File(fdPath);
		if(!file.exists()) return;
		long vdb = openVirusLib(vlibf);
		if(0 == vdb) return;
		setLicencePath(licence);
		if(!loadEngine(vdb)) return;
		travelPath(fdPath, useCallback);
		unloadEngine();
	}
	void extract(String vlibf, String licence, String fdPath) {
		long vdb = openVirusLib(vlibf);
		if(0 == vdb) return;
		if(!loadEngine(vdb)) return;
		setLicencePath(licence);
		extractFile(fdPath, null, this, "__test_extract_callback__");
		unloadEngine();
	}
	boolean setLicencePath(String path) {
		String osName = System.getProperties().getProperty("os.name");
		if(osName.equals("Linux")) {
			return setConfig("license-path=#" + path);
		} else {
			return setConfig("license-path=" + path);
		}
	}

	public static void main(String[] args) {
		System.out.println("------------ main start ----------------------");

		String vlibf = args[0]; //virus library path
		String licence = args[1]; // licence.json path
		String fdPath = args[2]; // target path
		Boolean flag = Boolean.parseBoolean(args[3]); //use callback ?
		
		Scan scan = new Scan();
		scan.detect(vlibf, licence, fdPath, flag);
		scan.extract(vlibf, licence, fdPath);

		System.out.println("------------ main end ----------------------");
	}
}