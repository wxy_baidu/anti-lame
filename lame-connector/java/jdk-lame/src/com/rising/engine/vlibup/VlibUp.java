package com.rising.engine.vlibup;

public class VlibUp extends RavEngine {

	private long upper = 0;
	public long UP_FAILED = -1;
	public long UP_SUCCEED = 0;
	public long UP_LASTEST = 1;

	public boolean createUpper() {
		upper = create();
		return 0 == upper ? false : true;
	}
	public long initUpper(String home, String localcfg, DownloadCallback cb, Object usrdata) {
		if(0 == upper) return UP_FAILED;
		long r = init(upper, home, localcfg, cb, usrdata);
		return r >=0 ? (r == 0 ? UP_SUCCEED : UP_LASTEST) : UP_FAILED;
	}
	public boolean update(String wanthome) {
		if(0 == upper) return false;
		return 0 == update(upper, wanthome);
	}
	public void releaseUpper() {
		if(0 != upper) release(upper);
	}
	public boolean getCfgFile(String localxml, DownloadCallback cb, Object usrdata) {
		return 0 == getCfg(localxml, cb, usrdata);
	}
}