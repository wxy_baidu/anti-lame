package com.rising.engine.vlibup;

public interface DownloadCallback {
	public abstract long download(String url, Object usrdata);
}
