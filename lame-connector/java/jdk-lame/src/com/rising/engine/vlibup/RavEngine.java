package com.rising.engine.vlibup;

public class RavEngine {

	protected native long create();
	protected native long getCfg(String localxml, DownloadCallback cb, Object usrdata);
	protected native long init(long upper, String home, String localcfg, DownloadCallback cb, Object usrdata);
	protected native long update(long upper, String wanthome);
	protected native void release(long upper);
		
	static {
		System.loadLibrary("jlame");
	}
}
