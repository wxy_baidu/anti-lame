package com.rising.engine.lame;

import com.rising.engine.lame.ExtractCallback;
import com.rising.engine.lame.LameInfo;
import com.rising.engine.lame.LicenceInfo;
import com.rising.engine.lame.ScanCallback;
import com.rising.engine.lame.ScanResult;

public class RavEngine {

	protected native long openVdb(String vlibf);
	protected native void closeVdb(long vdb);

	protected native long create(long vdb);
	protected native void destroy(long lame);
	protected native long fork(long lame);

	protected native long setParameter(long lame, String param);
	protected native long init(long lame);

	protected native ScanResult scanFile(long lame, String fname);
	protected native long scanFileWithCallback(long lame, String fname, ScanCallback cb, Object usrdata);
	protected native ScanResult scanMemory(long lame, byte [] data);
	protected native long scanMemoryWithCallback(long lame, byte [] data, ScanCallback cb, Object usrdata);

	protected native LameInfo getVersion();
	protected native LicenceInfo getLicence();

	protected native void releaseObject(Object object);
	
	protected native long extractFile(long lame, String fname, String password, ExtractCallback cb, Object usrdata);
	protected native long getSize(long handle);
	protected native long seek(long handle, long offset, long method);
	protected native long tell(long handle);
	protected native long read(long handle, byte [] buf, long size);

	static {
		System.loadLibrary("jlame");
	}
}
