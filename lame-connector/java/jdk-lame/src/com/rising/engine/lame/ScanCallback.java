package com.rising.engine.lame;

public interface ScanCallback {
	public abstract long enterScFile(String file_name, long depth, Object usr_data);
	public abstract void leaveScFile(String file_name, long depth, Object usr_data, long l);
	public abstract long alarm(String file_name, ScanResult sr, Object usr_data);
}
