package com.rising.engine.lame;

import java.util.ArrayList;
import java.util.List;

public class Lame extends RavEngine {

	private List<String> parameters = new ArrayList<String>();
	private long engine = 0;
	private long vdb_handle = 0;

	public long openVirusLib(String vdbf) {
		return openVdb(vdbf);
	}
	public void closeVirusLib() {
		if(0 != vdb_handle) {
			closeVdb(vdb_handle);
			vdb_handle = 0;
		}
	}
	public boolean load(long lib) {
		if(0 == lib) {
			lib = openVirusLib(null);
		}
		engine = create(lib);
		if(0 == engine) return false;
		for(String str:parameters) {
			setParameter(engine, str);
		}
		if(0 > init(engine)) {
			destroy(engine);
			return false;
		}
		vdb_handle = lib;
		return true;
	}
	public boolean setConfig(String param) {
		if(null == param) return false;
		parameters.add(param);
		return true;
	}
	public void destoryEngine() {
		if(0 != engine) {
			destroy(engine);
			engine = 0;
		}
	}
	public void unload() {
		closeVirusLib();
		destoryEngine();
	}
	public LicenceInfo getLicenceInfo() {
		LicenceInfo li = getLicence();
		if(null == li) {
			return null;
		} else {
			LicenceInfo info = new LicenceInfo(li);
			releaseObject(li);
			return info;
		}
	}
	public LameInfo getLameVersion() {
		LameInfo li = getVersion();
		if(null == li) {
			return null;
		} else {
			LameInfo info = new LameInfo(li);
			releaseObject(li);
			return info;	
		}
	}
	public ScanResult scanFile(String fname) {
		if(0 == engine || null == fname) {
			return null;
		}
		ScanResult result = scanFile(engine, fname);
		if(null == result) {
			return null;
		} else {
			ScanResult sr = new ScanResult(result);
			releaseObject(result);
			return sr;
		}
	}
	public ScanResult scanMemory(byte [] data) {
		if(0 == engine || null == data) {
			return null;
		}
		ScanResult result = scanMemory(engine, data);
		if(null == result) {
			return null;
		} else {
			ScanResult sr = new ScanResult(result);
			releaseObject(result);
			return sr;
		}
	}
	public boolean scanFile(String fname, ScanCallback cb, Object usrdata) {
		if(null == fname) return false;
		return 0 == scanFileWithCallback(engine, fname, cb, usrdata);
	}
	public boolean ScanMemory(byte [] data, ScanCallback cb, Object usrdata) {
		if(null == data) return false;
		return 0 == scanMemoryWithCallback(engine, data, cb, usrdata);
	}
	public boolean extractFile(String fname, String passwd, ExtractCallback cb, Object usrdata) {
		if(null == fname) return false;
		return 0 == extractFile(engine, fname, passwd, cb, usrdata);
	}
	public long getFileSize(long handle) {
		if(0 == handle) return 0;
		return getSize(handle);
	}
	public boolean seekFile(long handle, long offset, long method) {
		if(0 == handle) return false;
		return 0 == seek(handle, offset, method);
	}
	public long tellFile(long handle) {
		if(0 == handle) return -1;
		return tell(handle);
	}
	public long readFile(long handle, byte[] buf, long size) {
		if(0 == handle || null == buf || 0 >= size) return 0;
		return read(handle, buf, size);
	}
}
