package com.rising.engine.lame;

public interface ExtractCallback {
	public abstract long enterExtFile(String file_name, String format, long depth, long FCLS, long handle, Object usrdata);
	public abstract void leaveExtFile(String file_name, String format, long depth, long FCLS, long handle, Object usrdata, long l);
}
