package com.rising.engine.clame;

public interface FetchCallback {
	public abstract long deal(long index, byte[] result, Object usrdata);
}
