package com.rising.engine.clame;

public class RavEngine {

	protected native long fetchBatch(long flags, String [] fnames, FetchCallback cb, Object usrdata);
	protected native byte [] cloudSignatureFile(String jsoninfo, String [] fnames);
	
	static {
		System.loadLibrary("jlame");
	}
}
