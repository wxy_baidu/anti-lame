package com.rising.engine.clame;

public class Clame extends RavEngine {

	public long F_MD5 = 1;
	public long F_SHA1 = 2;
	public long F_TFE = 4;
	public long F_RDM = 8;
	public long F_ASG = 16;

	public long fetch(long flags, String [] fnames, FetchCallback cb, Object usrdata) {
		return fetchBatch(flags, fnames, cb, usrdata);
	}
	public byte [] calcSignature(String jsoninfo, String [] fnames) {
		return cloudSignatureFile(jsoninfo, fnames);
	}
}

