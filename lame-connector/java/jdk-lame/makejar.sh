#! /bin/bash

if [ ! -d "bin" ]; then
	mkdir -p bin
fi

javac -d bin src/engine/*.java
javac -d bin src/clame/*.java
javac -d bin src/vlibup/*.java

cd bin
jar -cf lame.jar engine/*.class clame/*.class vlibup/*.class
jar umf ../src/MANIFEST.MF lame.jar

cd ..
cp -f bin/lame.jar ../lame.mt.scanner/lib/
cp -f bin/lame.jar ../lame.scanner/lib/
cp -f bin/lame.jar ../lame.update/lib/
