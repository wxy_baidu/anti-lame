package com.rising.engine.tool;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UpContext {

	public List<String> list = new ArrayList<String>();
	public String wantname;
	public boolean flag;

	UpContext(String want) {
		File dir = new File(want);
		if(!dir.exists()) {
			dir.mkdir();
		}
		wantname = want;
	}

	public void push(String s) {
		list.add(s);
	}

	public String top() {
		if(list.isEmpty()) return "";
		return list.get(list.size()-1);
	}
}
