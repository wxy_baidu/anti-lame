package com.rising.engine.tool;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import com.rising.engine.vlibup.*;


public class Up extends VlibUp implements DownloadCallback {

	public long download(String url, Object usrdata) {
		UpContext ctx = (UpContext) usrdata;

		String file_path = ctx.wantname + "/" + url.substring(url.lastIndexOf('/')+1);
		ctx.flag = false;
		ctx.push(file_path);

		System.out.println(url + "  >>  " + file_path);

		if(!new File(file_path).exists()) {
			try {
				InputStream in = new URL(url).openStream();
				Files.copy(in, Paths.get(file_path), StandardCopyOption.REPLACE_EXISTING);
				ctx.flag = true;
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
		return 0;
	}

	public void Update(String home, String localxml, Object usrdata) {
		do {
			UpContext ctx = (UpContext) usrdata;

			if(!getCfgFile(localxml, this, usrdata)) {
				System.out.println("get viruslib cfg file failed.");
				return;
			}
			if(!createUpper()) {
				System.out.println("Create viruslib upper failed.");
				return;
			}
			long r = initUpper(home, ctx.top(), this, usrdata);
			if(UP_FAILED == r) {
				System.out.println("viruslib upper init failed.");
				break;
			} else if(UP_LASTEST == r) {
				System.out.println("viruslib is lastest.");
				break;
			}
			if(!update(ctx.wantname)) {
				System.out.println("viruslib upper update failed.");
				break;
			}
			System.out.println("viruslib upper update successfully.");
		} while(false);

		releaseUpper();
		Clean(usrdata);
	}

	public void Clean(Object usrdata) {
		UpContext ctx = (UpContext) usrdata;
		for(String path:ctx.list) {
			new File(path).delete();
		}
		new File(ctx.wantname).delete();
	}

	public static void main(String[] args) {
		System.out.println("-------- start ----------");
		new Up().Update("./", "vlcover.xml", new UpContext("xxxx"));
		System.out.println("-------- end ----------");
	}

}
