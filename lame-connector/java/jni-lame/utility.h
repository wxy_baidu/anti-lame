#ifndef __JAVA_LAME_COMMON_H__
#define __JAVA_LAME_COMMON_H__

class Jni_auto_release_string
{
private:
	JNIEnv*			env;
	jstring			str;
	const char*		buffer;
public:
	Jni_auto_release_string():env(0), str(0), buffer(NULL){}
	Jni_auto_release_string(JNIEnv* e, jstring s):env(e),str(s),buffer(NULL){
		Init(env, str);
	}
	~Jni_auto_release_string(){ if(buffer) env->ReleaseStringChars(str, (const jchar*)buffer); }
	void Init(JNIEnv* e, jstring s){ if(env && str) buffer = env->GetStringUTFChars(str,NULL); }
	operator const char* () { return buffer; }
};

template<class T>
class Jni_auto_release_object
{
private:
	JNIEnv*			env;
	T				jobj;
public:
	Jni_auto_release_object(JNIEnv* e, T o):env(e),jobj(o){}
	~Jni_auto_release_object(){ if(jobj) env->DeleteLocalRef(jobj); }
	operator T() const { return jobj; }
	T detach() { T p = jobj; jobj = NULL; return p; }
};

template <class T>
class Jni_auto_release_array
{
private:
	JNIEnv*			env;
	jbyteArray		jay;
	T*				buffer;
public:
	Jni_auto_release_array(JNIEnv* e, jbyteArray a):env(e),jay(a),buffer(NULL){
		if(env && jay) env->GetByteArrayElements(jay, NULL);
	}
	~Jni_auto_release_array(){ if(buffer) env->ReleaseByteArrayElements(jay,(jbyte*)buffer,JNI_FALSE); }
	operator T* () { return buffer; }
};

class Jni_auto_release_object_array
{
private:
	JNIEnv*			env;
	jobjectArray	jay;
	long			length;
	jstring*		array_list;
	char**			buffer_list;
public:
	Jni_auto_release_object_array(JNIEnv* e, jobjectArray a):env(e),jay(a),length(0),array_list(NULL),buffer_list(NULL){
		if(!env || !jay) return;

		length = env->GetArrayLength(jay);
		if(length <= 0) return;

		array_list = new jstring[length];
		if(!array_list) return;

		buffer_list = new char*[length];
		if(!buffer_list) return;

		for (long i=0; i<length; i++)
		{
			array_list[i] = (jstring)env->GetObjectArrayElement(jay, i);
			buffer_list[i] = (char*)env->GetStringUTFChars(array_list[i],NULL);
		}
	}
	~Jni_auto_release_object_array(){
		if(!length) return;
		if(buffer_list){
			for (long i=0; i<length; i++){
				env->ReleaseStringChars(array_list[i], (const jchar*)buffer_list[i]);
				env->DeleteLocalRef(array_list[i]);
			}
			delete [] buffer_list; buffer_list = NULL;
		}
		if(array_list) delete [] array_list; array_list = NULL;
	}
	operator const char** () const { return (const char**) buffer_list; }
};


class Jni_auto_release_callback_info
{
public:
	Jni_auto_release_callback_info(JNIEnv * e, jobject obj, jobject cb, jobject data):env(e),objBase(0),objInterface(0),usr_data(0){
		if(env && obj && cb && data){
			objBase = env->NewLocalRef(obj);
			objInterface = env->NewLocalRef(cb);
			usr_data = env->NewLocalRef(data);
		}
	}
	~Jni_auto_release_callback_info(){
		if(!env) return;
		if(objBase) env->DeleteLocalRef(objBase);
		if(objInterface) env->DeleteLocalRef(objInterface);
		if(usr_data) env->DeleteLocalRef(usr_data);
	}
public:
	JNIEnv*			env;
	jobject			objInterface;
	jobject			objBase;
	jobject			usr_data;
};


class Jni_auto_release_vm {
public:
	Jni_auto_release_vm(JNIEnv* e):env(e),pVm(NULL){
		env->GetJavaVM(&pVm);
		if(NULL != pVm)	pVm->AttachCurrentThread((void**)&env,NULL);
	}
	~Jni_auto_release_vm(){ if(pVm) pVm->DetachCurrentThread(); }
	JavaVM* ptr() const { return pVm; }
private:
	JNIEnv* env;
	JavaVM* pVm;
};

template<class T>
class Jni_auto_release_c_memory {
public:
	Jni_auto_release_c_memory(T* p=NULL):_Ptr(p){}
	operator T* () const { return _Ptr; }
	virtual ~Jni_auto_release_c_memory(){ if(_Ptr) delete [] _Ptr; _Ptr = NULL; }
private:
	T* _Ptr;
};

#endif //__JAVA_LAME_COMMON_H__