/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_rising_engine_clame_RavEngine */

#ifndef _Included_com_rising_engine_clame_RavEngine
#define _Included_com_rising_engine_clame_RavEngine
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_rising_engine_clame_RavEngine
 * Method:    fetchBatch
 * Signature: (J[Ljava/lang/String;Lcom/rising/engine/clame/FetchCallback;Ljava/lang/Object;)J
 */
JNIEXPORT jlong JNICALL Java_com_rising_engine_clame_RavEngine_fetchBatch
  (JNIEnv *, jobject, jlong, jobjectArray, jobject, jobject);

/*
 * Class:     com_rising_engine_clame_RavEngine
 * Method:    cloudSignatureFile
 * Signature: (Ljava/lang/String;[Ljava/lang/String;)[B
 */
JNIEXPORT jbyteArray JNICALL Java_com_rising_engine_clame_RavEngine_cloudSignatureFile
  (JNIEnv *, jobject, jstring, jobjectArray);

#ifdef __cplusplus
}
#endif
#endif
