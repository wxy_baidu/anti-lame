#include "stdafx.h"


JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameOpenVdb
	(JNIEnv * env, jobject jobj, jstring vlibf)
{
	Jni_auto_release_string jar_vlibf(env,vlibf);
	return (jlong)lame_open_vdb(jar_vlibf);
}

JNIEXPORT void JNICALL Java_engine_RavEngine_LameCloseVdb
	(JNIEnv * env, jobject jobj, jlong vdb)
{
	lame_close_vdb((void*)vdb);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameCreate
	(JNIEnv * env, jobject jobj, jlong vdb)
{
	if(0 == vdb) return 0;
	return (jlong)lame_create((void*)vdb);
}

JNIEXPORT void JNICALL Java_engine_RavEngine_LameDestroy
	(JNIEnv * env, jobject jobj, jlong lame)
{
	if(0 != lame) lame_destroy((void*)lame);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFork
	(JNIEnv * env, jobject jobj, jlong lame)
{
	if(0 == lame) return 0;
	return (jlong)lame_fork((void*)lame);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameParamSet
	(JNIEnv * env, jobject jobj, jlong lame, jstring param)
{
	if(0 == lame || NULL == param) return E_INVALIDARG;
	Jni_auto_release_string jar_param(env,param);
	return lame_param_set((void*)lame,jar_param);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameInit
	(JNIEnv * env, jobject jobj, jlong lame)
{
	if(0 == lame) return E_INVALIDARG;
	return lame_init((void*)lame);
}

static jobject local_lame_set_scanresult(JNIEnv * env, jclass objectcls, jobject obj, lame_scan_result * result)
{
	if(NULL != objectcls && NULL != obj && NULL != result)
	{
		jfieldID engid			= env->GetFieldID(objectcls,"engid","Ljava/lang/String;");
		jfieldID vname			= env->GetFieldID(objectcls,"vname","Ljava/lang/String;");
		jfieldID hitag			= env->GetFieldID(objectcls,"hitag","Ljava/lang/String;");
		jfieldID kclass			= env->GetFieldID(objectcls,"kclass","Ljava/lang/String;");
		jfieldID kclass_desc_a	= env->GetFieldID(objectcls,"kclass_desc_a","Ljava/lang/String;");
		jfieldID kclass_desc_w	= env->GetFieldID(objectcls,"kclass_desc_w","Ljava/lang/String;");
		jfieldID mklass			= env->GetFieldID(objectcls,"mklass","I");
		jfieldID vid32			= env->GetFieldID(objectcls,"vid32","I");
		jfieldID treat			= env->GetFieldID(objectcls,"treat","I");

		env->SetObjectField(obj,engid,env->NewStringUTF(result->engid));
		env->SetObjectField(obj,vname,env->NewStringUTF(result->vname));
		env->SetObjectField(obj,hitag,env->NewStringUTF(result->hitag));
		env->SetObjectField(obj,kclass,env->NewStringUTF(result->kclass));
		env->SetObjectField(obj,kclass_desc_a,env->NewStringUTF(result->kclass_desc_a));
		env->SetObjectField(obj,kclass_desc_w,env->NewString((jchar*)result->kclass_desc_w,wcslen(result->kclass_desc_w)));
		env->SetIntField(obj,mklass,result->mklass);
		env->SetIntField(obj,vid32,result->vid32);
		env->SetIntField(obj,treat,result->treat);
	}
	return obj;
}

JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameScanFile
	(JNIEnv * env, jobject jobj, jlong lame, jstring fname)
{
	if(0 == lame || NULL == fname) return NULL;
	Jni_auto_release_string jar_fname(env,fname);
	lame_scan_result result;
	if(S_OK == lame_scan_file((void*)lame,jar_fname,&result))
	{
		jclass objectcls = env->FindClass("engine/ScanResult");
		jobject obj = env->AllocObject(objectcls);
		return local_lame_set_scanresult(env, objectcls, obj, &result);
	}
	return NULL;
}

static LSCT LAMESTDCALLTYPE local_lame_scan_enter_file(const char* file_name, uint32_t depth, void* usr_data)
{
	Jni_auto_release_callback_info* cbinfo = (Jni_auto_release_callback_info*)usr_data;
	if(NULL == cbinfo) return LSCT_CONTINUE;
	JNIEnv* env = cbinfo->env;
	if(NULL == env) return LSCT_CONTINUE;
	Jni_auto_release_vm JVM(env);
	if(NULL == JVM.ptr()) return LSCT_CONTINUE;

	jobject obj_ScanFeedback = cbinfo->objInterface;
	if(NULL == obj_ScanFeedback) return LSCT_CONTINUE;
	jclass cls_ScanFeedback = env->GetObjectClass(obj_ScanFeedback);
	if(NULL == cls_ScanFeedback) return LSCT_CONTINUE;
	jmethodID mid_DtEnterFile = env->GetMethodID(cls_ScanFeedback,"DtEnterFile","(Ljava/lang/String;JLjava/lang/Object;)J");
	if(NULL == mid_DtEnterFile) return LSCT_CONTINUE;
	Jni_auto_release_object<jstring> obj_filename(env, env->NewStringUTF(file_name));
	return (LSCT)env->CallLongMethod(obj_ScanFeedback,mid_DtEnterFile,(jstring)obj_filename,(jlong)depth,cbinfo->usr_data);
}

static void LAMESTDCALLTYPE local_lame_scan_leave_file(const char* file_name, uint32_t depth, void* usr_data , LXLVT l)
{
	Jni_auto_release_callback_info* cbinfo = (Jni_auto_release_callback_info*)usr_data;
	if(NULL == cbinfo) return;
	JNIEnv* env = cbinfo->env;
	if(NULL == env) return;
	Jni_auto_release_vm JVM(env);
	if(NULL == JVM.ptr()) return;

	jobject obj_ScanFeedback = cbinfo->objInterface;
	if(NULL == obj_ScanFeedback) return;
	jclass cls_ScanFeedback = env->GetObjectClass(obj_ScanFeedback);
	if(NULL == cls_ScanFeedback) return;
	jmethodID mid_DtLeaveFile = env->GetMethodID(cls_ScanFeedback,"DtLeaveFile","(Ljava/lang/String;JLjava/lang/Object;J)V");
	if(NULL == mid_DtLeaveFile) return;
	Jni_auto_release_object<jstring> obj_filename(env, env->NewStringUTF(file_name));
	env->CallVoidMethod(obj_ScanFeedback,mid_DtLeaveFile,(jstring)obj_filename,(jlong)depth,cbinfo->usr_data,(jlong)l);
}

static LSCT LAMESTDCALLTYPE local_lame_scan_alarm(const char* file_name, lame_scan_result* result, void* usr_data)
{
	Jni_auto_release_callback_info* cbinfo = (Jni_auto_release_callback_info*)usr_data;
	if (!cbinfo) return LSCT_ABORT;
	JNIEnv* env = cbinfo->env;
	if(NULL == env) return LSCT_CONTINUE;
	Jni_auto_release_vm JVM(env);
	if(NULL == JVM.ptr()) return LSCT_CONTINUE;

	jobject obj_ScanFeedback = cbinfo->objInterface;
	if(NULL == obj_ScanFeedback) return LSCT_CONTINUE;
	jclass cls_ScanFeedback = env->GetObjectClass(obj_ScanFeedback);
	if(NULL == cls_ScanFeedback) return LSCT_CONTINUE;
	jmethodID mid_DtAlarm = env->GetMethodID(cls_ScanFeedback,"DtAlarm","(Ljava/lang/String;Lengine/ScanResult;Ljava/lang/Object;)J");
	if(NULL == mid_DtAlarm) return LSCT_CONTINUE;
	Jni_auto_release_object<jstring> obj_filename(env, env->NewStringUTF(file_name));
	jclass cls_ScanResult = env->FindClass("engine/ScanResult");
	if(NULL == cls_ScanResult) return LSCT_CONTINUE;
	jmethodID mid_ScanResult = env->GetMethodID(cls_ScanResult, "<init>", "()V");
	Jni_auto_release_object<jobject> obj_scanresult(env, env->NewObject(cls_ScanResult, mid_ScanResult));
	if(NULL == obj_scanresult) return LSCT_CONTINUE;
	local_lame_set_scanresult(env,cls_ScanResult,obj_scanresult,result);
	return (LSCT)env->CallLongMethod(obj_ScanFeedback,mid_DtAlarm,(jstring)obj_filename,(jobject)obj_scanresult,cbinfo->usr_data);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameScanFileWithCallback
	(JNIEnv * env, jobject jobj, jlong lame, jstring fname, jobject cb, jobject usr_data)
{
	if(0 == lame || NULL == fname || NULL == cb) return E_INVALIDARG;
	Jni_auto_release_string jar_fname(env,fname);

	Jni_auto_release_callback_info cbInfo(env,jobj,cb,usr_data);

	lame_scan_feedback feedback = {local_lame_scan_enter_file,local_lame_scan_leave_file,local_lame_scan_alarm};
	HRESULT hr = lame_scan_file_with_callback((void*)lame,jar_fname,&feedback,&cbInfo);

	env->DeleteLocalRef(cb);
	env->DeleteLocalRef(jobj);
	env->DeleteLocalRef(usr_data);
	return hr;
}

JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameScanMem
	(JNIEnv * env, jobject jobj, jlong lame, jbyteArray data)
{
	if(0 == lame || NULL == data) return NULL;
	jsize length = env->GetArrayLength(data);
	Jni_auto_release_array<uint8_t> jay(env, data);
	if( 0 == jay || 0 == length ) return NULL;
	lame_scan_result result;
	if(S_OK == lame_scan_mem((void*)lame, jay, length, &result))
	{
		jclass objectcls = env->FindClass("engine/ScanResult");
		jobject obj = env->AllocObject(objectcls);
		return local_lame_set_scanresult(env,objectcls,obj,&result);
	}
	return NULL;
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameScanMemWithCallback
	(JNIEnv * env, jobject jobj, jlong lame, jbyteArray data, jobject cb, jobject usr_data)
{
	if(0 == lame || NULL == data || NULL == cb) return E_INVALIDARG;
	jsize length = env->GetArrayLength(data);
	Jni_auto_release_array<uint8_t> jay(env, data);
	if( 0 == jay || 0 == length ) return E_INVALIDARG;

	Jni_auto_release_callback_info cbInfo(env,jobj,cb,usr_data);
	lame_scan_feedback feedback = {local_lame_scan_enter_file,local_lame_scan_leave_file,local_lame_scan_alarm};
	return lame_scan_mem_with_callback((void*)lame,jay,length,&feedback,&cbInfo);
}

JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameGetVersion
	(JNIEnv * env, jobject jobj)
{
	lame_info info = {};
	jobject obj = NULL;
	if(S_OK == lame_get_version(&info))
	{
		jclass objectcls = env->FindClass("engine/LameInfo");
		if(objectcls)
		{
			obj = env->AllocObject(objectcls);
			jfieldID engine_version		= env->GetFieldID(objectcls,"engine_version","Ljava/lang/String;");
			jfieldID virus_db_version	= env->GetFieldID(objectcls,"virus_db_version","Ljava/lang/String;");

			env->SetObjectField(obj,engine_version,env->NewStringUTF(info.engv));
			env->SetObjectField(obj,virus_db_version,env->NewStringUTF(info.vdbv));
		}
	}
	return obj;
}

JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameGetLicenceInfo
	(JNIEnv * env, jobject jobj)
{
	rx_licence_info info = {};
	jobject obj = NULL;
	if(S_OK == lame_get_licence_info(&info))
	{
		jclass objectcls = env->FindClass("engine/LicenceInfo");
		if(objectcls)
		{
			obj = env->AllocObject(objectcls);
			jfieldID Version	= env->GetFieldID(objectcls,"Version","Ljava/lang/String;");
			jfieldID Owner		= env->GetFieldID(objectcls,"Owner","Ljava/lang/String;");
			jfieldID Date		= env->GetFieldID(objectcls,"Date","Ljava/lang/String;");
			jfieldID Authm		= env->GetFieldID(objectcls,"Authm","Ljava/lang/String;");
			jfieldID Data		= env->GetFieldID(objectcls,"Data","Ljava/lang/String;");

			env->SetObjectField(obj,Version,env->NewStringUTF(info.Version));
			env->SetObjectField(obj,Owner,env->NewStringUTF(info.Owner));
			env->SetObjectField(obj,Date,env->NewStringUTF(info.Date));
			env->SetObjectField(obj,Authm,env->NewStringUTF(info.Authm));
			env->SetObjectField(obj,Data,env->NewStringUTF(info.Data));
		}
	}
	return obj;
}

JNIEXPORT void JNICALL Java_engine_RavEngine_LameScanResultRelease
	(JNIEnv * env, jobject jobj, jobject obj)
{
	if(NULL == obj) return;
	env->DeleteLocalRef(obj);
}

JNIEXPORT void JNICALL Java_engine_RavEngine_LameInfoRelease
	(JNIEnv * env, jobject jobj, jobject obj)
{
	if(NULL == obj) return;
	env->DeleteLocalRef(obj);
}

JNIEXPORT void JNICALL Java_engine_RavEngine_LameLicenceRelease
	(JNIEnv * env, jobject jobj, jobject obj)
{
	if(NULL == obj) return;
	env->DeleteLocalRef(obj);
}

//////////////////////////////////////////////////////////////////////////

static LXCT LAMESTDCALLTYPE local_lame_extract_enter_file(const char* file_name , const char* format ,
	uint32_t depth , uint64_t FCLS ,void* handle , void* usr_data)
{
	Jni_auto_release_callback_info* cbinfo = (Jni_auto_release_callback_info*)usr_data;
	if(NULL == cbinfo) return LCT_DO_EXTRACT;
	JNIEnv* env = cbinfo->env;
	if(NULL == env) return LCT_DO_EXTRACT;
	Jni_auto_release_vm JVM(env);
	if(NULL == JVM.ptr()) return LCT_DO_EXTRACT;

	jobject obj_ExtractFeedback = cbinfo->objInterface;
	if(NULL == obj_ExtractFeedback) return LCT_DO_EXTRACT;
	jclass cls_ExtractFeedback = env->GetObjectClass(obj_ExtractFeedback);
	if(NULL == cls_ExtractFeedback) return LCT_DO_EXTRACT;
	jmethodID mid_ExtEnterFile = env->GetMethodID(cls_ExtractFeedback,"ExtEnterFile","(Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/Object;)J");
	if(NULL == mid_ExtEnterFile) return LCT_DO_EXTRACT;
	Jni_auto_release_object<jstring> obj_filename(env, env->NewStringUTF(file_name));
	Jni_auto_release_object<jstring> obj_format(env, env->NewStringUTF(format));
	return (LXCT)env->CallLongMethod(obj_ExtractFeedback,mid_ExtEnterFile,(jstring)obj_filename,(jstring)obj_format,
		(jlong)depth,(jlong)FCLS,(jlong)handle,cbinfo->usr_data);
}

static void LAMESTDCALLTYPE local_lame_extract_leave_file(const char* file_name , const char* format ,
	uint32_t depth , uint64_t FCLS ,void* handle , void* usr_data , LXLVT l)
{
	Jni_auto_release_callback_info* cbinfo = (Jni_auto_release_callback_info*)usr_data;
	if(NULL == cbinfo) return;
	JNIEnv* env = cbinfo->env;
	if(NULL == env) return;
	Jni_auto_release_vm JVM(env);
	if(NULL == JVM.ptr()) return;

	jobject obj_ExtractFeedback = cbinfo->objInterface;
	if(NULL == obj_ExtractFeedback)
		return;
	jclass cls_ExtractFeedback = env->GetObjectClass(obj_ExtractFeedback);
	if(NULL == cls_ExtractFeedback)
		return;
	jmethodID mid_ExtLeaveFile = env->GetMethodID(cls_ExtractFeedback,"ExtLeaveFile","(Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/Object;J)V");
	if(NULL == mid_ExtLeaveFile)
		return;
	Jni_auto_release_object<jstring> obj_filename(env, env->NewStringUTF(file_name));
	Jni_auto_release_object<jstring> obj_format(env, env->NewStringUTF(format));
	env->CallVoidMethod(obj_ExtractFeedback,mid_ExtLeaveFile,(jstring)obj_filename,(jstring)obj_format,(jlong)depth,
		(jlong)FCLS,(jlong)handle,cbinfo->usr_data,(jlong)l);
}


JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameExtractFile
	(JNIEnv * env, jobject jobj, jlong lame, jstring fname, jstring password, jobject cb, jobject usr_data)
{
	if(0 == lame || NULL == fname || NULL == cb) return E_FAIL;
	Jni_auto_release_string jar_fname(env,fname);
	Jni_auto_release_string jar_password(env,password);

	Jni_auto_release_callback_info cbInfo(env,jobj,cb,usr_data);
	lame_extract_feedback feedback = {local_lame_extract_enter_file, local_lame_extract_leave_file};
	return lame_extract_file((void*)lame,jar_fname,jar_password,&feedback,&cbInfo);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileGetSize
	(JNIEnv * env, jobject jobj, jlong handle)
{
	if(0 == handle) return 0;
	return lame_file_get_size((void*)handle);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileSeek
	(JNIEnv * env, jobject jobj, jlong handle, jlong offset, jlong method)
{
	if(0 == handle) return E_FAIL;
	return lame_file_seek((void*)handle,offset,(LSMETHOD)method);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileTell
	(JNIEnv * env, jobject jobj, jlong handle)
{
	if(0 == handle) return -1;
	return lame_file_tell((void*)handle);
}

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileRead
	(JNIEnv * env, jobject jobj, jlong handle, jbyteArray buf, jlong size)
{
	if(0 == handle || NULL == buf || size <= 0) return 0;
	jsize length = env->GetArrayLength(buf);
	length = (size > length) ? length : size;
	Jni_auto_release_c_memory<uint8_t> lpBuffer(new uint8_t[length]);
	if(NULL == lpBuffer) return 0;
	long lReadBytes = lame_file_read((void*)handle,lpBuffer,length);
	if(lReadBytes > 0) env->SetByteArrayRegion(buf,0,lReadBytes,(jbyte*)(uint8_t*)lpBuffer);
	return lReadBytes;
}

//////////////////////////////////////////////////////////////////////////
static long local_clame_fetch_cb(void* usr_data , size_t index , const void* packet , size_t size)
{
	Jni_auto_release_callback_info* cbinfo = (Jni_auto_release_callback_info*)usr_data;
	if(NULL == cbinfo) return 0;
	JNIEnv* env = cbinfo->env;
	if(NULL == env) return 0;
	Jni_auto_release_vm JVM(env);
	if(NULL == JVM.ptr()) return 0;

	jobject obj_FetchFeedback = cbinfo->objInterface;
	if(NULL == obj_FetchFeedback) return 0;
	jclass cls_FetchFeedback = env->GetObjectClass(obj_FetchFeedback);
	if(NULL == cls_FetchFeedback) return 0;
	jmethodID mid_deal = env->GetMethodID(cls_FetchFeedback,"deal","(J[BLjava/lang/Object;)J");
	if(NULL == mid_deal) return 0;
	
	Jni_auto_release_object<jbyteArray> jay(env, env->NewByteArray(size));
	if(NULL == jay) return 0;
	env->SetByteArrayRegion(jay, 0, size, (jbyte*)packet);
	return env->CallLongMethod(obj_FetchFeedback,mid_deal,(jlong)index,(jbyteArray)jay,cbinfo->usr_data);
}

JNIEXPORT jlong JNICALL Java_clame_RavEngine_ClameFetchBatch
  (JNIEnv * env, jobject jobj, jlong flags, jobjectArray fnames, jobject cb, jobject usr_data)
{
	if(!fnames || !cb) return E_INVALIDARG;
	long length = env->GetArrayLength(fnames);
	if(length <= 0) return E_FAIL;

	Jni_auto_release_callback_info cbInfo(env,jobj,cb,usr_data);
	Jni_auto_release_object_array name_list(env, fnames);
	return clame_fetch_batch(flags, name_list, length, local_clame_fetch_cb, &cbInfo);
}

JNIEXPORT jbyteArray JNICALL Java_clame_RavEngine_ClameCloudSignatureFile
	(JNIEnv * env, jobject jobj, jstring json_usrinfo, jobjectArray fnames)
{
	if(!fnames || !json_usrinfo) return NULL;
	long length = env->GetArrayLength(fnames);
	if(length <= 0) return NULL;

	Jni_auto_release_object_array name_list(env, fnames);
	Jni_auto_release_string json(env, json_usrinfo);
	uint32_t size = 0;
	const uint8_t* cloud_hash = 0;
	long hr = clame_cloud_signatue_file(json, name_list, length, &cloud_hash, &size);
	if(hr == S_OK) {
		Jni_auto_release_object<jbyteArray> jay(env, env->NewByteArray(size));
		if(NULL == jay) return NULL;
		env->SetByteArrayRegion(jay, 0, size, (jbyte*)cloud_hash);
		clame_mem_free((void*)cloud_hash);
		return jay.detach();
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////

static long LAMESTDCALLTYPE local_lame_vlibup_download(void* usr_data, const char* src_url)
{
	Jni_auto_release_callback_info* cbinfo = (Jni_auto_release_callback_info*)usr_data;
	if(NULL == cbinfo) return 0;
	JNIEnv* env = cbinfo->env;
	if(NULL == env) return 0;
	Jni_auto_release_vm JVM(env);
	if(NULL == JVM.ptr()) return 0;

	jobject obj_vlibupFeedback = cbinfo->objInterface;
	if(NULL == obj_vlibupFeedback) return 0;
	jclass cls_vlibupFeedback = env->GetObjectClass(obj_vlibupFeedback);
	if(NULL == cls_vlibupFeedback) return 0;
	jmethodID mid_download = env->GetMethodID(cls_vlibupFeedback,"Download","(Ljava/lang/String;Ljava/lang/Object;)J");
	if(NULL == mid_download) return 0;

	Jni_auto_release_object<jstring> obj_url(env, env->NewStringUTF(src_url));
	return env->CallLongMethod(obj_vlibupFeedback,mid_download,(jstring)obj_url,cbinfo->usr_data);
}

JNIEXPORT jlong JNICALL Java_vlibup_RavEngine_LameVlibupCreate
  (JNIEnv * env, jobject jobj)
{
	return (jlong)lame_vlibup_create();
}

JNIEXPORT jlong JNICALL Java_vlibup_RavEngine_LameVlibupGetCfg
	(JNIEnv * env, jobject jobj, jstring local_xml, jobject cb, jobject usr_data)
{
	if(!local_xml) return E_INVALIDARG;
	Jni_auto_release_string xml(env, local_xml);
	Jni_auto_release_callback_info cbInfo(env,jobj,cb,usr_data);
	return lame_vlibup_getcfg(xml, local_lame_vlibup_download, &cbInfo);
}

JNIEXPORT jlong JNICALL Java_vlibup_RavEngine_LameVlibupInit
  (JNIEnv * env, jobject jobj, jlong upper, jstring vlib_home, jstring local_cfg, jobject cb, jobject usr_data)
{
	if(!upper) return E_FAIL;
	Jni_auto_release_string home(env, vlib_home);
	Jni_auto_release_string cfg(env, local_cfg);

	Jni_auto_release_callback_info cbInfo(env,jobj,cb,usr_data);
	return lame_vlibup_init((void*)upper, home, cfg, local_lame_vlibup_download, &cbInfo);
}

JNIEXPORT jlong JNICALL Java_vlibup_RavEngine_LameVlibupUpdate
  (JNIEnv * env, jobject jobj, jlong upper, jstring want_home)
{
	Jni_auto_release_string want(env, want_home);
	return lame_vlibup_update((void*)upper, want);
}

JNIEXPORT void JNICALL Java_vlibup_RavEngine_LameVlibupRelease
  (JNIEnv * env, jobject jobj, jlong p)
{
	lame_vlibup_free((void*)p);
}

