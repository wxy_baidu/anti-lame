#! /bin/bash

if [ ! -d "bin" ]; then
	mkdir -p bin
fi

# build
javac -d bin -cp lib/lame.jar src/com/rising/engine/scanner/*.java

# copy
cp -rf lib/lame.jar ../../../../../rame.bin/
cp -rf bin/com ../../../../../rame.bin/
cp -rf javatest.sh ../../../../../rame.bin/

# run
#cd bin
#java -classpath ../lib/lame.jar: -Djava.library.path=./ com.rising.engine.scanner.t_mulscan $1 $2
