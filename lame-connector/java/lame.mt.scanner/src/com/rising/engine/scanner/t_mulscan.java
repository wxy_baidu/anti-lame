package com.rising.engine.scanner;

import java.util.*;
import com.rising.engine.lame.*;

public class t_mulscan extends Lame implements t_callback, t_scan_callback {
	
	private List<t_scan> t_list = new ArrayList<t_scan>();
	private volatile long t_count;
	private volatile long t_filecnt = 0;
	private volatile long t_curcnt = 0;
	private volatile long t_viruscnt = 0;
	
	t_mulscan(long count) {
		t_count = count;
	}
	public boolean push(String file) {
		t_filecnt++;
		int idx = (int)(t_filecnt % t_count);
		t_scan s = t_list.get(idx);
		s.push(file);
		return true;
	}
	public boolean scan(String path, String virusname) {
		t_curcnt++;
		if(null != virusname) {
			t_viruscnt++;
			System.out.println("[" + t_curcnt + "|" + t_filecnt + "] " + path + "\tInfected: " + virusname);
			return true;
		} else {
			System.out.println("[" + t_curcnt + "|" + t_filecnt + "] " + path);
			return false;
		}
	}
	public void detect(String path) {

		//init sdk
		long vdb = openVirusLib(null);
		if(0 == vdb) return;

		//init thread
		for(long i=0; i<t_count; i++) {
			t_scan ts = new t_scan("thread_" + i, vdb, this);
			t_list.add(ts);
			ts.start();
		}

		//travl
		t_travl tl = new t_travl(path, this);
		tl.start();
		
		//wait
		sleep(1000);
		while(!is_exit()) {
			sleep(100);
		}
		stop();
		closeVirusLib();

		if(t_filecnt > 0) {
			System.out.printf("Infected: %d / %d = %.2f%%\n", t_viruscnt, t_filecnt, 100.0 * t_viruscnt/ t_filecnt);	
		} else {
			System.out.println("No target to scan.");
		}
	}
	public void sleep(long t) {
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public boolean is_exit() {
		for( t_scan i : t_list) {
			if(i.query() > 0) return false;
		}
		return true;
	}
	public void stop() {
		while(t_curcnt != t_filecnt) {
			sleep(100);
		}
		for( t_scan i : t_list) {
			i.stop();
		}
	}

	public void get_mem_info() {
		long Mb = 1024 * 1024;
		long totalMemory = Runtime.getRuntime().totalMemory() / Mb;
		long freeMemory = Runtime.getRuntime().freeMemory() / Mb;
		long maxMemory = Runtime.getRuntime().maxMemory() / Mb;

		System.out.println("totalMemory:" + totalMemory + " freeMemory:" + freeMemory + " maxMemory:" + maxMemory);
	}

	public static void main(String[] args) {
		int count = Integer.parseInt(args[0], 10);
		String path = args[1];
		t_mulscan mscan = new t_mulscan(count);
		mscan.get_mem_info();
		mscan.detect(path);
		mscan.get_mem_info();
	}
}
