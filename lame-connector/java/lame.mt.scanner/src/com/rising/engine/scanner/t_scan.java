package com.rising.engine.scanner;

import java.util.*;
import com.rising.engine.lame.*;

public class t_scan extends Lame implements Runnable {

	private String t_name;
	private Thread t;
	private Queue<String> t_queue = new LinkedList<String>();
	private t_scan_callback t_cb;
	private long t_vdb;
	private boolean t_running = true;
	
	t_scan(String name, long vdb, t_scan_callback cb) {
		t_name = name;
		t_cb = cb;
		t_vdb = vdb;
	}

	boolean init() {
		setConfig("precise-format=0");
		if(!loadEngine(t_vdb)) {
			System.out.println("load failed.");
			return false;
		}
		return true;
	}

	public void scan(String path) {
		ScanResult sr = scanFile(path);
		if(null != sr) {
			t_cb.scan(path, sr.vname);
		} else {
			t_cb.scan(path, null);
		}
	}

	public void run() {
		init();
		
		while(t_running) {
			while(t_queue.size() == 0) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			String ss = t_queue.remove();
			if(null != ss) {
				scan(ss);
			}
		}

		destoryEngine();
	}

	public void start() {
		if (t == null) {
			t = new Thread (this, t_name);
			t.start();
		}
	}

	public void stop() {
		t_running = false;
	}

	public long query() {
		return t_queue.size();
	}

	public boolean push(String file) {
		while(t_queue.size() > 100 ) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return t_queue.add(file);
	}
}
