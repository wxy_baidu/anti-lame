package com.rising.engine.scanner;

import java.io.File;

public class t_travl implements Runnable {

	private String t_path;
	private Thread t;
	private t_callback t_cb;
	
	t_travl(String path, t_callback cb) {
		t_path = path;
		t_cb = cb;
	}
	public void Travel(String path) {
		File or = new File(path);
		if(!or.exists()) return;
		if(or.isDirectory()) {
			File [] files = or.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						t_cb.push(file.getAbsolutePath());
					} else if (file.isDirectory()) {
						Travel(file.getAbsolutePath());
					}
				}
			}
		} else if(or.isFile()) {
			t_cb.push(or.getAbsolutePath());
		}
	}
	public void run() {
		Travel(t_path);
	}
	public void start() {
		if (t == null) {
			t = new Thread (this, t_path);
			t.start();
		}
	}
}
