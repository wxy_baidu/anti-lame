* [工程说明](#工程说明)
  * [Jni-lame](#jni-lame)
  * [Jdk-lame](#jdk-lame)
  * [lame.scanner](#lame.scanner)
  * [lame.mt.scanner](#lame.mt.scanner)
  * [lame.update](#lame.update)


# 工程说明
___
- **jni-lame**
>本工程是C转JAVA的接口。
- **jdk-lame**
>本工程是JAVA封装的接口，利用jni-lame封装的C接口封装成Lame类供用户使用，最终生成jar包文件。
- **lame.scanner**
>本工程是测试代码用例，导入jdk-lame生成的jar包，使用了两种方式扫描文件，一种是直接扫描一种是使用回调函数扫描。
- **lame.mt.scanner**
>本工程是多线程测试代码用例，导入jdk-lame生成的jar包，使用了多线程方式扫描文件。
- **lame.update**
>本工程是病毒库升级代码用例，导入jdk-lame生成的jar包，依据参数进行病毒库增量升级。


## jni-lame
___

根据java工程封装的jni接口函数。
```c
EXTERN_C JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameOpenVdb(JNIEnv*, jobject, jstring);
EXTERN_C JNIEXPORT void JNICALL Java_engine_RavEngine_LameCloseVdb(JNIEnv*, jobject, jlong);

EXTERN_C JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameCreate(JNIEnv*, jobject, jlong);
EXTERN_C JNIEXPORT void JNICALL Java_engine_RavEngine_LameDestroy(JNIEnv*, jobject, jlong);
EXTERN_C JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFork(JNIEnv*, jobject, jlong);
EXTERN_C JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameParamSet(JNIEnv*, jobject, jlong, jstring);
EXTERN_C JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameInit(JNIEnv*, jobject, jlong);

EXTERN_C JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameScanFile(JNIEnv*, jobject, jlong, jstring);
EXTERN_C JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameScanFileWithCallback (JNIEnv *, jobject, jlong, jstring, jobject, jobject);
EXTERN_C JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameScanMem(JNIEnv*, jobject, jlong, jbyteArray);
EXTERN_C JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameScanMemWithCallback(JNIEnv *, jobject, jlong, jbyteArray, jobject, jobject);
EXTERN_C JNIEXPORT void JNICALL Java_engine_RavEngine_LameScanResultRelease(JNIEnv*, jobject, jobject);

EXTERN_C JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameGetVersion(JNIEnv*, jobject);
EXTERN_C JNIEXPORT void JNICALL Java_engine_RavEngine_LameInfoRelease(JNIEnv*, jobject, jobject);

EXTERN_C JNIEXPORT jobject JNICALL Java_engine_RavEngine_LameGetLicenceInfo(JNIEnv*, jobject);
EXTERN_C JNIEXPORT void JNICALL Java_engine_RavEngine_LameLicenceRelease(JNIEnv*, jobject, jobject);

JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameExtractFile(JNIEnv *, jobject, jlong, jstring, jstring, jobject, jobject);
JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileGetSize(JNIEnv *, jobject, jlong);
JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileSeek(JNIEnv *, jobject, jlong, jlong, jlong);
JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileTell(JNIEnv *, jobject, jlong);
JNIEXPORT jlong JNICALL Java_engine_RavEngine_LameFileRead(JNIEnv *, jobject, jlong, jbyteArray, jlong);
```


## jdk-lame
___

扫描回调函数接口类
```java
public interface ScanFeedback {
	public abstract long DtEnterFile(String file_name, long depth, Object usr_data);
	public abstract void DtLeaveFile(String file_name, long depth, Object usr_data, long l);
	public abstract long DtAlarm(String file_name, ScanResult sr, Object usr_data);
}
```

根据jni函数封装好的java扫描接口类。
```java
public class Lame extends RavEngine {
	public long OpenVdb(String vdbf);
	public void CloseVdb();
	public boolean Load(long lib);
	public boolean Set(String param);
	public void Unload();
	public LicenceInfo GetLicence();
	public LameInfo GetVersion();
	public ScanResult ScanFile(String fname);
	public ScanResult ScanMem(byte [] data);
	public boolean ScanFileWithCallback(String fname,ScanFeedback cb, Object usrdata);
	public boolean ScanMemWithCallback(byte [] data,ScanFeedback cb, Object usrdata);
}
```

根据jni函数封装好的java文件解包接口类。
```java
public class Lame extends RavEngine {
	public boolean ExtractFile(String fname, String passwd, ExtractFeedback cb, Object usrdata);
	public long FileGetSize(long handle);
	public boolean FileSeek(long handle, long offset, long method);
	public long FileTell(long handle);
	public long FileRead(long handle, byte[] buf, long size);
}
```


## lame.scanner
___

此示例中类scan使用回调或者不使用回调函数两种扫描方式都实现了，如果不使用回调函数则可以不实现JdkInterface接口函数。
```java
public class scan extends Lame implements ScanFeedback, ExtractFeedback {

	// test Jni API
	public void TestInterface(String filename) {
		long vdb = OpenVdb(null);
		if(0 == vdb) return;
		if(!Load(vdb)){
			Unload();
			return;
		}
		Set("precise-format=0");
		ScanResult sr = ScanFile(filename);
		if(null != sr){
			System.out.println(filename + "\t" + sr.vname + "!" + sr.vid32);
			LameScanResultRelease(sr);
		} else {
			System.out.println(filename);
		}
		Unload();

		LameInfo lv = GetVersion();
		if(null != lv){
			System.out.println("sdk version info");
			System.out.println("engine: " + lv.engine_version);
			System.out.println("malware: " + lv.virus_db_version);
			LameInfoRelease(lv);
		}
		
		LicenceInfo li = GetLicence();
		if(null != li){
			System.out.println("licence info");
			System.out.println("version: " + li.Version);
			System.out.println("Owner: " + li.Owner);
			System.out.println("Date: " + li.Date);
			System.out.println("Authm: " + li.Authm);
			System.out.println("Data: " + li.Data);
			LameLicenceRelease(li);
		}
	}
	//callbcak
	public long DtEnterFile(String file_name, long depth, Object usr_data) {
		System.out.println("[callback] ++ " + file_name + " && " + depth + " && " + usr_data);
		return 0;
	}
	public void DtLeaveFile(String file_name, long depth, Object usr_data, long l) {
		System.out.println("[callback] -- " + file_name + " && " + depth + " && " + usr_data + " && " + l);	
	}
	public long DtAlarm(String file_name, ScanResult sr, Object usr_data) {
		System.out.println("[callback] ** " + file_name + " && " + sr.vname + "!" + sr.vid32 + " && " + usr_data);
		return 1;
	}
	public long ExtEnterFile(String file_name, String format, long depth, long FCLS, long handle, Object usr_data) {
		//System.out.println("[callback] ++ " + file_name + " && " + format + " && " + depth + " && " + FCLS + " && " + handle + " && " + usr_data);
		if(depth >= 1) {
			long size = FileGetSize(handle);
			long offset = FileTell(handle);
			if(FileSeek(handle,0,0)) {
				byte [] buffer = new byte [(int)size]; 
				long lbytes = FileRead(handle, buffer, size);
				System.out.println(file_name + " " + size + " " + offset + " " + lbytes);	
			}	
		}
		return 1;
	}
	public void ExtLeaveFile(String file_name, String format, long depth, long FCLS, long handle, Object usr_data, long l) {
		//System.out.println("[callback] -- " + file_name + " && " + format + " && " + depth + " && " + FCLS + " && "  + handle + " && " + usr_data + " && " + l);
	}
	// test lame API
	public void PrintResultl(String path, boolean usecb) {
		ExtractFile(path, null, this, "__test_extract_string__");
		if(usecb) {
			ScanFileWithCallback(path, this, "__test_scan_string__");
		}
		else {
			ScanResult sr = ScanFile(path);
			if(null != sr) {
				System.out.println("[normal] " + path + "\tInfected: " + sr.vname);
			} else {
				System.out.println("[normal] " + path);
			}
		}
	}
	public void TravelPath(String path, boolean usecb) {
		File or = new File(path);
		if(!or.exists()) return;
		
		if(or.isDirectory()) {
			File [] files = or.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isFile()) {
						PrintResultl(file.getAbsolutePath(), usecb);
					} else if (file.isDirectory()) {
						TravelPath(file.getAbsolutePath(), usecb);
					}
				}
			}
		} else if(or.isFile()) {
			PrintResultl(or.getAbsolutePath(), usecb);
		}
	}
	void LameTest(String path,boolean usecb) {
		File file = new File(path);
		if(!file.exists()) return;
		if(!Load(0)) return;
		TravelPath(path,usecb);
		Unload();
	}
	
	public static void main(String[] args) {
		System.out.println("------------ main start ----------------------");
		scan s = new scan();
		String path = args[0];
		boolean usecb = true;
		s.LameTest(path,usecb);
		s.Unload();
		System.out.println("------------ main end ----------------------");
	}
}
```


## lame.mt.scanner
___

此示例中类t_mulscan使用多线程方式对文件进行扫描，每个线程是相互独立的实例，可以共用一个病毒库vdb，但是线程间不能相互混用。
```java
public class t_mulscan extends Lame implements t_callback, t_scan_callback {
	
	private List<t_scan> t_list = new ArrayList<t_scan>();
	private long t_count;
	private long t_filecnt = 0;
	private long t_curcnt = 0;
	private long t_viruscnt = 0;
	
	t_mulscan(long count) {
		t_count = count;
	}
	public boolean push(String file) {
		synchronized (this) {
			t_filecnt++;
			int idx = (int)(t_filecnt % t_count);
			t_scan s = t_list.get(idx);
			s.push(file);
		}
		return true;
	}
	public boolean scan(String path, String virusname) {
		synchronized (this) {
			t_curcnt++;
			if(null != virusname) {
				t_viruscnt++;
				System.out.println("[" + t_curcnt + "|" + t_filecnt + "] " + path + "\tInfected: " + virusname);
				return true;
			} else {
				System.out.println("[" + t_curcnt + "|" + t_filecnt + "] " + path);
				return false;
			}
		}
	}
	public void detect(String path) {
		//init sdk
		long vdb = OpenVdb(null);
		if(0 == vdb) return;

		//init thread
		for(long i=0; i<t_count; i++) {
			t_scan ts = new t_scan("thread_" + i, vdb, this);
			t_list.add(ts);
			ts.start();
		}

		//travl
		t_travl tl = new t_travl(path, this);
		tl.start();
		
		//stop
		sleep(1000);
		while(!is_exit()) {
			sleep(100);
		}
		stop();

		if(t_filecnt > 0) {
			System.out.println("Infected: " + t_viruscnt + " / " + t_filecnt + " = " + 100.0 * t_viruscnt/ t_filecnt + "%");	
		} else {
			System.out.println("No target to scan.");
		}

	}
	public static void main(String[] args) {
		int count = Integer.parseInt(args[0], 10);
		String path = args[1];
		t_mulscan mscan = new t_mulscan(count);
		mscan.detect(path);
	}
}
```


## lame.update
___

此示例中类update通过回调函数Download对病毒库升级需要的列表文件进行下载，并通过下载好的文件进行增量升级。
```java
public class update extends Upper implements DownloadFeedback {

	public long Download(String url, Object usrdata) {
		UpContext ctx = (UpContext) usrdata;

		String file_path = ctx.wantname + "/" + url.substring(url.lastIndexOf('/')+1);
		ctx.flag = false;
		ctx.push(file_path);

		System.out.println(url + "  >>  " + file_path);

		if(!new File(file_path).exists()) {
			try {
				InputStream in = new URL(url).openStream();
				Files.copy(in, Paths.get(file_path), StandardCopyOption.REPLACE_EXISTING);
				ctx.flag = true;
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
		return 0;
	}

	public void Update(String home, String localxml, Object usrdata) {
		do {
			UpContext ctx = (UpContext) usrdata;

			if(!VlibupGetCfg(localxml, this, usrdata)) {
				System.out.println("get viruslib cfg file failed.");
				return;
			}
			if(!VlibupCreate()) {
				System.out.println("Create viruslib upper failed.");
				return;
			}
			long r = VlibupInit(home, ctx.top(), this, usrdata);
			if(UP_FAILED == r) {
				System.out.println("viruslib upper init failed.");
				break;
			} else if(UP_LASTEST == r) {
				System.out.println("viruslib is lastest.");
				break;
			}
			if(!VlibupUpdate(ctx.wantname)) {
				System.out.println("viruslib upper update failed.");
				break;
			}
			System.out.println("viruslib upper update successfully.");
		} while(false);

		VlibupRelease();
		Clean(usrdata);
	}

	public void Clean(Object usrdata) {
		UpContext ctx = (UpContext) usrdata;
		for(String path:ctx.list) {
			new File(path).delete();
		}
		new File(ctx.wantname).delete();
	}

	public static void main(String[] args) {
		System.out.println("-------- start ----------");
		new update().Update("./", "vlco.xml", new UpContext("xxxx"));
		System.out.println("-------- end ----------");
	}
}
```