#include "stdafx.h"

using namespace _13;
using namespace v4;

class Scanner : public IRXScanCallback, CUnknownImp
{
public:
	UNKNOWN_IMP2_(_13::IRXCallback, _13::IRXCallbackX);
public:
	STDMETHOD_(PROCOPT, GetProcOptions )( ) {
		return PROC_CC_BOOST;
	}

	STDMETHOD_(EFC,EnterFile)( IRXStream * pstm, SIZE_T uNest ) { 
		return EFC_CONTINUE;
	}
	STDMETHOD_(VOID,LeaveFile)( IRXStream * pstm, SIZE_T uNest, LEAVECASE uCase ) {
		return ;
	}

	STDMETHOD_(VOID,EnterArchive)( IRXArchive * arch ) {
		return ;
	}
	STDMETHOD_(VOID,LeaveArchive)( IRXArchive * arch, TRESULT trRebuild ) {
		return ;
	}

	STDMETHOD_(TEFC, OnAlarm)( IRXArchive * arch, SGID40 uSigId ) {
		return TEFC_ABORT;
	}
	STDMETHOD_(TEFC, OnTreated)( IRXArchive * arch, SGID40 uSigId, TRESULT trTreat ) {
		return TEFC_ABORT;
	}

	// 询问压缩包密码等功能
	STDMETHOD(OnComplexCallback)( xv::Value& inVar, xv::Value& outVar ) {
		return E_NOTIMPL;
	}

	STDMETHOD(QueryLibFileVersion)( LPCSTR aLibFileName, UINT32 uCurrent, UINT32 * lpLastVer ) {
		return E_NOTIMPL;
	}

	// it's first
	// if FAILED, call OnTreated 
	STDMETHOD(OnTreatedEx)( IRXArchive * arch, SGID40 uSigId, TEFC& rHowTreat, TRESULT trTreat, SIZE_T uTreatCount ) {
		return E_NOTIMPL;
	}

	/**
	* @brief 当前文件报毒时的回调(会取代IRXCallback::OnAlarm)
	* @param arch 目标文档对象（文件格式对象）
	* @param result 报毒信息
	* @return 处理方式控制码,参见 @enum TEFC
	*/
	STDMETHOD_(TEFC, OnAlarm)(IRXArchive *arch, IExtDetectionResult *result) 
	{
		return TEFC_CLEAN;
	}

	/**
	* @brief 当前文件报毒时的回调(会取代IRXCallback::OnTreated/OnTreatedEx)
	* @param arch 目标文档对象（文件格式对象）
	* @param result 报毒信息
	* @param trTreat 病毒处理的结果,参见 @enum TRESULT
	* @return 处理方式控制码,参见 @enum TEFC
	*/
	STDMETHOD_(TEFC, OnTreated)(IRXArchive *arch, IExtDetectionResult *result, TRESULT trTreat) 
	{
		return TEFC_REMOVE;
	}

};

static void ShowHelp()
{
	printf("Usage:\n");
	printf("	  lame.v4.scanner d:\\malware.rmd d:\\samples\n\n");
}

int main( int argc, char ** argv ) 
{
	if(argc < 3) {
		ShowHelp();
		return -1;
	}

	LPCSTR vlib_path = argv[1];
	LPCSTR scan_file_path = argv[2];

	UTIL::com_ptr<IRXParameters> params;
	RFAILED( RameCreateInstance(NULL, ECID_Parameter, re_uuidof(IRXParameters), params.vpp() ) );

	// 通过字符串设置
	params->SetParamString("precise-format=0");

	// 通过xvalue设置
	xv::xvalue_t value(1);
	params->SetParam("kill", value);

	UTIL::com_ptr<IUnknown> libfile;

	XSTRP xFileName;
	xFileName.IsUnicode = FALSE;
	xFileName.NameA = vlib_path;
	SignatureFile SignFile;
	RFAILED(RameSignatureOpen( params, xFileName, NULL, &SignFile ) );

	EngineInitParam InitParam;
	InitParam.FileSystem = NULL;
	InitParam.Parameters = params;
	InitParam.SignatureFile = SignFile.Signature;

	EngineInstance engine_;
	RFAILED( RameEngineCreate(&InitParam, &engine_) );

	Scanner scanner;
	scanner.AddRef();

	xFileName.NameA = scan_file_path;
	UTIL::com_ptr<IRXStream> target;
	RFAILED( engine_.FileSystem->XCreateStream(xFileName, target.pp() ) );

	RameEngineScan( &engine_, target, &scanner );

	RameEngineClose( &engine_ );

	RameSignatureClose(&SignFile);
	
	return 0;

}