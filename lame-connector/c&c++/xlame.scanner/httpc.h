#ifndef __HTTP_CLIENT__
#define __HTTP_CLIENT__

#include "stdafx.h"
#define BUILDING_LIBCURL
#define HTTP_ONLY
#include "../curl/curl.h"
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "wldap32.lib")
#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "curl.lib")

class http_request_t
{
public:
	struct curl_t 
	{
		CURL * _curl;
		curl_t()
		{
			_curl = curl_easy_init();
		}
		~curl_t()
		{
			if( _curl ) curl_easy_cleanup(_curl);
		}
		CURL * detach()
		{
			CURL * r = _curl;
			_curl = 0;
			return r;
		}
		operator CURL * () {
			return _curl;
		}
	};

private:

	curl_t curl_;
	struct curl_slist * headers_;
	std::string response_;

public:

	http_request_t( ) : headers_(0)
	{
		//curl_easy_setopt( curl_, CURLOPT_VERBOSE, 1 );
	}
	~http_request_t()
	{
		if( headers_ ) curl_slist_free_all(headers_);
	}
	CURL * get_curl() {
		return curl_;
	}
	CURLcode set_url( const char * url )
	{
		return curl_easy_setopt( curl_, CURLOPT_URL, url ); //post参数  
	}
	CURLcode add_header( const char * header ) 
	{
		struct curl_slist * new_headers = curl_slist_append(headers_, header);
		if( !new_headers ) return CURLE_OUT_OF_MEMORY;
		headers_  = new_headers;
		return CURLE_OK;
	}
	CURLcode set_timeout( size_t timeout_ms )
	{
		return curl_easy_setopt( curl_, CURLOPT_TIMEOUT_MS, timeout_ms ); //post参数  
	}
	CURLcode post( const void * data, size_t bytes )
	{
		CURLcode r;
		if( headers_ ) {
			r = curl_easy_setopt( curl_, CURLOPT_HTTPHEADER, headers_ );
			if( r != CURLE_OK ) return r;
		}
		r = curl_easy_setopt( curl_, CURLOPT_POSTFIELDSIZE, bytes ); //post参数  
		if( r != CURLE_OK ) return r;

		r = curl_easy_setopt(curl_, CURLOPT_SSL_VERIFYPEER, 0L);
		if( r != CURLE_OK ) return r;

		r = curl_easy_setopt( curl_, CURLOPT_POSTFIELDS, data ); //post参数 
		if( r != CURLE_OK ) return r;

		curl_easy_setopt( curl_, CURLOPT_WRITEFUNCTION, &http_request_t::recv_to_std_string );
		if( r != CURLE_OK ) return r;

		response_.clear();
		curl_easy_setopt( curl_, CURLOPT_WRITEDATA, &response_ );
		if( r != CURLE_OK ) return r;

		return curl_easy_perform(curl_);
	}

	CURLcode get_http_response_code( size_t * http_response_code )
	{
		return curl_easy_getinfo( curl_, CURLINFO_RESPONSE_CODE, http_response_code );
	}

	std::string & get_response_data()
	{
		return response_;
	}

private:

	static size_t recv_to_std_string(void * buffer, size_t size, size_t nmemb, void * user_p ) {
		size_t eff = size * nmemb;
		if( !eff ) return 0;
		std::string & wd = *(std::string * )user_p;
		wd.append( (char*)buffer, eff );
		return nmemb;
	}
	
};


#endif