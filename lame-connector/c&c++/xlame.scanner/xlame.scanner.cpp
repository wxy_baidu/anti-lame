// xlame.scanner.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <queue>
#include <vector>
#include <mutex>
#include <thread>
#include <lame.v2.h>
#include "json/json.h"
#include "httpc.h"


#ifdef _WIN32
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "wldap32.lib")
#endif

template<class T>
class  SafeQ
{
public:
	SafeQ(uint32_t qsize = 100):_qsize(qsize){}
public:
	bool push(T& s , bool force = true)
	{
		if(push_(s)) return true;

		if (!force) return false;

		while (true)
		{
			if(push_(s)) return true;
			std::chrono::milliseconds(10);
		}

		return true;
	}

	bool fetch(T& s)
	{
		std::lock_guard<std::mutex> lock(_locker);
		if (_queue.empty()) return false;
		s = _queue.front();
		_queue.pop();
		return true;
	}

	bool fetch(std::vector<T>& values , uint32_t fetch_count)
	{
		std::lock_guard<std::mutex> lock(_locker);
		if (_queue.empty()) return false;

		int32_t _fetch_count = fetch_count > _queue.size() ? _queue.size() : fetch_count;
		while (_fetch_count > 0)
		{
			values.push_back(_queue.front());
			_queue.pop();
			_fetch_count--;
		}

		return true;
	}
private:
	inline bool push_(T& s)
	{
		std::lock_guard<std::mutex> lock(_locker);
		if (_queue.size() + 1 >= _qsize) return false;

		_queue.push(s);
		return true;
	}
private:
	std::queue<T> _queue;
	std::mutex _locker;
	uint32_t _qsize;
}; 


typedef SafeQ<std::string> SafeSq;

#define THREAD_QUIT_FALG		"#quit#"
class XScanner
{
#define ONCE_CLOUD_QUERY_SIZE		10
public:
	XScanner():_timeout(1000),
			   _is_continue(true)
	{
		_url = "https://umc.rising.com.cn/openzdr/detect?apikey=d477c085-2475-4d43-a09a-883468ecbe9c";
	}
public:
	HRESULT Create()
	{
		_threads.push_back(std::thread( std::bind(&XScanner::process_path_thread , this )));
		_threads.push_back(std::thread( std::bind(&XScanner::cloud_query_thread , this )));

		return S_OK;
	}
	void Process(LPCSTR lpdir)
	{
		if (!lpdir) return;
		sq_push(_scan_path_q, lpdir);
	}

	void Destroy()
	{
		sq_push(_scan_path_q,THREAD_QUIT_FALG);

		std::vector<std::thread>::iterator iter;
		for (iter = _threads.begin() ; iter != _threads.end() ; iter++)
		{
			iter->join();
		}
	}
private:
	void ProcesssFileOrDir(LPCSTR lpdir)
	{

#ifdef _WIN32

		DWORD dwFileAttr = GetFileAttributes(lpdir);
		if( dwFileAttr == INVALID_FILE_ATTRIBUTES ) return ;
		if( dwFileAttr & FILE_ATTRIBUTE_REPARSE_POINT ) return ;
		if( dwFileAttr & FILE_ATTRIBUTE_DIRECTORY ) ProcessDir(lpdir );
		else ProcessFile(lpdir);
#else

		struct stat statInfo;
		if(stat(lpdir, &statInfo) != 0) return;
		if(S_ISREG(statInfo.st_mode))
			ProcessFile(lpdir);
		else if(S_ISDIR(statInfo.st_mode))
			ProcessDir(lpdir);
#endif
	}
	void ProcessDir(LPCSTR lpdir)
	{

#ifdef _WIN32
		HANDLE hFind;
		WIN32_FIND_DATA wfData;

		std::string sPath(lpdir);
		if (sPath.back() == '\\') sPath.append("*");
		else sPath.append("\\*");

		hFind = FindFirstFile(sPath.c_str(),&wfData);
		if(hFind == INVALID_HANDLE_VALUE) return ;

		do
		{
			if( wfData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT ) 
				continue;

			if( (_stricmp(wfData.cFileName,".")==0) || (_stricmp(wfData.cFileName,"..")==0) )
				continue;

			std::string strDir(lpdir);
			if (strDir.back() != '\\') strDir.append("\\");
			strDir.append(wfData.cFileName);

			if(wfData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ProcessDir(strDir.c_str());
			else ProcessFile(strDir.c_str());

		}while(FindNextFile(hFind,&wfData));

		FindClose(hFind);

#else

		DIR *dirp;  
		struct dirent *direntp;  
		if((dirp = opendir(lpdir)) == NULL)
			return;
		std::string filePath;
		while((direntp = readdir(dirp)) != NULL) {
			if(strcmp(direntp->d_name, ".") == 0 || strcmp(direntp->d_name, "..") == 0)
				continue;
			filePath = lpdir;
			filePath.append("/");
			filePath.append(direntp->d_name);
			ProcessFile(filePath.c_str());
		}
		closedir(dirp);
		return ;


#endif

	}
	HRESULT ProcessFile(LPCSTR lpFileName)
	{
		sq_push(_cloudq, lpFileName);
		return S_OK;
	}

	HRESULT fetch_cloud_feature(const uint8_t** ft , uint32_t& size)
	{
		if (!ft) return E_FAIL;

		std::vector<std::string> files;
		_cloudq.fetch(files , ONCE_CLOUD_QUERY_SIZE);

		if (files.empty()) return E_FAIL;

		uint32_t fname_count = 0;
		const char* fnames[ONCE_CLOUD_QUERY_SIZE] = {0};
		for (uint32_t idx = 0 ; idx < files.size() ; idx++)
		{
			if (files[idx] == THREAD_QUIT_FALG) 
			{
				_is_continue = false;
				continue;
			}

			fnames[idx] = files[idx].c_str();
			fname_count++;
		}

		if (fname_count == 0) return E_FAIL;
		
		return clame_cloud_signatue_file(0 , fnames , fname_count ,ft , &size);
	}


	HRESULT http_request(uint8_t* data , uint32_t size , Json::Value& jresult)
	{
		const char * http_header = "Content-Type: application/octet-stream";

		http_request_t req;
		CURLcode r = req.set_url(_url.c_str());
		if( r ) return E_FAIL;

		if( _timeout < 2000 ) 
			_timeout = 2000;
		else if( _timeout > 6000 )
			_timeout = 6000;

		r = req.set_timeout(_timeout);
		if( r ) return E_FAIL;

		r = req.add_header(http_header);
		if( r ) return E_FAIL;


		r = req.post( data, size);
		if( r ) {
			return E_FAIL; 
		}

		size_t code = 0;
		r = req.get_http_response_code( &code );
		if( code != 200 ) 
		{
			return E_FAIL; 
		}


		std::string jres = req.get_response_data();
		std::string jr (jres, 1, jres.size()-2);
		return Json::Reader().parse(jr , jresult , false) ? S_OK : E_FAIL;
	}

	void cloud_query_thread()
	{
		while (_is_continue)
		{
			uint8_t* feature = 0;
			uint32_t size = 0;

			if (FAILED(fetch_cloud_feature((const uint8_t**)&feature , size)))
			{
				std::chrono::milliseconds(20);
				continue;
			}

			Json::Value jresult;
			HRESULT hr = http_request(feature , size , jresult);
			clame_mem_free(feature);
			if (FAILED(hr)) continue;
		}

		sq_push(_cloudq, THREAD_QUIT_FALG);

	}
	void process_path_thread()
	{
		while (true)
		{
			std::string s;
			if(!_scan_path_q.fetch(s))
			{
				std::chrono::milliseconds(10);
				continue;
			}

			if (s == THREAD_QUIT_FALG) break;
			ProcesssFileOrDir(s.c_str());
		}

		sq_push(_cloudq, THREAD_QUIT_FALG);
	}
	inline void sq_push(SafeSq& sq, LPCSTR name)
	{
		std::string __name(name);
		sq.push(__name);
	}
private:
	SafeSq _cloudq;
	SafeSq _scanq;
	SafeSq _scan_path_q;
	uint32_t _timeout;
	std::string _url;
	bool _is_continue;
	std::vector<std::thread> _threads;
};

int main(int argc, char* argv[])
{
	if (argc < 2) return -1;
	
	XScanner scanner;
	if(scanner.Create() < 0)  return -1;
	scanner.Process(argv[1]);
	scanner.Destroy();
	return 0;
}

