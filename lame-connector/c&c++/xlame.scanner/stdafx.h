// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <stdio.h>


// TODO: reference additional headers your program requires here

#ifndef _WIN32

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdarg.h>
#include <string.h>

typedef long HRESULT;
typedef const char* LPCSTR;

#define E_FAIL	long(0x80004005L)
#define S_OK	long(0x0L)

#ifndef SUCCEEDED
#define SUCCEEDED(hr) ((HRESULT)(hr) >= 0)
#endif

#ifndef FAILED
#define FAILED(hr) ((HRESULT)(hr) < 0)
#endif

#endif

