// scan.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <string>
#include <lame.v2.h>


#define BUILDING_LIBCURL
#define HTTP_ONLY
#include "../curl/curl.h"
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "wldap32.lib")
#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "curl.lib")

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream){
	return fwrite(ptr, size, nmemb, (FILE*)stream);
}

class CDownload {
private:
	CURL* curl;
public:
	CDownload(): curl(NULL){ curl = curl_easy_init(); }
	~CDownload(){ if(curl) curl_easy_cleanup(curl); }
	HRESULT get( const char* url, char* filename) {
		HRESULT hr = E_FAIL;
		FILE *	fp = NULL;
		do {
			if( !url ) break;
			CURLcode lc = curl_easy_setopt( curl, CURLOPT_URL, url );
			if( CURLE_OK != lc ) break;

			lc = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
			if( CURLE_OK != lc ) break;

			lc = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
			if( CURLE_OK != lc ) break;

			const char* lpTmpfilename = '\0' != filename[0] ? filename : "virlib.tmp";
			fp = fopen(lpTmpfilename,"wb");
			if( NULL == fp ) break;

			lc = curl_easy_setopt( curl, CURLOPT_WRITEDATA, fp );
			if( CURLE_OK != lc ) break;

			lc = curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, write_data );
			if( CURLE_OK != lc ) break;

			lc = curl_easy_perform( curl );
			if( CURLE_OK != lc ) break;

			if(fp) fclose(fp); fp = NULL;
			if( '\0' == filename[0] ) {	strcpy( filename, lpTmpfilename ); }
			hr = S_OK;
		} while (false);
		if(fp) fclose(fp);
		return hr;
	}
};

std::string get_filename_from_path(std::string str){
#ifdef _WIN32
	size_t pos = str.rfind('\\');
	if(-1 == pos) {
		pos = str.rfind('/');
	}
#else
	size_t pos = str.rfind('/');
#endif
	if(-1 == pos) return "";
	return std::string(str, pos+1, str.size()-pos);
}

std::string merge_path(std::string a, std::string b) {
	char c = a[a.size()-1];
	if('\\' != c || '/' != c){
#ifdef _WIN32
		return a + "\\" + b;
#else
		return a + "/" + b;
#endif
	} else {
		return a + b;
	}
}

typedef struct dlcb_data{
	std::string want_home;
	std::string local_path;
	std::string url_header;
	long get_url_hdr(std::string &str) {
		size_t pos = str.find("viruslib/");
		if(-1 != pos) {
			url_header.assign(str, 0, pos+9);
			return S_OK;
		}
		return E_FAIL;
	}
	dlcb_data(std::string wh) : want_home(wh) {}
} dlcb_data;

long savefile(const char* url, const char* path) {
	CDownload dl;
	return dl.get(url, (char*)path);
}

long LAMESTDCALLTYPE save_to_file_cb(void* user_data, const char* src_url) {
	std::string url;
	dlcb_data* data = (dlcb_data*)user_data;
	if(strnicmp(src_url, "http", 4) == 0) {
		data->local_path = merge_path(data->want_home, get_filename_from_path(src_url));
		url = src_url;
		data->get_url_hdr(url);
	}else {
		url = data->url_header + src_url;
		data->local_path = merge_path(data->want_home, get_filename_from_path(src_url));
	}
	savefile(url.c_str(), data->local_path.c_str());
	printf("%s --> %s\n", url.c_str(), data->local_path.c_str());
	return 0;
}

long update(std::string url, std::string vlib_home, std::string want_home, std::string licence) {
	if(url.empty() || vlib_home.empty() || want_home.empty() || licence.empty())
		return E_INVALIDARG;

	std::string filename = merge_path(want_home, get_filename_from_path(url));
	savefile(url.c_str(), filename.c_str());

	void* up = lame_vlibup_create();
	if(!up) return E_FAIL;

	dlcb_data data(want_home);
	std::string cfg_name;
	long hr;
	if(stricmp(url.c_str()+url.size()-3, "xml") == 0) {
		hr = lame_vlibup_getcfg(filename.c_str(), save_to_file_cb, &data);
		if(hr < 0) {
			lame_vlibup_free(up);
			return hr;
		}
		cfg_name = data.local_path;
	}else {
		data.get_url_hdr(url);
		cfg_name = filename;
	}
	hr = lame_vlibup_init(up, vlib_home.c_str(), cfg_name.c_str(), save_to_file_cb, &data);
	if(hr < 0) {
		lame_vlibup_free(up);
		return hr;
	}
	hr = lame_vlibup_update(up, want_home.c_str());
	if(hr < 0) {
		lame_vlibup_free(up);
		return hr;
	}
	lame_vlibup_free(up);
	return 0;
}

long get_list_and_download(std::string cfg_url, std::string vlib_home, std::string want_home) {
	if(cfg_url.empty() || vlib_home.empty() || want_home.empty())
		return E_INVALIDARG;

	std::string filename = merge_path(want_home, get_filename_from_path(cfg_url));
	savefile(cfg_url.c_str(), filename.c_str());

	dlcb_data data(want_home);
	data.get_url_hdr(cfg_url);

	long hr;
	void* up = lame_vlibup_create();
	if(!up) return E_FAIL;
	hr = lame_vlibup_init(up, vlib_home.c_str(), filename.c_str(), save_to_file_cb, &data);
	lame_vlibup_free(up);
	return hr;
}

int main(int argc, char* argv[])
{
	if(argc != 4) {
		printf("lame.libup {xml/cfg url} {lib home} {download home} {licence path}\n");
		return -1;
	}

	char* url = argv[1];
	char* vlib_home = argv[2];
	char* want_home = argv[3];
	char* licence = argv[4];
	//get_list_and_download(url, vlib_home, want_home);
	long hr = update(url, vlib_home, want_home, licence);
	switch (hr)
	{
	case 0:
		printf("viruslib update successfully.\n");
		break;
	case 1:
		printf("viruslib is lastest version.\n");
		break;
	default:
		printf("viruslib update failed.\n");
		break;
	}
	return hr;
}
