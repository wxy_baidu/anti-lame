#ifndef __LAME_XFILE_H__

#if defined(__cplusplus)

class XFile : public lam_xfile_face
{
public:
	std::string _fname;
	FILE* handle;
	XFile():handle(0)
	{
	}

	~XFile()
	{
		close();
	}

	long open(LPCSTR fname)
	{
		close();
		if (!fname) return E_INVALIDARG;
		handle = fopen(fname , "rb+");
		if (!handle) return E_INVALIDARG;

		fseek(handle , 0 , SEEK_SET);

		_fname.assign(fname);

		return S_OK;
	}

	void close()
	{
		if (handle)
		{
			fclose(handle);
			handle = 0;
		}
	}


	virtual long read(LPVOID lpBuffer, DWORD cbBytesToRead, OUT LPDWORD lpBytesRead OPTIONAL)
	{
		if (!lpBuffer || cbBytesToRead  == 0 || !handle) return E_INVALIDARG;

		uint32_t size = fread(lpBuffer , 1 , cbBytesToRead , handle);
		if (lpBytesRead)
		{
			*lpBytesRead = size;
		}

		return S_OK;
	}
	virtual long write(LPCVOID lpBuffer, DWORD cbBytesToWrite,OUT LPDWORD lpBytesWritten OPTIONAL)
	{
		if (!lpBuffer || !handle) return E_INVALIDARG;

		if (cbBytesToWrite == 0) return S_OK;


		uint32_t size = fwrite(lpBuffer , 1 , cbBytesToWrite , handle);
		if (lpBytesWritten)
		{
			*lpBytesWritten = size;
		}
		return S_OK;
	}
	virtual long flush()
	{
		if (!handle) return E_INVALIDARG;

		fflush(handle);

		return S_OK;
	}
	virtual long seek(LONG lDistanceToMove,PLONG lpDistanceToMoveHigh OPTIONAL,DWORD dwMoveMethod)
	{

		if (!handle) return E_INVALIDARG;

		fseek(handle ,lDistanceToMove ,dwMoveMethod);

		return S_OK;
	}
	virtual long tell(OUT LPDWORD lpOffsetLow,OUT LPDWORD lpOffsetHigh OPTIONAL)
	{
		if (!handle || !lpOffsetLow) return E_INVALIDARG;

		*lpOffsetLow = ftell(handle);

		return S_OK;
	}
	virtual long get_size(OUT LPDWORD lpSizeLow,OUT LPDWORD lpSizeHigh OPTIONAL)
	{
		if (!handle || !lpSizeLow) return E_INVALIDARG;

		DWORD pos = ftell(handle);

		fseek(handle , 0 , SEEK_END);

		*lpSizeLow = ftell(handle);

		fseek(handle , pos , SEEK_SET);

		return S_OK;
	}
	virtual long set_size(DWORD dwSizeLow, LPDWORD lpSizeHigh OPTIONAL)
	{
		if (!handle) return E_INVALIDARG;


		DWORD size = dwSizeLow;

		int fno = fileno(handle);
		chsize(fno ,size);

		return S_OK;
	}
	virtual long remove()
	{
		return E_FAIL;
	}
	virtual const char* get_name()
	{
		if (!handle) return 0;

		return (const char*)_fname.c_str();
	}
	virtual const wchar_t* get_name_w()
	{
		return 0;
	}
};



#else

//struct PhysicalFile
//{
//	std::string _fname;
//	FILE* handle;
//	PhysicalFile():handle(0)
//	{
//	}
//
//	~PhysicalFile()
//	{
//		close();
//	}
//
//	long open(LPCSTR fname)
//	{
//		close();
//		if (!fname) return E_INVALIDARG;
//		handle = fopen(fname , "rb+");
//		if (!handle) return E_INVALIDARG;
//
//		fseek(handle , 0 , FILE_BEGIN);
//
//		_fname.assign(fname);
//
//		return S_OK;
//	}
//
//	void close()
//	{
//		if (handle)
//		{
//			fclose(handle);
//			handle = 0;
//		}
//	}
//};
//
//
//long STDCALL physical_file_read(lame_xhandle_t handle , LPVOID lpBuffer, DWORD cbBytesToRead, OUT LPDWORD lpBytesRead OPTIONAL)
//{
//
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!lpBuffer || cbBytesToRead  == 0 || !h) return E_INVALIDARG;
//
//	uint32_t size = fread(lpBuffer , 1 , cbBytesToRead , h->handle);
//	if (lpBytesRead)
//	{
//		*lpBytesRead = size;
//	}
//
//	return S_OK;
//}
//
//long STDCALL physical_file_write(lame_xhandle_t handle , LPCVOID lpBuffer, DWORD cbBytesToWrite,OUT LPDWORD lpBytesWritten OPTIONAL)
//{
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!lpBuffer || !h) return E_INVALIDARG;
//
//	if (cbBytesToWrite == 0) return S_OK;
//
//
//	uint32_t size = fwrite(lpBuffer , 1 , cbBytesToWrite , h->handle);
//	if (lpBytesWritten)
//	{
//		*lpBytesWritten = size;
//	}
//
//
//	return S_OK;
//}
//
//long STDCALL physical_file_flush(lame_xhandle_t handle)
//{
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!h) return E_INVALIDARG;
//
//	fflush(h->handle);
//
//	return S_OK;
//}
//
//
//long STDCALL physical_file_seek(lame_xhandle_t handle , LONG lDistanceToMove,PLONG lpDistanceToMoveHigh OPTIONAL,DWORD dwMoveMethod)
//{
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!h) return E_INVALIDARG;
//
//	fseek(h->handle ,lDistanceToMove ,dwMoveMethod);
//	return S_OK;
//}
//
//
//long STDCALL physical_file_tell(lame_xhandle_t handle , OUT LPDWORD lpOffsetLow,OUT LPDWORD lpOffsetHigh OPTIONAL)
//{
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!h || !lpOffsetLow) return E_INVALIDARG;
//
//	*lpOffsetLow = ftell(h->handle);
//
//	return S_OK;
//
//}
//
//
//long STDCALL physical_file_get_size(lame_xhandle_t handle , OUT LPDWORD lpSizeLow,OUT LPDWORD lpSizeHigh OPTIONAL)
//{
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!h || !lpSizeLow) return E_INVALIDARG;
//
//	DWORD pos = ftell(h->handle);
//
//	fseek(h->handle , 0 , FILE_END);
//
//	*lpSizeLow = ftell(h->handle);
//
//	fseek(h->handle , pos , FILE_BEGIN);
//
//	return S_OK;
//}
//
//
//long STDCALL physical_file_set_size(lame_xhandle_t handle , DWORD dwSizeLow, LPDWORD lpSizeHigh OPTIONAL)
//{
//
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!h) return E_INVALIDARG;
//
//
//	DWORD size = dwSizeLow;
//
//	int fno = fileno(h->handle);
//	chsize(fno ,size);
//
//	return S_OK;
//}
//
//
//long STDCALL physical_file_remove(lame_xhandle_t handle)
//{
//	return E_FAIL;
//}
//
//
//const char* STDCALL physical_file_get_name(lame_xhandle_t handle)
//{
//	PhysicalFile* h = (PhysicalFile*)handle;
//	if (!h) return 0;
//
//	return (const char*)h->_fname.c_str();
//}
//
//
//
//const wchar_t* STDCALL physical_file_get_name_w(lame_xhandle_t handle)
//{
//	return 0;
//}
//


#endif





#endif