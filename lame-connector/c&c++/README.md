* [工程说明](#工程说明)
   * [lame.scanner](#lame.scanner)
   * [lame.mt.scanner](#lame.mt.scanner)
   * [xlame.scanner](#xlame.scanner)
   * [lame.extractor](#lame.extractor)
   * [lame.v4.scanner](#lame.v4.scanner)
   * [lame.libup](#lame.libup)
   * [lame.homeup](#lame.homeup)


# 工程说明
---

- **lame.scanner**

   单线程扫描文件示例,里面有获取病毒库版本号,授权文件信息,根据病毒库配置文件获取病毒库版本号等.


- **lame.mt.scanner**

   多线程扫描文件示例.此工程代码需要c++11的支持,要求的VS或者GCC的版本要高一些.


- **xlame.scanner**

   连云扫描示例程序.


- **lame.extractor**

   文件解包示例程序,可把复合文档类文件中的子文件,逐一解析dump出来.


- **lame.v4.scanner**

   V4版引擎接口示例程序,可更加深入的控制扫描流程.


- **lame.libup**

   病毒库客户端升级示例程序.

- **lame.homeup**

   病毒库服务器升级示例程序.
