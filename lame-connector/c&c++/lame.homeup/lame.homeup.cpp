// scan.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <lame.v2.h>


#define BUILDING_LIBCURL
#define HTTP_ONLY
#include "../curl/curl.h"
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "wldap32.lib")
#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "curl.lib")

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream){
	return fwrite(ptr, size, nmemb, (FILE*)stream);
}

class CDownload {
private:
	CURL* curl;
public:
	CDownload(): curl(NULL){ curl = curl_easy_init(); }
	~CDownload(){ if(curl) curl_easy_cleanup(curl); }
	HRESULT get( const char* url, char* filename) {
		HRESULT hr = E_FAIL;
		FILE *	fp = NULL;
		do {
			if( !url ) break;
			CURLcode lc = curl_easy_setopt( curl, CURLOPT_URL, url );
			if( CURLE_OK != lc ) break;

			lc = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
			if( CURLE_OK != lc ) break;

			lc = curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
			if( CURLE_OK != lc ) break;

			const char* lpTmpfilename = '\0' != filename[0] ? filename : "virlib.tmp";
			fp = fopen(lpTmpfilename,"wb");
			if( NULL == fp ) break;

			lc = curl_easy_setopt( curl, CURLOPT_WRITEDATA, fp );
			if( CURLE_OK != lc ) break;

			lc = curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, write_data );
			if( CURLE_OK != lc ) break;

			lc = curl_easy_perform( curl );
			if( CURLE_OK != lc ) break;

			if(fp) fclose(fp); fp = NULL;
			if( '\0' == filename[0] ) {	strcpy( filename, lpTmpfilename ); }
			hr = S_OK;
		} while (false);
		if(fp) fclose(fp);
		return hr;
	}
};

std::string get_filename_from_path(std::string str){
#ifdef _WIN32
	size_t pos = str.rfind('\\');
	if(-1 == pos) {
		pos = str.rfind('/');
	}
#else
	size_t pos = str.rfind('/');
#endif
	if(-1 == pos) return "";
	return std::string(str, pos+1, str.size()-pos);
}

std::string merge_path(std::string a, std::string b) {
	char c = a[a.size()-1];
	if('\\' != c || '/' != c){
#ifdef _WIN32
		return a + "\\" + b;
#else
		return a + "/" + b;
#endif
	} else {
		return a + b;
	}
}

typedef struct dlcb_data{
	std::string local_root;
	std::string local_path;
	std::string url_header;
	std::vector<std::string> list;
	BOOL _bCreateDirectory;

	dlcb_data():_bCreateDirectory(FALSE){}
	long get_url_hdr(std::string &str) {
		size_t pos = str.find("viruslib/");
		if(-1 != pos) {
			url_header.assign(str, 0, pos+9);
			return S_OK;
		}
		return E_FAIL;
	}
	void MakeDirectory( std::string aWanntFile )
	{
		if( _bCreateDirectory ) return;

		std::string _rootdir, localdirectory;
		int pos = aWanntFile.find( '/', 0 );
		_rootdir.assign( aWanntFile, 0, pos );
		localdirectory = local_root + "/" + _rootdir;

		CreateDirectoryA( localdirectory.c_str(), 0 );

		localdirectory = local_root + "/" + _rootdir + "/dat";
		CreateDirectoryA( localdirectory.c_str(), 0 );

		localdirectory = local_root + "/" + _rootdir + "/cfg";
		CreateDirectoryA( localdirectory.c_str(), 0 );
		_bCreateDirectory = TRUE;
	}
	void push(std::string f) {
		list.push_back(f);
	}
#ifndef _WIN32
	BOOL CreateDirectoryA( const char* filename, long ){
		return 0 == mkdir( filename, 0755 );
	}
#endif
} dlcb_data;

long savefile(const char* url, const char* path) {
	CDownload dl;
	return dl.get(url, (char*)path);
}

long LAMESTDCALLTYPE save_to_file_cb(void* user_data, const char* src_url, int need_download) {
	std::string url;
	dlcb_data* data = (dlcb_data*)user_data;
	data->push(src_url);

	if(strnicmp(src_url, "http", 4) == 0) {
		data->local_path = merge_path(data->local_root, get_filename_from_path(src_url));
		url = src_url;
		data->get_url_hdr(url);
	}else {
		url = data->url_header + src_url;
		data->local_path = merge_path(data->local_root, src_url);
	}
	if (need_download) {
		data->MakeDirectory(src_url);
		savefile(url.c_str(), data->local_path.c_str());
	}
	printf("%s --> %s\n", url.c_str(), data->local_path.c_str());
	return 0;
}

long update(std::string url, std::string local_root) {
	if(url.empty() || local_root.empty())
		return E_INVALIDARG;

	std::string filename = merge_path(local_root, get_filename_from_path(url));
	savefile(url.c_str(), filename.c_str());

	dlcb_data data;
	data.local_root = local_root;
	data.get_url_hdr(url);

	void* up = lame_vhomeup_create();
	if(!up) return E_FAIL;

	std::string cfg_name;
	long hr;
	if(stricmp(url.c_str()+url.size()-3, "xml") == 0) {
		hr = lame_vhomeup_getcfg(filename.c_str(), save_to_file_cb, &data);
		if(hr < 0) {
			lame_vhomeup_free(up);
			return hr;
		}
		cfg_name = data.local_path;
	}else {
		cfg_name = filename;
	}
	hr = lame_vhomeup_update(up, cfg_name.c_str(), local_root.c_str(), save_to_file_cb, &data);
	if(hr < 0) {
		lame_vhomeup_free(up);
		return hr;
	}
	lame_vhomeup_free(up);
	return S_OK;
}

int main(int argc, char* argv[])
{
	if(argc != 3) {
		printf("lame.homeup {xml/cfg url} {local root}\n");
		return -1;
	}

	char* url = argv[1];
	char* local_root = argv[2];
	long hr = update(url, local_root);
	if (hr == S_OK) {
		printf("viruslib rp files update successfully.\n");
	} else {
		printf("viruslib rp files update failed.\n");
	}
	return hr;
}
