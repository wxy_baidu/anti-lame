// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifdef _WIN32
#include <stdio.h>
#include <tchar.h>
#include <io.h>
#else
#include <sys/stat.h>
typedef long HRESULT;
typedef const char* LPCSTR;
typedef int BOOL;
#define TRUE				1
#define FALSE				0
#define E_INVALIDARG		long(0x80070057L)
#define E_FAIL				long(0x80004005L)
#define S_OK				long(0x0L)
#define stricmp				strcasecmp
#define strnicmp			strncasecmp
#define OUT
#define OPTIONAL
#define chsize				ftruncate
typedef unsigned int DWORD;
typedef unsigned int* LPDWORD;
typedef void* LPVOID;
typedef const void* LPCVOID;
typedef long LONG;
typedef long* PLONG;
#endif


// TODO: reference additional headers your program requires here
