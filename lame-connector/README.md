# 说明

此目录中包含有各个开发语言封装的SDK接口及示例代码, 详情请见各子目录. 若没有您使用的开发语言示例代码, 还请留言, 我们会努力完善.

# **开发语言**

目前已经适配的语言：

- [C/C++](#C/C++)
- [JAVA](#JAVA)
- [Python(CPytohn)](#Python(CPytohn))
- [dotNet](#dotNet)
- [Nodejs](#Nodejs)
- [go](#go)
- [php](#php)

## C/C++

```c
EXTERN_C void*  lame_open_vdb(const char* vlibf);
EXTERN_C void   lame_close_vdb(void* vdb);

EXTERN_C void*  lame_create(void* vdb);
EXTERN_C void   lame_destroy(void* lame);
EXTERN_C void*  lame_fork(void* lame);

EXTERN_C long   lame_param_set(void* lame , const char*  param);
EXTERN_C long   lame_init(void* lame);
EXTERN_C long   lame_scan_file(void* lame , const char* fname , lame_scan_result* pResult);
EXTERN_C long   lame_scan_mem(void* lame , uint8_t* data , uint32_t size , lame_scan_result* pResult);


EXTERN_C long   lame_scan_file_with_callback(void* lame , const char* fname , lame_callback cb , void* user_data);
EXTERN_C long   lame_scan_mem_with_callback(void* lame , uint8_t* data , uint32_t size , lame_callback cb , void* user_data);

EXTERN_C long   lame_get_version(lame_info* info);
EXTERN_C long   lame_get_licence_info(rx_licence_info* info);
```
## JAVA

```java
public interface ScanFeedback {
	public abstract long DtEnterFile(String file_name, long depth, Object usr_data);
	public abstract void DtLeaveFile(String file_name, long depth, Object usr_data, long l);
	public abstract long DtAlarm(String file_name, ScanResult sr, Object usr_data);
}

public class Lame extends RavEngine {
	public long OpenVdb(String vdbf);
	public void CloseVdb();
	public boolean Load(long lib);
	public boolean Set(String param);
	public void Unload();
	public LicenceInfo GetLicence();
	public LameInfo GetVersion();
	public ScanResult ScanFile(String fname);
	public ScanResult ScanMem(byte [] data);
	public boolean ScanFileWithCallback(String fname,ScanFeedback cb, Object usrdata);
	public boolean ScanMemWithCallback(byte [] data,ScanFeedback cb, Object usrdata);
}
```
## Python

```python
class LameBase:
	def SetParam(self, param)
	def SetParameter(self, param)
	def Load(self, vdb_object)
	def GetVersion(self)
	def GetLicense(self)
	def Unload(self)

class Lame(LameBase):
	def ScanFile(self,fname)
	def ScanMem(self, data)
	def Clone(self)

class LameWithFeedback(LameBase):
	def SetCallack(self, enter_file, leave_file, alram)
	def ScanFile(self, fname)
	def ScanMem(self, data)
	def Clone(self)
```
## dotNet

```c
public class Lame : LameBase
{
	public LameScanResult ScanFile(string sFile);
	public LameScanResult ScanMem(byte[] bytes);
	public object Clone();
}

public delegate void ScanInternalFileEvent(string fname, LameScanResult result);

public class LameWithEvent : LameBase {
	public void ScanFile(string sFile);
	public void ScanMem(byte[] bytes);
	private void ScanInternalFile(string fname, IntPtr resp, IntPtr zero);
	public object Clone();
	private LameScanResult FetchResult(IntPtr resp);
}

public class VirusLib
{
	public bool lame_open_vdb(string vdbf);
	public void lame_close_vdb();
	public IntPtr vdb_handle;
}
```

## Nodejs

```javascript
lame.prototype.GetVersionInfo();
lame.prototype.GetLicenceInfo();

lame.prototype.OpenVdb(vlib);
lame.prototype.CloseVdb();

lame.prototype.ExtractFile(filename, password, enter_file, leave_file, userdata);
lame.prototype.Extract_GetSize(handle);
lame.prototype.Extract_Seek(handle, offset, method);
lame.prototype.Extract_Tell(handle);
lame.prototype.Extract_Read(handle, size);

lame.prototype.Load();
lame.prototype.SetParameters(param);
lame.prototype.Unload();
lame.prototype.ScanFile(path);
lame.prototype.ScanMem(data, size);
lame.prototype.ScanFileWithCallback(path, enter_file, leave_file, alarm, userdata);
lame.prototype.ScanMemWithCallback(data, size, enter_file, leave_file, alarm, userdata);
```

## go

```go
func (l *Lame) OpenVdb(vlib string) unsafe.Pointer
func (l *Lame) CloseVdb()
func (l *Lame) Create(vdb unsafe.Pointer) unsafe.Pointer
func (l *Lame) Destory()
func (l *Lame) Init() bool
func (l *Lame) SetParam(param string) bool
func (l *Lame) ScanFile(path string) *DetectResult
func (l *Lame) ScanMem(data [] byte, size int) *DetectResult
func (l *Lame) ScanFileWithCallback(path string, cb ScanCallBack, userdata interface{}) bool
func (l *Lame) ScanMemWithCallback(data [] byte, size int, cb ScanCallBack, userdata interface{}) bool
func (l *Lame) GetVersion() *LameVersion
func (l *Lame) GetLicenceInfo() *LicenceInfo
func (l *Lame) ExtractFile(filename string, password string, cb ExtractCallBack, userdata interface{}) bool
func (l *Lame) ExtractGetSize(handle unsafe.Pointer) int
func (l *Lame) ExtractSeek(handle unsafe.Pointer, offset int, method int32) bool
func (l *Lame) ExtractTell(handle unsafe.Pointer) int32
func (l *Lame) ExtractRead(handle unsafe.Pointer, buf [] byte, size uint32) uint32
```

## php

```php
class Lame
{
	function open_vdb($vlib_path = "");
	function create();
	function release(&$handle);
	function close_vdb(&$vdb);
	function set_parameter($param);
	function load();
	function unload();
	function scan($filename);
	function scan_with_callback($filename, $enter_file, $leave_file, $alarm, &$user_data);
	function scan_memory($contents);
	function scan_memory_with_callback($contents, $enter_file, $leave_file, $alarm, &$user_data);
	function extract_file($filename, $enter_file, $leave_file, &$user_data);
	function extract_get_size($handle);
	function extract_seek($handle, $offset, $method);
	function extract_tell($handle);
	function extract_read($handle, $size);
	function get_version();
	function get_licence_info();
}
```
