﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace lame.dotnet
{
    class ClameUtil 
    {
        public static IntPtr[] String2UnmanageArray(string[] file_names)
        {
            IntPtr[] parry = new IntPtr[file_names.Length];

            if (file_names == null || file_names.Length == 0) return null;

            for (int idx = 0; idx < file_names.Length; idx++)
            {
                parry[idx] = Marshal.StringToHGlobalAnsi(file_names[idx]);
            }

            return parry;
        }

        public static void ClearUnmanageArray(ref IntPtr[] pArray)
        {
            try
            {
                for (int idx = 0; idx < pArray.Length; idx++)
                {
                    if (pArray[idx] == IntPtr.Zero) continue;
                    Marshal.FreeHGlobal(pArray[idx]);

                    pArray[idx] = IntPtr.Zero;
                }

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
    public class Clame
    {
        [System.Runtime.InteropServices.DllImportAttribute("clame.dll", EntryPoint = "clame_mem_free", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void clame_mem_free_(System.IntPtr p);

        [System.Runtime.InteropServices.DllImportAttribute("clame.dll", EntryPoint = "clame_cloud_signatue_file", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        private static extern int clame_cloud_signatue_file_([InAttribute()] [MarshalAsAttribute(UnmanagedType.LPStr)] string json_user_info,  ref System.IntPtr fnames, int count, ref System.IntPtr result, ref int rsize);

       
        public Clame() 
        {
           
        }

        public bool clame_cloud_signatue_file(string[] file_names, out byte[] bytes , string json_user_info = null) 
        {
            bytes = null;
            IntPtr[] c_file_names = ClameUtil.String2UnmanageArray(file_names);
            IntPtr pdata = IntPtr.Zero;
            try
            {
                if (c_file_names == null) return false;

                int data_size = 0;
                long l = clame_cloud_signatue_file_(json_user_info, ref c_file_names[0], file_names.Length, ref pdata, ref data_size);
                if (l != 0 || data_size == 0) return false;

                bytes = new byte[data_size];
                Marshal.Copy(pdata, bytes, 0, data_size);
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
                if (pdata != IntPtr.Zero)
                {
                    clame_mem_free_(pdata);
                }

                ClameUtil.ClearUnmanageArray(ref c_file_names);
            }

      

            return true;
        }


    }


    public delegate long ClameFetchHashEvent(string fname, byte[] data);

    public class ClameWithEvent
    {
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate long clame_fetch_callback_(System.IntPtr user_data, int index, System.IntPtr packet, int size);


        [System.Runtime.InteropServices.DllImportAttribute("clame.dll", EntryPoint = "clame_fetch_batch", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int clame_fetch_batch_([InAttribute()] [MarshalAsAttribute(UnmanagedType.LPStr)] string flags, ref System.IntPtr pfiles, uint count, [MarshalAs(UnmanagedType.FunctionPtr)] clame_fetch_callback_ pcb, ref System.IntPtr user_data);

        public ClameFetchHashEvent FetchHashEvent;
        public ClameWithEvent()
        {
            FetchHashEvent = DefaultClameFetchHashEvent;
        }

        public bool clame_fetch_batch(string[] file_names, string flags = null)
        {
            IntPtr[] c_file_names = ClameUtil.String2UnmanageArray(file_names);
            IntPtr pdata = IntPtr.Zero;

            try
            {
                if (c_file_names == null) return false;

                clame_fetch_callback_ cb = new clame_fetch_callback_(clame_fetch_callback_handle);

                long l = clame_fetch_batch_(flags, ref c_file_names[0], (uint)file_names.Length, cb, ref c_file_names[0]);
                if (l != 0) return false;
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
                ClameUtil.ClearUnmanageArray(ref c_file_names);
            }



            return true;
        }


        public long clame_fetch_callback_handle(System.IntPtr user_data, int index, System.IntPtr packet, int size)
        {
            if (size == 0) return 0;
            
            byte[] bytes = new byte[size];
            Marshal.Copy(packet, bytes, 0, size);
            IntPtr p = Marshal.ReadIntPtr(user_data, index * 4);

            string fname = Marshal.PtrToStringAnsi(p);

            return FetchHashEvent(fname , bytes);
        }


        private long DefaultClameFetchHashEvent(string fname, byte[] data) 
        {
            return 0;
        }

    }
}
