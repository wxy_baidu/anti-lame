﻿using System;
using System.IO;

namespace lame.dotnetcore.sample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("\tlame.dotnet.sample [viruslib-path] [licence-path] [target-path]");
                return;
            }

            string virus_path = args[0];
            string licence_path = args[1];
            string target_path = args[2];

            var vdb = new VirusLib();
            if (!vdb.lame_open_vdb(virus_path))
            {
                Console.WriteLine("Faild to load virus lib.");
                return;
            }

            LameTest(vdb, licence_path, target_path);

            LameWithEventTest(vdb, licence_path, target_path);

            vdb.lame_close_vdb();
        }

        private static void PrintScanResult(string path, LameScanResult result)
        {
            if (result == null) return;

            Console.Write(path);
            if (result != null)
            {
                Console.Write("     Infected:" + result.VirusName + " (" + result.EngineID + ")");
            }
            Console.WriteLine("");
        }

        private static void LameTest(VirusLib vdb, string licence, string path)
        {
            if (vdb == null) return;

            var _lame = new Lame();
            // 1. set
            if (0 != licence.Length)
            {
                _lame.SetParameters("license-path=" + licence);
            }

            // 2. load
            if (!_lame.Load(vdb)) return;

            // 3. scan
            if (File.Exists(path))
            {
                var _result = _lame.ScanFile(path);
                PrintScanResult(path, _result);
            }
            else if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path);
                foreach (var f in files)
                {
                    var _result = _lame.ScanFile(f);
                    PrintScanResult(f, _result);
                }

                //travel dir......
            }

            //3.
            _lame.Unload();
        }

        private static LSCT AlarmEventHandle(string file, LameScanResult result)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(file);
            if (result != null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("     Infected:" + result.VirusName + " (" + result.EngineID + ")");
            }
            Console.WriteLine("");
            return LSCT.CONTINUE;
        }

        private static LSCT EnterFileEventHandle(string fname, uint depth)
        {
            return LSCT.CONTINUE;
        }

        private static void LeaveFileEventHandle(string fname, uint depth)
        {
        }

        private static void LameWithEventTest(VirusLib vdb, string licence, string path)
        {
            if (vdb == null) return;

            var _lame = new LameWithEvent();

            // 1. set
            if (0 != licence.Length)
            {
                _lame.SetParameters("license-path=" + licence);
            }

            _lame.EnterFileEvent = EnterFileEventHandle;
            _lame.LeaveFileEvent = LeaveFileEventHandle;
            _lame.AlarmEvent = AlarmEventHandle;

            // 2. load
            if (!_lame.Load(vdb)) return;

            // 3. scan
            if (File.Exists(path))
            {
                _lame.ScanFile(path);
            }
            else if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path);
                foreach (var f in files)
                {
                    _lame.ScanFile(f);
                }

                //travel dir
            }

            //3.
            _lame.Unload();
        }
    }
}