* [工程说明](#工程说明)
  * [lame.py](#lame.py)
  * [lame.scanner.py](#lame.scanner.py)
  * [lame.mt.scanner.py](#lame.mt.scanner.py)
  * [xlame.scanner.py](#xlame.scanner.py)
  * [lame.sample.py](#lame.sample.py)
  * [lame.extractor.py](#lame.extractor.py)


# 工程说明
___

- **lame.py**

   使用python封装的扫描类接口.

- **lame.scanner.py**

   使用lame.py接口写的一个单线程扫描器,可直接使用扫描文件.

- **lame.mt.scanner.py**

   使用lame.py接口写的一个多线程扫描器,可直接使用扫描文件.

- **xlame.scanner.py**

   使用lame.py接口封装的云和本地引擎同时扫描的简单示例.

- **lame.sample.py**

   使用lame.py的简单示例.

- **lame.extractor.py**

   使用lame.py接口封装的文件解包程序,可用于对复合文档进行文件解压.


## lame.py
___

病毒库相关类(加载、释放)

```python
class VirusDb:
	def __init__(self, lpath);
	def OpenVdb(self,vdbf);
	def CloseVdb(self);
	def GetVbdHandle(self);
```

不带回调函数的扫描类

```python	
class Lame(LameBase):
	def __init__(self, lpath);
	def ScanFile(self,fname);
	def ScanMem(self, data);
	def Clone(self);
```

带回调函数的扫描类

```python
CMPFUNC = WINFUNCTYPE(None,c_char_p, POINTER(LameScanResult),c_void_p) 
def LameCallback(fname, result, userdata);

class LameWithFeedback(LameBase):
	def __init__(self, lpath);
	def SetCallack(self,cb);
	def Report(self, fname, result);
	def ScanFile(self,fname);
	def ScanMem(self, data );
	def Clone(self);
```


## lame.scanner.py
___

不带回调函数的扫描类

```python
class LameScanner:
    def __init__(self, lpath);
    def Load(self, vdb);
    def SetParam(self, param);
    def Scan(self, path_);
    def Unload(self);
    def ShowLicense(self, licence_path=None);
    def ShowVersion(self, virlib_path=None);
```

带回调函数的扫描类

```python
class LameScanner2:
    def __init__(self, lpath);
    def Load(self, vdb);
    def SetParam(self, param);
    def Unload(self);
    def ShowLicense(self, licence_path=None);
    def ShowVersion(self, virlib_path=None);
    def Scan(self, path_);
```

## lame.mt.scanner.py
___

```python
class scanner:
    def __init__(self, lpath);
    def load(self, param_lst);
    def unload(self);
    def run(self, sc);
    def append_scan_path(self, path);
```

## xlame.scanner.py
___

```python
class XScanner:
    def load(self, cloud_engine = 1,local_engine = 2,vlib_path = None);
    def process(self, path);
    def unload(self);
    def __fetch_cloud_feture(self);
    def __https_request(self,data);
    def __cloud_query_thread(self);
    def __process_cloud_result(self, fnames,result);
    def __push_to_scan_q(self, files);
    def __process_file_or_dir(self, file_or_path);
    def __process_path_thread(self);
    def __lame_scan_thread(self, _lame);
    def __display_result(self, fname,result);
```

## lame.sample.py
___

简单示例代码

```python
def PrintLameScanResult(fname, result):
    sys.stdout.write(fname)
    if result is not None:
        sys.stdout.write(" " + "  Infected: " + format_bstring(result.vname) + " (" + format_bstring(result.engid) + ")")
    sys.stdout.write("\n")

def print_lame_licence_info(_lame, licence=None):
    _license = _lame.GetLicense(licence)
    if _license is not None:
        print("License:")
        print("     Version: " + format_bstring(_license.Version))
        print("     Owner  : " + format_bstring(_license.Owner,'gb2312'))
        print("     Date   : " + format_bstring(_license.Date))
        print("     Authm  : " + format_bstring(_license.Authm))
        print("     Data  :  " + format_bstring(_license.Data) + "\n")
    return

def print_lame_version_info(_lame, vdbf=None):
    _version = _lame.GetVersion(vdbf)
    if _version is not None:
        print("Engine Version    : " + format_bstring(_version.engv))
        print("Viruse Lib Version: " + format_bstring(_version.vdbv) + "\n")
    return

def lame_test(dbf, lame_path, licence_path, scan_path):
    _lame = Lame(lame_path)

    _lame.SetParam("license-path=" + licence_path)
    #_lame.SetParam("kill")

    if not _lame.Load(dbf):
        sys.stdout.write("load lame failed.")
        return

    print_lame_licence_info(_lame, licence_path)
    print_lame_version_info(_lame)

    if os.path.isfile(scan_path) :
        _result = _lame.ScanFile(scan_path)
        PrintLameScanResult(scan_path, _result)
    else:
        for dirpath,dirnames,filenames in os.walk(scan_path):
            for file in filenames:
                fullpath=os.path.join(dirpath,file)
                _result = _lame.ScanFile(fullpath)
                PrintLameScanResult(fullpath, _result)
    _lame.Unload()
    return

def enter_file(fname, depth, _lame):
    _lame._virus_name = None
    return LameWithFeedback.LSCT_CONTINUE

def leave_file(fname, depth, l, _lame):
    sys.stdout.write(format_bstring(fname))
    if _lame._virus_name is not None:
        sys.stdout.write(" " + "  Infected: " + format_bstring(_lame._virus_name))
        _lame._virus_name = None
    sys.stdout.write("\n")
    return

def alarm(fname, result, _lame):
    if result is not None:
        _lame._virus_name = result.vname
    return LameWithFeedback.LSCT_CONTINUE

def lame_with_feedback_test(dbf, lame_path, licence_path, scan_path):
    if dbf is None:
        return

    _lame = LameWithFeedback(lame_path)
    _lame.SetCallack(enter_file, leave_file, alarm)

    _lame.SetParam("license-path=" + licence_path)
    #_lame.SetParam("kill")

    if not _lame.Load(dbf):
        return

    print_lame_licence_info(_lame, licence_path)
    print_lame_version_info(_lame)

    if os.path.isfile(scan_path) :
        _lame.ScanFile(scan_path)
    else:
        for dirpath,dirnames,filenames in os.walk(scan_path):
            for file in filenames:
                fullpath=os.path.join(dirpath,file)
                _lame.ScanFile(fullpath)
    _lame.Unload()
    return


def main(path):
    if path is None:
        return

    _scan_path_count = len(path)
    if _scan_path_count == 0:
        return

    print('\n---- test start ----\n')

    lame_path = path[0]
    virlib_path = path[1]
    licence_path = path[2]
    scan_path = path[3]

    _dbf = VirusDb(lame_path)
    if not _dbf.OpenVdb(virlib_path):
        print('Faild to openvdb.')
        return

    print('\n---- normal sample ----\n')
    lame_test(_dbf, lame_path, licence_path, scan_path)

    print('\n---- callback sample ----\n')
    lame_with_feedback_test(_dbf, lame_path, licence_path, scan_path)

    _dbf.CloseVdb()

    print('\n---- test end ----\n')
    return


def help():
    print('Usage:')
    print(' lame.sample.py lame_path vlib_path licence_path target_path')
    return

def exit(signum, frame):
    sys.exit()

# sys.argv.append('D:\\MyJob\\SDK\\produce\\make\\SDK_build_results\\lame-win-x64')
# sys.argv.append('D:\\MyJob\\SDK\\produce\\make\\malware.rmd')
# sys.argv.append('D:\\MyJob\\SDK\\produce\\make\\licence.json')
# sys.argv.append('D:\\rsdata\\1\\11')

if  __name__ == "__main__":
    signal.signal(signal.SIGINT, exit)
    signal.signal(signal.SIGTERM, exit)
    if len(sys.argv) == 1:
        help()
    else:
        main(sys.argv[1:])
```

## lame.extractor.py
___

```python
class LameDumper:
    def  Load(self);
    def Unload(self);
    def Extract(self, fname, password);
    def __Enter_File(self, immfile);
    def __Leave_File(self, immfile, lxlvt);
    def DumpFile(self, immfile);
```
