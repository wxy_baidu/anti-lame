import os
import sys
import urllib.request
from lame import *

class save_ctx:
    def  __init__(self, want_home):
        self.want_home = want_home
        self.local_path = None
        self.url_hdr = None
        return
    def get_url_hdr(self, url):
        pos = url.find('viruslib/')
        if pos < 0:
            return
        self.url_hdr = url[0:pos+9]
        return

def save(url, filename):
    f = urllib.request.urlopen(url)
    data = f.read()
    with open(filename, "wb") as code:
         code.write(data)
    return

def save_to_file(vlibup, file):
    ctx = vlibup.context
    if 'http' in file:
        ctx.local_path = ctx.want_home + os.sep + os.path.basename(file)
        url = file
        ctx.get_url_hdr(url)
    else:
        url = ctx.url_hdr + file
        ctx.local_path = ctx.want_home + os.sep + os.path.basename(file)
    save(url, ctx.local_path)
    print(url + ' --> ' + ctx.local_path)
    return 0

def update(lame_path, url, vlib_home, want_home, licence):
    if lame_path is None or url is None or vlib_home is None or want_home is None:
        return

    filename = want_home + os.sep + os.path.basename(url)
    save(url, filename)

    ctx = save_ctx(want_home)
    vlibup = LameVlibUp(lame_path)
    vlibup.SetCallack(save_to_file, ctx)
    if not vlibup.Create():
        print('libup Create failed.\n')

    cfg_name = ''
    if 'xml' in url:
        if not vlibup.GetCfg(filename):
            vlibup.Free()
            return
        cfg_name = ctx.local_path
    else:
        ctx.get_url_hdr(url)
        cfg_name = filename

    if not vlibup.InitEx(vlib_home, cfg_name, licence):
        print('libup InitEx failed.\n')
        vlibup.Free()
        return

    if not vlibup.Update(want_home):
        print('libup update failed.\n')
    else:
        print('libup update successfully.\n')

    vlibup.Free()
    return


def main(path):
    if path is None:
        return
    _scan_path_count = len(path)
    if _scan_path_count == 0:
        return
    lame_path = path[0]
    url = path[1]
    vlib_home = path[2]
    want_home = path[3]
    licence = path[4]
    return update(lame_path, url, vlib_home, want_home, licence)

def help():
    print('Usage:')
    print(' lame.libup.py lame_path [xml/cfg url] [vlib home] [want home] [licence path]')
    return

def exit(signum, frame):
    sys.exit()

# sys.argv.append('D:\\MyJob\\SDK\\produce\\make\\SDK_build_results\\lame-win-x64')
# sys.argv.append('http://rsup10.rising.com.cn/viruslib/vlco-fullver.xml')
# sys.argv.append('D:\\MyJob\\SDK\\produce\\make\\SDK_build_results\\lame-win-x64')
# sys.argv.append('D:\\MyJob\\SDK\\produce\\make\\SDK_build_results\\lame-win-x64')
# sys.argv.append('D:\\MyJob\\SDK\\produce\\make\\licence.json')

if  __name__ == "__main__":
    signal.signal(signal.SIGINT, exit)
    signal.signal(signal.SIGTERM, exit)
    if len(sys.argv) == 1:
        help()
    else:
        main(sys.argv[1:])