import os
import sys
import  threading
if 3 == sys.version_info[0]:
    import queue
else:
    import Queue
import time
import requests
import ctypes
from clame import *
from lame import *


class safeq:

    def __init__(self, size=100):
        self.__size = size
        self.__locker = threading.Lock()
        self.__queue = list()

    def put(self, s, force=True):

        if s is None:
            return False

        ret = False

        if force:
            while True:

                self.__locker.acquire()

                if len(self.__queue) < self.__size:
                    self.__queue.append(s)
                    ret = True

                self.__locker.release()

                if ret:
                    return True

                time.sleep(0.1)

        else:

            self.__locker.acquire()

            if len(self.__queue) < self.__size:
                self.__queue.append(s)
                ret = True

            self.__locker.release()

            return ret

    def get(self):

        vl = None

        self.__locker.acquire()

        if len(self.__queue) > 0:
            vl = self.__queue[0]
            self.__queue.remove(vl)

        self.__locker.release()

        return vl

    def clear(self):
        self.__locker.acquire()
        self.__queue = []
        self.__locker.release()
        pass

class XScanner:

    QUIT_FLAG = "#quit#"
    CLOUD_ONCE_QUERY_COUNT=2
    MAX_CLOUD_ENGINE_COUNT = 20
    MAX_LOCAL_ENGINE_COUNT = 10

    def __init__(self , url , lame_path = None):

        if url is None or len(url) == 0:
            raise Exception('invalid url')

        self.__url = url
        self.__cloud_q = safeq(100)
        self.__scan_q = safeq(100)
        self.__scan_path_q = safeq()
        self.__cloud_hash_fetch = CLame(lame_path)
        self.__thread_handles = []
        self.__file_count = 0
        self.__virust_count = 0
        self.__vdb_handle = None
        self.__engine_handles = []
        self.__cloud_engine_count = 0
        self.__local_engine_count = 0
        self.__lock = threading.RLock()
        self.__is_continue = True
        self.__local_scanners = []

    def load(self , cloud_engine = 1 ,local_engine = 2 ,vlib_path = None):

        print('load virus sigature db ...')
        self.__vdb_handle = VirusDb()
        if not self.__vdb_handle.OpenVdb(vlib_path):
            print("Faild to load virus sigature db.")
            return False

        thd = threading.Thread(target=self.__process_path_thread , args = ())
        thd.start()
        self.__thread_handles.append(thd)

        if cloud_engine > XScanner.MAX_CLOUD_ENGINE_COUNT:
            cloud_engine = XScanner.MAX_CLOUD_ENGINE_COUNT

        self.__cloud_engine_count = cloud_engine

        while cloud_engine > 0:
            thd = threading.Thread(target=self.__cloud_query_thread, args=())
            thd.start()
            self.__thread_handles.append(thd)
            cloud_engine -= 1
            pass

        print(str(self.__cloud_engine_count) + ' cloud query thread is running.')


        if local_engine > XScanner.MAX_LOCAL_ENGINE_COUNT:
            local_engine = XScanner.MAX_LOCAL_ENGINE_COUNT
        self.__local_engine_count = local_engine

        while local_engine > 0:

            _lame = Lame()
            if not _lame.Load(self.__vdb_handle):
                raise Exception("faild to load virus engine.")
            self.__local_scanners.append(_lame)

            thd = threading.Thread(target=self.__lame_scan_thread, args=(_lame,))
            thd.start()
            self.__thread_handles.append(thd)
            local_engine -= 1
            pass

        print(str(self.__local_engine_count) + ' local scan thread is running.')

        return  True

    def process(self , path):

        if path is None or len(path) == 0:
            return

        self.__scan_path_q.put(path)

        pass

    def unload(self):

        self.__scan_path_q.put(XScanner.QUIT_FLAG)
        for thd in self.__thread_handles:
            thd.join()

        if not self.__vdb_handle is None:
            self.__vdb_handle.CloseVdb()
            pass

        print('\nstatistics:')
        print('files :' + str(self.__file_count))
        print('virus :' + str(self.__virust_count))

    def __fetch_cloud_feture(self):

        is_continue = True
        files = []
        _fetch_count = XScanner.CLOUD_ONCE_QUERY_COUNT
        while _fetch_count > 0:
            file = self.__cloud_q.get()
            if file == None:
                break

            if file == XScanner.QUIT_FLAG:
                is_continue = False
                break

            files.append(file)
            _fetch_count -= 1
            pass

        if len(files) == 0:
            return (is_continue , None , None)

        return (is_continue , files , self.__cloud_hash_fetch.GetCloudHash(files))

    def __https_request(self ,data):

        if data is None:
            return None

        rep = requests.get(self.__url, data=data, timeout=10, headers={'Content-Type': 'application/octet-stream'})
        if rep is None:
            return

        if rep.status_code != 200:
            return None

        s = rep.content.decode()
        values = json.loads(s)

        return values

    def __cloud_query_thread(self):

        _virus_count = 0
        _is_continue = True
        while _is_continue:
            (_is_continue , files , data) = self.__fetch_cloud_feture()

            if  files == None:
                time.sleep(0.1)
                continue

            if data == None:
                self.__push_to_scan_q(files)
                continue

            _result = self.__https_request(data)
            if _result == None:
                self.__push_to_scan_q(files)
                continue

            _virus_count += self.__process_cloud_result(files , _result)

            pass

        self.__scan_q.put(XScanner.QUIT_FLAG)

        #print('cloud_query_thread quit')

        self.__lock.acquire()
        self.__virust_count += _virus_count
        self.__lock.release()

        pass

    def __process_cloud_result(self , fnames ,result):

        if len(fnames) != len(result):
            for file in fnames:
                self.__scan_q.put(file)
            return 0

        _virus_count = 0
        for i in range(0, len(fnames)):

            if result[i]["r"] == 'b' and len(result[i]["n"]) > 0:
                _virus_count += 1
                self.__display_result(fnames[i] , "     Infected:" + result[i]["n"]+" (cloud)")
            else:
                self.__scan_q.put(fnames[i])
                pass

            pass

        return  _virus_count

    def __push_to_scan_q(self , files):

        if self.__local_engine_count == 0:
            for file in files:
                self.__display_result(file ,None)

            return

        if files is None or len(files) == 0:
            pass

        for file in files:
            self.__scan_q.put(file)

    def __process_file_or_dir(self , file_or_path):

        if file_or_path is None or len(file_or_path) == 0:
            return

        if not os.path.exists(file_or_path):
            return

        for dir_path , dirs , files in os.walk(file_or_path):
            for file in files:
                if not self.__is_continue:
                    return

                full_path = os.path.join(dir_path , file)
                self.__file_count += 1
                if self.__cloud_engine_count > 0:
                    self.__cloud_q.put(full_path)
                else:
                    self.__scan_q.put(full_path)
                pass
            pass
        pass

    def __process_path_thread(self):

        while True:

            _path = self.__scan_path_q.get()
            if _path == XScanner.QUIT_FLAG:
                break

            pass

            self.__process_file_or_dir(_path)

        #print('process_path_thread quit')
        if self.__cloud_engine_count == 0:
            self.__scan_q.put(XScanner.QUIT_FLAG)
            pass
        else:
            self.__cloud_q.put(XScanner.QUIT_FLAG)
            pass


    def __lame_scan_thread(self , _lame):

        if _lame is None:
            return

        _virus_count = 0

        while True:

            file = self.__scan_q.get()
            if file is None:
                time.sleep(0.1)
                continue

            if file == XScanner.QUIT_FLAG:
                break

            _result = _lame.ScanFile(file)
            if _result is None:
                self.__display_result(file , None)
            else:
                _virus_count += 1
                self.__display_result(file , "  Infected: " + format_bstring(_result.vname) + " (" + format_bstring(_result.engid) + ")")

            pass

        #print('lame_scan_thread quit')
        _lame.Unload()
        self.__scan_q.put(XScanner.QUIT_FLAG)

        self.__lock.acquire()
        self.__virust_count += _virus_count
        self.__lock.release()

        pass

    def __display_result(self , fname ,result):

        if fname is None or len(fname) == 0:
            return

        self.__lock.acquire()

        sys.stdout.write(fname)
        if not result is None:
            sys.stdout.write(result)
            pass
        sys.stdout.write('\n')
        self.__lock.release()
        pass



def exit(signum, frame):
    sys.exit()

def help():
    print('usage:')
    print(' xlame.scanner.py  [-cloud=1] [-local=2] c:\dll')
    print(' -cloud:specify cloud worker count. default value is 1.')
    print(' -local:specify local worker count. default value is 2.')
    pass

def str2int(value):

    v = 0
    try:
        v = int(value)
    except:
        pass

    return v

#sys.argv.append('-cloud=1')
#sys.argv.append('d:/rsdata/1/11')

if __name__ == '__main__':

    try:
        signal.signal(signal.SIGINT, exit)
        signal.signal(signal.SIGTERM, exit)

        if len(sys.argv) == 1:
            help()
            raise Exception('invalid param')

        cloud_worker_count = 1
        local_worker_count = 2
        path_lst = []
        for s in sys.argv[1:]:
            if s[0:1] == '-':
                s = s.lower()
                if s.startswith('-cloud='):
                    cloud_worker_count = str2int(s[7:])
                elif s.startswith('-local='):
                    local_worker_count = str2int(s[7:])
                    pass
                else:
                    pass

            else:
                path_lst.append(s)

        if len(path_lst) == 0:
            help()
            raise Exception('there is no file or dir to scan.')

        cloud_url = 'https://umc.rising.com.cn/openzdr/detect?apikey=d477c085-2475-4d43-a09a-883468ecbe9c'
        scanner = XScanner(cloud_url)

        if not scanner.load(cloud_worker_count, local_worker_count):
            raise Exception('faild to load xscanner')

        for file in path_lst:
            scanner.process(file)
            pass

        scanner.unload()
        pass

    except:
        pass

