import os
import sys
from ctypes import *
import platform
import signal

def format_bstring(bstr, CodePage=None):
    if 3 == sys.version_info[0]:
        if CodePage is None:
            return bstr.decode()
        else:
            return bstr.decode(CodePage)
    else:
        return bstr

class LameScanResult(Structure):
    _fields_ = [
        ("mkclass", c_int),
        ("kclass", c_char  * 16),
        ("kclass_desc", c_char  * 16),
        ("kclass_desc_w", c_wchar  * 16),
        ("engid", c_char  * 32),
        ("vname", c_char  * 256),
        ("vid32", c_int),
        ("vid40", c_longlong),
        ("hitag", c_char  * 32),
        ("treat", c_int)
        ]

class LameVersionInfo(Structure):
    _fields_ = [
        ("engv", c_char * 33),
        ("vdbv", c_char * 33)
    ]

class LameLicenseInfo(Structure):
    _fields_ = [
        ("Version", c_char * 16),
        ("Owner" , c_char * 128),
        ("Date"  , c_char * 64),
        ("Authm" , c_char * 32),
        ("Data"  , c_char * 2048)
    ]

class LameCloudInfo(Structure):
    _fields_ = [
        ("data", c_char_p)
    ]

class LameUtity:

    @staticmethod
    def GetFullLameName(lpath = None):

        if lpath is None:
            lpath = sys.path[0]

        if not os.environ['PATH'].find(lpath) > 0:
            os.environ['PATH'] = os.environ['PATH'] + ';' + lpath
            pass

        if platform.system() == "Windows":
            #os.environ['PATH'] = os.environ['PATH'] + ';' + lpath
            lamepath = lpath + '\\lame.dll'
        else:
            #os.environ['PATH'] = os.environ['PATH'] + ';' + lpath
            lamepath = lpath + '/liblame.so'
        
        return lamepath


class VirusDb:
 
    def __init__(self, lpath = None):
        _lame_path = LameUtity.GetFullLameName(lpath)
        if not os.path.exists(_lame_path):
            raise Exception("File is not exists.")
        self._lame_dll_handle = CDLL(_lame_path)

        if self._lame_dll_handle is None:
            raise Exception("Faild to load lame library.")


    def OpenVdb(self,vdbf):
        if self._lame_dll_handle is None:
            return False

        self._lame_lib_handle = self._lame_dll_handle.lam_virdb_open(vdbf)
        if self._lame_lib_handle is None:
            return False

        return True 

    def CloseVdb(self):
        if self._lame_dll_handle is None:
            return

        if self._lame_lib_handle is None:
            return

        self._lame_dll_handle.lam_virdb_close(self._lame_lib_handle)
        self._lame_lib_handle = None

    def GetVbdHandle(self):
        return self._lame_lib_handle

class Enter_File_Info(Structure):
    _fields_ = [
        ("info_size", c_int32),
        ("scan_depth", c_int32),
        ("disk_file_path", c_char_p),
        ("embed_file_path", c_char_p)
    ]

class Leave_File_Info(Structure):
    _fields_ = [
        ("info_size", c_int32),
        ("scan_depth", c_int32),
        ("disk_file_path", c_char_p),
        ("embed_file_path", c_char_p),
        ("l_case", c_int)
    ]

class Alarm_Info(Structure):
    _fields_ = [
        ("info_size", c_int32),
        ("disk_file_path", c_char_p),
        ("embed_file_path", c_char_p),
        ("scan_result", POINTER(LameScanResult)),
    ]


LAMSCANENTERFILE = WINFUNCTYPE(c_int, POINTER(Enter_File_Info), c_void_p)
def LamScanEnterFile(result, userdata):
    _lam = cast(userdata, py_object).value
    
    res = cast(result,POINTER(Enter_File_Info)).contents
    fname = res.disk_file_path
    return _lam.EnterFile(fname)


LAMSCANLEAVEFILE = WINFUNCTYPE(None, POINTER(Leave_File_Info), c_void_p)
def LamScanLeaveFile(result, userdata):
    _lam = cast(userdata, py_object).value

    res = cast(result,POINTER(Leave_File_Info)).contents
    return _lam.LeaveFile(res)


LAMSCANALARM1 = WINFUNCTYPE(c_int, POINTER(Alarm_Info), c_void_p)
def LamScanAlarm(result, userdata):
    _lam = cast(userdata, py_object).value

    _res = cast(result, POINTER(Alarm_Info)).contents
    alarmRes = cast(_res.scan_result, POINTER(LameScanResult)).contents
    return _lam.Alarm(alarmRes)


LAMSCANCLOUD = WINFUNCTYPE(None, c_void_p, c_void_p)
def LamScanCloud(result, userdata):
    _lam = cast(userdata, py_object).value


LAMSCANPWD = WINFUNCTYPE(None, c_char_p, c_void_p)
def LamScanPwd(pwd, userdata):
    _lam = cast(userdata, py_object).value


class LamScanCallback(Structure):
    _fields_ = [
        ("lam_scan_enter_file", LAMSCANENTERFILE),
        ("lam_scan_leave_file", LAMSCANLEAVEFILE),
        ("lam_scan_alarm",      LAMSCANALARM1),
        ("lam_scan_cloud",      LAMSCANCLOUD),
        ("lam_scan_pwd",        LAMSCANPWD),
    ]

class LameBase:

    def __init__(self, lpath = None, iscb = None):
        self._lame_path = LameUtity.GetFullLameName(lpath)
        if not os.path.exists(self._lame_path):
            raise Exception("File is not exists.")
        self._lame_dll_handle = CDLL(self._lame_path)

        if self._lame_dll_handle is None:
            raise Exception("Faild to load lame library.")

        self._lame_path = lpath
        self._lame_engine_handle = None
        self._params = []
        self._iscb = iscb

        self.EnterFile = None
        self.LeaveFile = None
        self.Alarm = None
        self._this_obj_ref = c_void_p()
        self._this_obj_ref.value = id(self)
        self._virus_name = None
        self._callback = LamScanCallback()
        self._callback.lam_scan_enter_file = LAMSCANENTERFILE(LamScanEnterFile)
        self._callback.lam_scan_leave_file = LAMSCANLEAVEFILE(LamScanLeaveFile)
        self._callback.lam_scan_alarm = LAMSCANALARM1(LamScanAlarm)
        self._callback.lam_scan_cloud = LAMSCANCLOUD(LamScanCloud)
        self._callback.lam_scan_pwd = LAMSCANPWD(LamScanPwd)

    def SetCallback(self, enter_file, leave_file, alram):
        self.EnterFile = enter_file
        self.LeaveFile = leave_file
        self.Alarm = alram

    def SetParam(self, param):
        if self._lame_dll_handle is None:
            return

        if param is None:
            return

        self._params.append(param)

        if self._lame_engine_handle is not None:
            self.SetParameter(param)

        return


    def SetParameter(self, param):
        if 3 == sys.version_info[0]:
            p = param.encode('gbk')
        else:
            p = param
        return self._lame_dll_handle.lam_engine_set_param(self._lame_engine_handle, p)


    def Load(self, vdb_object):
        self._vbd_handle = vdb_object.GetVbdHandle()
        if self._vbd_handle is None:
            return False

        self._lame_engine_handle = self._lame_dll_handle.lam_engine_new(self._vbd_handle)
        if self._lame_engine_handle == 0:
            return False

        for p in self._params:
            self.SetParameter(p)

        if self._iscb is True:
            ret = self._lame_dll_handle.lam_engine_init_2(self._lame_engine_handle, byref(self._callback), self._this_obj_ref)
        else:
            ret = self._lame_dll_handle.lam_engine_init(self._lame_engine_handle)

        if ret < 0:
            self._lame_dll_handle.lam_engine_close(self._lame_engine_handle)
            return False

        return True


    def GetVersion(self):
        try:
            if self._lame_dll_handle is None:
                return None
            _version = LameVersionInfo()
            ret = self._lame_dll_handle.lam_version(byref(_version))
            if ret < 0:
                return None
            return _version
        except Exception as e:
            print(e)
            return None


    def GetLicense(self):
        try:
            if self._lame_dll_handle is None:
                return None
            _License = LameLicenseInfo()
            ret = self._lame_dll_handle.lam_licence_get(byref(_License))
            if ret < 0:
                return None
            return _License
        except Exception as e:
            print(e)
            return None


    def Unload(self):
        if self._lame_dll_handle is None:
            return

        if self._lame_engine_handle is None:
            return

        #self._lame_dll_handle.lam_engine_close(self._lame_engine_handle)


class Lame(LameBase):
    LSCT_CONTINUE = 0x01
    LSCT_ABORT    = 0x02

    def __init__(self, lpath = None, iscb = None):
        LameBase.__init__(self, lpath, iscb)

    def ScanFile(self,fname):

        if self._lame_dll_handle is None:
            return None

        if self._lame_engine_handle is None:
            return None

        if fname is None:
            return None

        if not os.path.exists(fname):
            return None

        _result = LameScanResult()

        if 3 == sys.version_info[0]:
            scan_file = self._lame_dll_handle.lam_engine_scan_w
        else:
            scan_file = self._lame_dll_handle.lam_engine_scan_a

        ret = scan_file(self._lame_engine_handle, fname, byref(_result))
        if ret < 0:
            return None

        return _result

    def ScanMem(self, data):
        if self._lame_dll_handle is None:
            return None

        if self._lame_engine_handle is None:
            return None

        if data is None:
            return None

        _result = LameScanResult()
        ret = self._lame_dll_handle.lam_engine_scan_m(self._lame_engine_handle, data, byref(_result))
        if ret < 0:
            return None

        rsize = sizeof(_result)
        return _result

    def ScanFileWithCloud(self, fname):

        if self._lame_dll_handle is None:
            return None

        if fname is None:
            return None

        if not os.path.exists(fname):
            return None

        if 3 == sys.version_info[0]:
            scan_file_with_cloud = self._lame_dll_handle.lame_cloud_signatue_file_w
        else:
            scan_file_with_cloud = self._lame_dll_handle.lame_cloud_signatue_file

        _result = LameCloudInfo()
        ret = scan_file_with_cloud(fname, byref(_result))
        if ret < 0:
            return None
        _result_cloud = format_bstring(_result.data)
        self._lame_dll_handle.lam_mem_free(_result)

        return _result_cloud

    def Clone(self):
        _lame = Lame(self._lame_path) 

        for s in self._params:
            _lame.SetParam(s)
        
        if not _lame.Load(self._vbd_handle):
            return None

        return _lame

