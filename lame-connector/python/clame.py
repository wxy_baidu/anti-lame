import sys
import os
import json
import struct
from ctypes import *
import requests
import platform
import traceback





class CLame:
    def __init__(self , clame_path = None):

        clame_dll_name = ''
        lame_dll_name = ''
        if platform.system() == "Windows":
            clame_dll_name = 'clame.dll'
            lame_dll_name = 'lame.dll'
            
            if clame_path is None:
                os.environ['PATH'] = os.environ['PATH'] + ';' + sys.path[0]
            else:
                os.environ['PATH'] = os.environ['PATH'] + ';' + clame_path
        else:
            clame_dll_name = 'libclame.so'
            lame_dll_name = 'liblame.so'

        if clame_path is None:
            self.__clame_path = os.path.join(sys.path[0] , clame_dll_name)
            if not os.path.exists(self.__clame_path):
                self.__clame_path = os.path.join(sys.path[0] , lame_dll_name)
                pass
        else:
            self.__clame_path = os.path.join(clame_path , clame_dll_name)
            if not os.path.exists(self.__clame_path):
                 self.__clame_path = os.path.join(clame_path , lame_dll_name)
            pass
        
        self._clame_dll_handle = CDLL(self.__clame_path)
        pass

    def __lame_cloud_signatue_file(self , fnames , juser = None ):

        c_result = c_void_p(0)
        try:
            c_names = ( c_char_p * len(fnames)) ()
            for idx in range(0, len(fnames)):
                c_names[idx] = c_char_p(fnames[idx])
                pass 

            rsize = c_int(0)
            ret = self._clame_dll_handle.clame_cloud_signatue_file(juser , byref(c_names) , len(fnames) , byref(c_result) , byref(rsize))
            if ret != 0 or rsize.value == 0:
                return None

            cdata = (c_ubyte * rsize.value).from_address(c_result.value)

            barry = bytearray(cdata)

            return barry
            
        finally:
            if c_result.value != 0:
                self._clame_dll_handle.clame_mem_free(c_result)
                c_result.value = 0
                pass
            pass
        
        return None

    def __lame_cloud_signatue_file_w(self , fnames , juser = None ):

        c_result = c_void_p(0)
        try:
            c_names = ( c_wchar_p * len(fnames)) ()
            for idx in range(0, len(fnames)):
                c_names[idx] = c_wchar_p(fnames[idx])
                pass 

            rsize = c_int(0)
            ret = self._clame_dll_handle.clame_cloud_signatue_file_w(juser , byref(c_names) , len(fnames) , byref(c_result) , byref(rsize))
            if ret != 0 or rsize.value == 0:
                return None

            cdata = (c_ubyte * rsize.value).from_address(c_result.value)

            barry = bytearray(cdata)

            return barry
            
        finally:
            if c_result.value != 0:
                self._clame_dll_handle.clame_mem_free(c_result)
                c_result.value = 0
                pass
            pass
        
        return None

    def GetCloudHash(self , fnames , user_info = None):
        if fnames == None or len(fnames) == 0:
            return None

        if 3 == sys.version_info[0]:
            return self.__lame_cloud_signatue_file_w(fnames , user_info)
        else:
            return self.__lame_cloud_signatue_file(fnames , user_info)


CLAMEFETCHCALLBACK = CFUNCTYPE(c_long , c_void_p , c_int ,c_void_p, c_int) 
def clame_fetch_callback_(userdata , index , data , size):

    _clame = cast(userdata, py_object).value
    cdata = (c_ubyte * size).from_address(data)
    barry = bytearray(cdata)

    return _clame.clame_user_callback(index , barry)

class CLameWitchCallback:
    FETCH_CONTINUE = 0
    FETCH_STOP     = -1
    def __init__(self , callback , clame_path = None):

        clame_dll_name = ''
        lame_dll_name = ''
        if platform.system() == "Windows":
            clame_dll_name = 'clame.dll'
            lame_dll_name = 'lame.dll'
            if clame_path is None:
                os.environ['PATH'] = os.environ['PATH'] + ';' + sys.path[0]
            else:
                os.environ['PATH'] = os.environ['PATH'] + ';' + clame_path
        else:
            clame_dll_name = 'libclame.so'
            lame_dll_name = 'liblame.so'

        if clame_path is None:
            self.__clame_path = os.path.join(sys.path[0] , clame_dll_name)
            if not os.path.exists(self.__clame_path):
                self.__clame_path = os.path.join(sys.path[0] , lame_dll_name)
                pass
        else:
            self.__clame_path = os.path.join(clame_path , clame_dll_name)
            if not os.path.exists(self.__clame_path):
                 self.__clame_path = os.path.join(clame_path , lame_dll_name)
            pass
        

        self._clame_dll_handle = CDLL(self.__clame_path)

        self.clame_user_callback = callback
        self.clame_fetch_callback = CLAMEFETCHCALLBACK(clame_fetch_callback_)

        self._this_obj_ref = c_void_p()
        self._this_obj_ref.value = id(self)

        pass

    def __clame_fetch_batch(self , fnames , sflags):

        try:
            c_names = ( c_char_p * len(fnames)) ()
            for idx in range(0, len(fnames)):
                c_names[idx] = c_char_p(fnames[idx])
                pass 

            ret = self._clame_dll_handle.clame_fetch_batch(sflags , byref(c_names) , len(fnames) , self.clame_fetch_callback , self._this_obj_ref)
            if ret == 0:
                return True
            
        except:
            pass
        
        return False


    def __clame_fetch_batch_w(self , fnames , sflags):

        try:
            c_names = ( c_wchar_p * len(fnames)) ()
            for idx in range(0, len(fnames)):
                c_names[idx] = c_wchar_p(fnames[idx])
                pass 

            ret = self._clame_dll_handle.clame_fetch_batch_w(sflags , byref(c_names) , len(fnames) , self.clame_fetch_callback , self._this_obj_ref)
            if ret == 0:
                return True
            
        except:
            pass

        return False


    
    def GetCloudHash(self , fnames , sflags = None):
        if fnames == None or len(fnames) == 0:
            return False
        
        if 3 == sys.version_info[0]:
            return self.__clame_fetch_batch_w(fnames , sflags)
        else:
            return self.__clame_fetch_batch(fnames , sflags)

    pass


