* [工程说明](#工程说明)
  * [plame.php](#plame.php)
  * [lame.scanner.php](#lame.scanner.php)
  * [lame.sample.php](#lame.sample.php)

# 工程说明
___

- **plame.php**

   使用php封装的扫描类接口.

- **lame.scanner.php**

   依据plame.php封装的一个更加简单的扫描器接口.

- **lame.sample.php**

   使用lame.scanner.php的简单示例.


## plame.php
___

导入C动态库并获取接口函数列表

```php
if (!extension_loaded('plame'))
{
	dl('plame.' . PHP_SHLIB_SUFFIX);
}

```

病毒库相关类(加载、释放)

```php
class Virusdb
{
	function __construct($vlib_path = "");
	function __destruct();
	function get_vlib();
}

php封装的基础扫描接口

class Lame
{
	function __construct($vdb = null);
	function __destruct();
	function open_vdb($vlib_path = "");
	function create();
	function release(&$handle);
	function close_vdb(&$vdb);
	function set_parameter($param);
	function load($vdbf);
	function unload();
	function scan($filename);
	function scan_with_callback($filename, $enter_file, $leave_file, $alarm, &$user_data);
	function scan_memory($contents);
	function scan_memory_with_callback($contents, $enter_file, $leave_file, $alarm, &$user_data);
	function extract_file($filename, $enter_file, $leave_file, &$user_data);
	function extract_get_size($handle);
	function extract_seek($handle, $offset, $method);
	function extract_tell($handle);
	function extract_read($handle, $size);
	function get_version($vdbf);
	function get_licence_info($licf);
}
```

## lame.scanner.php
___

封装的扫描接口类

```php
class Scanner
{
	function __construct();
	function __destruct();
	function ScanFile($target_path);
	function SetCallback($enter_file, $leave_file, $alarm = null);
	function ScanFileWithCallback($target_path, &$user_data);
	function ScanMemory($contents);
	function ScanMemoryWithCallback($contents, &$user_data);
	function ShowVersion($vdbf);
	function ShowLicence($licf);
	function SetConfig($param);
	function Load($vdbf);
	function Unload();
	function Extract($filename, &$user_data);
	function ExtractGetSize($handle);
	function ExtractSeek($handle, $offset, $method);
	function ExtractTell($handle);
	function ExtractRead($handle, $size);
	function travl_path($target_path);
	function scan($filename);
}
```

## lame.sample.php
___

简单示例代码

```php
require("lame.scanner.php");

$s_ef_cb = function ($filename, $depth, &$user_data) {
	return LSCT_CONTINUE;
};
$s_lf_cb = function ($filename, $depth, &$user_data, $l) {
	if (null != $user_data)
	{
		echo "$filename	Infected: " . $user_data . "\n";
	}
	else
	{
		echo "$filename\n";
	}
};
$s_a_cb = function ($filename, $info, &$user_data) {
	$user_data = $info->vname;
	return LSCT_CONTINUE;
};
$e_ef_cb = function ($filename, $fmt, $depth, $flag, $handle, &$user_data) {
	if (null != $handle && $depth >= 1)
	{
		$real_path = "dump/" . $filename;
		$new_filename = __DIR__ . "/" . $real_path;
		analyze_path($real_path);
		dump_file($handle, $new_filename, $user_data);
	}
	return LCT_DO_EXTRACT;
};
$e_lf_cb = function ($filename, $fmt, $depth, $flag, $handle, &$user_data, $lv) {
};

function read_by_pos($handle, $offset, $length, $scanner)
{
	if (!$scanner->ExtractSeek($handle, $offset, LAME_SEEK_SET))
		return null;
	return $scanner->ExtractRead($handle, $length);
}
function check_dir($target_path)
{
	if (!file_exists($target_path))
	{
		mkdir($target_path, 0777, true);
	}
}
function analyze_path($target_path)
{
	$list = explode("/", $target_path);
	$length = sizeof($list);
	if ($length <= 1) return;
	$path = $list[0];
	check_dir($path);
	for ($i = 1; $i < $length - 1; $i++)
	{
		$path = $path . "/" . $list[$i];
		check_dir($path);
	}
}
function dump_file($handle, $target_path, $scanner)
{
	$size = $scanner->ExtractGetSize($handle);
	$offset = 0;
	$f = fopen($target_path, "w") or die("unable to open file!");
	while ($size > 0)
	{
		$bytes_read = 0x1000 < $size ? 0x1000 : $size;
		$contents = read_by_pos($handle, $offset, $bytes_read, $scanner);
		if (null == $contents) break;
		fwrite($f, $contents);
		$offset += $bytes_read;
		$size -= $bytes_read;
	}
	fclose($f);
}

function main()
{
	global $s_ef_cb, $s_lf_cb, $s_a_cb, $e_ef_cb, $e_lf_cb;
	$filename = "/rsdata/virus/";
	$scan = new Scanner();
	$scan->SetConfig("license-path=#/rsdata/licence.json");
	$scan->Load("/rsdata/malware.rmd");
	$scan->ScanFile($filename);
	$virus_name = null;
	$scan->SetCallback($s_ef_cb, $s_lf_cb, $s_a_cb);
	$scan->ScanFileWithCallback($filename, $virus_name);

	$filename = "/rsdata/virus/1";
	$contents = file_get_contents($filename);
	$scan->ScanMemory($contents);
	$virus_name = null;
	$scan->SetCallback($s_ef_cb, $s_lf_cb, $s_a_cb);
	$scan->ScanMemoryWithCallback($contents, $virus_name);

	$filename = "/rsdata/t.tgz";
	$scan->SetCallback($e_ef_cb, $e_lf_cb);
	$scan->Extract($filename, $scan);
}

echo "\n***************************** test-start **************************************\n\n";
main();
echo "\n***************************** test-finished **************************************\n";
```
