* [工程说明](#工程说明)
  * [lame.js](#lame.js)
  * [lame.scanner.js](#lame.scanner.js)
  * [lame.sample.js](#lame.sample.js)

# 工程说明
___

- **lame.js**

   使用nodejs封装的扫描类接口。

- **lame.scanner.js**

   依据lame.js使用nodejs封装的一个更加简单的扫描器接口。

- **lame.sample.js**

   使用lame.scanner.js的简单示例。



## lame.js
___

导入C动态库并获取接口函数列表

```javascript
var node_lame = require('./build/release/node_lame');
```

获取版本信息函数

```javascript
lame.prototype.GetVersionInfo(vdbf);
lame.prototype.GetLicenceInfo(licence);
````

病毒库相关类(加载、释放)

```javascript
lame.prototype.OpenVdb(vlib);
lame.prototype.CloseVdb();
```

解文件包类

```javascript
lame.prototype.ExtractFile(filename, password, enter_file, leave_file, userdata);
lame.prototype.Extract_GetSize(handle);
lame.prototype.Extract_Seek(handle, offset, method);
lame.prototype.Extract_Tell(handle);
lame.prototype.Extract_Read(handle, size);
```

引擎加载扫描文件类

```javascript
lame.prototype.Load();
lame.prototype.SetParameters(param);
lame.prototype.Unload();
lame.prototype.ScanFile(path);
lame.prototype.ScanMem(data);
lame.prototype.ScanFileWithCallback(path, enter_file, leave_file, alarm, userdata);
lame.prototype.ScanMemWithCallback(data, enter_file, leave_file, alarm, userdata);
```

## lame.scanner.js
___

封装的扫描接口类

```javascript
Scanner.prototype.Load(vdbf, licence)
Scanner.prototype.Unload();
Scanner.prototype.ScanFile(filepath);
Scanner.prototype.ScanFileWithCallback(filepath);
Scanner.prototype.ScanMem(data);
Scanner.prototype.ScanMemWithCallback(data, enter_file, leave_file, alarm, userdata);
Scanner.prototype.ShowVersion(vdbf);
Scanner.prototype.ShowLicence(licence);
```

封装的解包接口类

```javascript
Scanner.prototype.ExtractFile(filename, password, enter_file, leave_file, userdata);
Scanner.prototype.ExtGetSize(handle);
Scanner.prototype.ExtSeek(handle, offset, method);
Scanner.prototype.ExtTell(handle);
Scanner.prototype.ExtRead(handle, size);
```

## lame.sample.js
___

简单示例代码

```javascript
'use strict';
var fs = require('fs');
var buffer = require('buffer');
var Scanner = require('./lame.scanner.js');

var LSCT_CONTINUE = 1;
var LSCT_ABORT = 2;

var LCT_CONTINUE = 1;
var LCT_DO_EXTRACT = 2;
var LCT_ABORT = 3;

function scan_enter_file(filename, depth, userdata) {
	return LSCT_CONTINUE;
}

function scan_leave_file(filename, depth, userdata, l) {

}

function scan_alarm(filename, info, userdata) {
	if (null != info) {
		console.warn(filename + '\tInfected: ' + info.vname);
	} else {
		console.log(filename);
	}
	return LSCT_CONTINUE;
}

function save_to_file(filename, data) {
	try {
		var va = buffer.transcode(Buffer.from(data,'utf8'), 'utf8', 'binary');
		fs.appendFileSync(filename, va);
	} catch (e) {
		console.log(e);
	}
}

function dump_file(scanner, handle, filename) {
	var size = scanner.ExtGetSize(handle);
	var offset = 0;
	while (size > 0) {
		var bytesRead = 0x1000 < size ? 0x1000 : size;
		var data = scanner.ExtReadPos(handle, offset, bytesRead);
		if (null == data || undefined == data || bytesRead != data.length) break;
		save_to_file(filename, data);
		offset += data.length;
		size -= data.length;
	}
}

function extract_enter_file(filename, format, depth, FCLS, handle, userdata) {
	var scanner = userdata;
	if (null != handle && depth >= 1) {
		var path = __dirname + "/" + filename;
		dump_file(scanner, handle, path);
	}
	return LCT_DO_EXTRACT;
}

function extract_leave_file(filename, format, depth, FCLS, handle, userdata, l) {
	console.log(filename + ' format: ' + format);
}

function DetectFile(scanner, path, flag) {
	if (flag) {
		scanner.ScanFile(path);
	} else {
		scanner.ScanFileWithCallback(path, scan_enter_file, scan_leave_file, scan_alarm, "filescan_callbck");
	}
}

function DetectMemory(scanner, data, flag) {
	if (flag) {
		scanner.ScanMem(data);
	} else {
		scanner.ScanMemWithCallback(data, scan_enter_file, scan_leave_file, scan_alarm, "memscan_callbck");
	}
}

function main(vdbf, licence) {
	var scanner = new Scanner();
	scanner.SetConfig("license-path=" + licence);
	if (!scanner.Load(vdbf)) return;

	// get information
	scanner.ShowVersion(vdbf);
	scanner.ShowLicence(licence);

	// extract
	scanner.ExtractFile('d:/rsdata/5/5.zip', "", extract_enter_file, extract_leave_file, scanner);

	// file
	DetectFile(scanner, 'd:/rsdata/5/3949568_22900584_MYGL.exe.###', false);

	// memory
	var data = fs.readFileSync('d:/rsdata/5/3949568_22900584_MYGL.exe.###');
	DetectMemory(scanner, data, true);

	scanner.Unload();
}

// main
console.log("--------------- test start ---------------");
main('D:\\MyJob\\SDK\\produce\\make\\SDK_build_results\\lame-win-x64\\malware.rmd', 'D:\\MyJob\\SDK\\produce\\make\\licence.json');
console.log("--------------- test end -----------------");
```
