'use strict';
var fs = require('fs');
var buffer = require('buffer');
var Scanner = require('./lame.scanner.js');

var LSCT_CONTINUE = 1;
var LSCT_ABORT = 2;

var LCT_CONTINUE = 1;
var LCT_DO_EXTRACT = 2;
var LCT_ABORT = 3;

function scan_enter_file(filename, depth, userdata) {
	return LSCT_CONTINUE;
}

function scan_leave_file(filename, depth, userdata, l) {

}

function scan_alarm(filename, info, userdata) {
	if (null != info) {
		console.warn(filename + '\tInfected: ' + info.vname);
	} else {
		console.log(filename);
	}
	return LSCT_CONTINUE;
}

function save_to_file(filename, data) {
	try {
		var va = buffer.transcode(Buffer.from(data,'utf8'), 'utf8', 'binary');
		fs.appendFileSync(filename, va);
	} catch (e) {
		console.log(e);
	}
}

function dump_file(scanner, handle, filename) {
	var size = scanner.ExtGetSize(handle);
	var offset = 0;
	while (size > 0) {
		var bytesRead = 0x1000 < size ? 0x1000 : size;
		var data = scanner.ExtReadPos(handle, offset, bytesRead);
		if (null == data || undefined == data || bytesRead != data.length) break;
		save_to_file(filename, data);
		offset += data.length;
		size -= data.length;
	}
}

function extract_enter_file(filename, format, depth, FCLS, handle, userdata) {
	var scanner = userdata;
	if (null != handle && depth >= 1) {
		var path = __dirname + "/" + filename;
		dump_file(scanner, handle, path);
	}
	return LCT_DO_EXTRACT;
}

function extract_leave_file(filename, format, depth, FCLS, handle, userdata, l) {
	console.log(filename + ' format: ' + format);
}

function DetectFile(scanner, path, flag) {
	if (flag) {
		scanner.ScanFile(path);
	} else {
		scanner.ScanFileWithCallback(path, scan_enter_file, scan_leave_file, scan_alarm, "filescan_callbck");
	}
}

function DetectMemory(scanner, data, flag) {
	if (flag) {
		scanner.ScanMem(data);
	} else {
		scanner.ScanMemWithCallback(data, scan_enter_file, scan_leave_file, scan_alarm, "memscan_callbck");
	}
}

function main(vdbf, licence) {
	var scanner = new Scanner();
	scanner.SetConfig("license-path=" + licence);
	if (!scanner.Load(vdbf)) return;

	// get information
	scanner.ShowVersion(vdbf);
	scanner.ShowLicence(licence);

	// extract
	scanner.ExtractFile('d:/rsdata/5/5.zip', "", extract_enter_file, extract_leave_file, scanner);

	// file
	DetectFile(scanner, 'd:/rsdata/5/3949568_22900584_MYGL.exe.###', false);

	// memory
	var data = fs.readFileSync('d:/rsdata/5/3949568_22900584_MYGL.exe.###');
	DetectMemory(scanner, data, true);

	scanner.Unload();
}

// main
console.log("--------------- test start ---------------");
main('D:\\MyJob\\SDK\\produce\\make\\SDK_build_results\\lame-win-x64\\malware.rmd', 'D:\\MyJob\\SDK\\produce\\make\\licence.json');
console.log("--------------- test end -----------------");