
# libtools

   此目录中包含有SDK的病毒库升级工具和序列号生成工具.
   
- [libup](#libup)
- [homeup](#homeup)
- [mcgen](#mcgen)

___

## libup

病毒库升级程序,可以从本地/远程升级病毒库.参数如下所示:
   
### libup用法

升级本地病毒库文件

#### 1.本地/共享升级

命令行如下:
```
libup -loc {info file} {lib home} {want home}
参数代表意义如下
	- info file: 本地配置文件
	- lib home: 本地病毒库文件所在文件夹路径(需要是已存在的文件夹)
	- want home: 病毒库升级所需要的文件夹路径
```
![libup-loc升级图示](../resource/libup-loc.png)

#### 2.通过导航联网升级

命令行如下:
```
libup -xml {virlib name/url} {lib home} {download home}
参数代表意义如下
	- virlib name/url: 病毒库名称或病毒库xml的网络链接地址
	- lib home: 本地病毒库文件所在文件夹路径,默认当前路径
	- download home: 病毒库升级所需要的文件下载路径,默认当前路径
```
![libup-xml升级图示](../resource/libup-xml.png)

#### 3.通过配置文件联网升级

命令行如下:
```
libup -cfg {cfg url} {lib home} {download home}
参数代表意义如下
	- cfg url: 配置文件的网络链接地址
	- lib home: 本地病毒库文件所在文件夹路径,默认当前路径
	- download home: 病毒库升级所需要的文件下载路径,默认当前路径
```
![libup-cfg升级图示](../resource/libup-cfg.png)

___

## homeup

病毒库中心更新程序,用户可通过此工具将rising的病毒库移植到自己构建的服务器平台上,并可以通过libup工具从自己的服务器升级.

### homeup用法

获取并升级中心系统的病毒库文件,包括bas文件,rp文件和配置文件.

#### 1.本地/共享升级

命令行如下:
```
homeup -loc {info file} {local root} {remote root}
参数代表意义如下
	- info file: 本地配置文件
	- local root: 病毒中心的本地文件夹所在路径
	- remote root: 病毒中心文件服务器的文件夹路径
```
![homeup-loc升级图示](../resource/homeup-loc.png)

#### 2.通过导航联网升级

命令行如下:
```
homeup -xml {virlib name/url} {local root}
参数代表意义如下
	- virlib name/url: 病毒库名称或病毒库xml的网络链接地址
	- local root: 病毒中心的本地文件夹所在路径
```
![homeup-xml升级图示](../resource/homeup-xml.png)

#### 3.通过配置文件联网升级

命令行如下:
```
homeup -cfg {cfg url} {local root}
参数代表意义如下
	- cfg url: 配置文件的网络链接地址
	- local root: 病毒中心的本地文件夹所在路径
```
![homeup-cfg升级图示](../resource/homeup-cfg.png)

___

## mcgen

客户端序列号提取工具

![mcgen图示](../resource/mcgen.png)

