
# 瑞星反病毒引擎SDK

## 概述

本软件开发包(SDK)为瑞星网安技术股份有限公司完全自主知识产权产品，采用瑞星最先进的四核杀毒引擎并经优化而形成的独立型通用SDK。本SDK有速度快、资源占用稳定、易使用、易扩展等特点，其可应用于各种二次开发研发。更多详情请看 [http://www.rising.com.cn/avsdk/](http://www.rising.com.cn/avsdk/)

- [引擎优势](#引擎优势)
- [授权文件生成](#授权文件生成)
- [适应语言](#适应语言)
- [操作系统](#操作系统)
- [性能参数](#性能参数)
- [目录结构](#目录结构)
- [产品升级](#产品升级)

___

## 引擎优势

### 1.丰富全面的平台支持

纯C++实现，无任何内联汇编，几乎可在Windows及所有符合Posix标准操作系统和CPU上编译运行。已经稳定应用于：
- Windows + x86/x64
- Linux + x86/x64
- Linux + MIPS 32/64 + 大小字节序
- Linux + ARM 32/64
- Unix + PowerPC
- 国产操作系统(Linux家族) + 国产CPU(x86/MIPS)

### 2.强劲的文件深度分析/提取/解码能力

支持多种压缩包、自解压包、符合文档、媒体文件、加密脚本等

#### <1> 支持的压缩包(自解压包)

||||||
|---|---|---|---|---|
| 7z | ace | arc | arj | bz2 |
| cab | cabset | gz/gzip | lha | paquet
| rar | rar5 |sea | tar | xz |
| zip | apk  | jar | zipx  | zoo |
| OLE

#### <2> 支持的安装包:

```lua
├── Instyler
├── NSIS
├── Inno(all version)
├── SFactory
├── SmartInstaller
├── Wise Installer
├── SIS -- 塞班(Symbian)系统软件安装包
├── deb -- Debian系Linux的软件安装包
```

#### <3> 支持的电子邮件和邮箱文件

```lua
├── EML/MSG -- 基于文本的电子邮件存档
├── MSO -- 基于微软复合文档的电子邮件存档
├── FixMail Mailbox -- 低版本
├── Outlook 4/5 Mailbox
├── Outlook 2003+ (.pst)
├── Netscape
```

#### <4> 支持的文档/多媒体文件

提取文档中嵌入的其它资源，如：宏、脚本、可执行程序等

```lua
├── Open Office XML -- *.xml，*.odp，*.odt，*ods
├── Microsoft Office Binary(v2003-) -- *.doc， *.xls， *.ppt， ...
├── Microsoft Office XML(v2003+) -- *.docx ，*.docm， *.xlsx，*.xlsm，*.xlsb *.pptx，*.pptm， ...
├── Adobe PDF -- 包括 .pdf 和 基于XML的 .xdp
├── Flash SWF -- 支持 defalte 和 lzma 两种压缩算法(提取SWF中嵌入的PE文件)
├── CHM -- Windows帮助文件
├── RTF
```

#### <5> 支持的磁盘/镜像/文件系统

提取磁盘内的分区以及分区内存放的文件

```lua
├── 虚拟化磁盘
|  ├── VMDK -- vmware
|  ├── VHD -- microsoft
|  ├── QCOW -- kvm/quemu
|  ├──--目前均未支持快照
├── MBR/GPT -- 分区表的磁盘镜像
├── NTFS -- 文件系统
├── DMG -- MacOS下的磁盘镜像文件
├── CramFS -- 专门针对闪存设计的只读压缩的文件系统
├── CPIO -- 由linux cpio命令创建的归档文件
├── ISO
```

#### <6> 支持的脚本发布包：

```lua
├── AutoIt
├── QuickBatch
├── Gentee
├── 易语言 -- 老版本
```

#### <7> 支持的其他文件类型：

```lua
├── VBE/JSE -- 微软的加密VBS脚本
├── HTML Data URI
├── UUE
├── FAS -- AutoCAD脚本快速加载文件
├── Windows PE -- 提取资源中的文件
```

### 3.丰富的脱壳能力

#### <1> 针对Windows PE文件:

|||||||||
|---|---|---|---|---|---|---|---|
| acprotect | armadillo | aspack | aspr | bitarts | crypt | dbpe | enigma |
 execrypt | exeshield | expressor | orien | packman | pcguard | pearmor | pec2 |
| pespin | petite | pex | pklite | rlpack | svkp | upx | yoda |
| molebox | ahpack | cexe | depack | exe32pack | exefog | ezip | FSG |
| himeys | krypton | mew | nakepack | npack | nprotect | nspack | NTkrnl |
| obsidium | pcshrinker | pecrypt | pep | pedinisher | pelocknt | pestil | polycrypt |
| punisher | sdprotect | slvcodeprotector | themida | vprotect | yzpack | pe-ninja|

#### <2> 针对Linux ELF文件：

   upx (all targets，实验性质)

### 4.全面的模拟执行能力(反病毒虚拟机)

#### <1> MS-DOS + BIOS + 8086 模拟器

   该模拟器用于虚拟执行 MBR，DOS-COM，DOS-EXE等古老的程序

#### <2> x86平台32/64位混合式Windows模拟器

   该模拟器用于应对现代恶意软件，支持32位和64位程序。同时还支持以32位方式执行64位程序，或以64位方式执行32位程序，来应对一些极端情况

#### <3> JavaScript沙盒

   用于虚拟执行JScript/JavaScript

### 5.丰富多样、针对性强的恶意软件识别技术

#### <1> 简单特征码匹配技术

   采用逻辑位置 + 偏移 + 特征串的方式识别病毒

#### <2> 词法分析技术

   针对VBS/JS/PHP/JAVA/C等文本文件进行词法分析后，在语义层面对恶意脚本进行识别

#### <3> 自定义查杀毒逻辑

   分析员通过编写查杀病毒的代码，可查杀超级复杂的数量极大的感染型病毒恶意软件家族

#### <4> 模拟执行跟踪技术

   又称反病毒虚拟机，将目标文件(PE文件/JS脚本)在模拟器中执行，跟踪其执行时执行的指令、修改的内存、调用的函数、产生的副作用，记录日志，同时驱动特征码匹配、自定义查杀毒逻辑

#### <5> 智能特征码匹配技术

   一种MPM全文搜索技术，类似正则表达式的病毒特征描述方式但在恶意软件识别上扩充了很多特殊的能力，适用于内容匹配和模式匹配

#### <6> 多维度(内容平面)交叉判定技术

   将文件通过不同的处理方式，分离成不同维度的内容平面，并在这些内容平面中进行特征匹配，通过不同平面中特征匹配情况来综合判定目标文件是否为恶意软件。例如：敏感函数名 在原始内容中无法被匹配到，但在模拟执行后的函数调用日志中存在，可以推断出该敏感函数被人为加密或隐藏了，这便是一种恶意软件的逻辑特征

#### <7> 图像匹配技术

   该技术专门针对“图标诱导”类的WinPE 恶意软件，例如，将自身的程序图标设置为MsOffice/PDF等文档图标来诱导用户双击执行的恶意软件

#### <8> 代码基因技术

   针对不同类型的文件，提取这些文件的主干(框架)内容，并计算其指纹(我们称为代码基因)，其可用于对抗轻微变型/混淆的恶意程序、恶意脚本、恶意宏。该技术支持 机器自动化 处理，无需人工提取，效率极高

#### <9> 关键内容检验技术

   针对 Windows PE 文件(包含dotNet)，划定约15个关键内容区域，对这些区域按照特定算法计算指纹，并匹配恶意指纹库。该技术支持机器自动化处理，无需人工提取，效率高

#### <10> 数字签名校验及匹配技术

   将目标文件的数字签名同已经收录的恶意数字签名进行比对

#### <11> 简单哈希匹配技术

   文件尺寸＋文件内容哈希值

#### <12> YARA技术

   融合了准行业标准恶意软件表述语言

#### <13> 人工智能技术

   针对WinPE文件和Flash文件的人工智能恶意软件识别技术——RDM+。RDM+是瑞星自主研发的人工智能引擎，目前以“云脑”和“嵌入”两种形式存在于反病毒引擎中。“云脑”模式拥有庞大的预测模型、极快的误报处置速度、客户端无感的周期性更新，其预测过程融入在传统云引擎交互过程中，为使用者带来巨大的“零日”恶意软件识别能力的提升。“嵌入”模式则采用更为小尺寸的模型，以实现客户端部署，它为引擎提供无网络环境下的基于人工智能的恶意软件识别能力，由于模型较小，其在误报概率上会略微高于“云脑”模式

### 6.丰富的文件格式识别能力
```lua
format-list
├── BOOT -- MBR引导扇区
├── MZ -- DOS可执行程序(exe)
├── PE -- Windows可执行程序
├── DEX -- 安卓应用主程序
├── ZIP -- ZIP压缩包
├── CAB -- Microsoft制订的压缩包格式
├── ARJ -- ARJ压缩包
├── RAR -- RAR压缩包
├── RAR5 -- RAR 5.0 压缩包
├── ARC -- ARC压缩包
├── ZOO -- Unix系统中使用旧的压缩格式
├── LZH -- 用LHARC压缩的文件
├── TAR -- UNIX合并文件
├── GZIP -- gz 压缩包
├── UUE -- UUE 编码文件
├── ACE -- Ace 压缩档案格式
├── BZ2 -- BZ2 压缩包
├── CHM -- Windows帮助文件(经过编译打包的HTML文件集合)
├── 7Z -- 7Zip 压缩包
├── XZ -- XZ 压缩包
├── SIS -- 塞班系统的软件安装包
├── APK -- 安卓应用发布包
├── IPA -- 苹果IOS软件发布包
├── ZIPEXE -- ZIP 自解压包
├── CABEXE -- CAB 自解压包
├── ARJEXE -- ARJ 自解压包
├── RAREXE -- RAR 自解压包
├── RAR5EXE -- RAR5 自解压包
├── ARCEXE -- ARC 自解压包
├── ZOOEXE -- ZOO 自解压包
├── LZHEXE -- LZH 自解压包
├── ZIPNE -- ZIP 自解压包
├── ZIPPE -- ZIP 自解压包
├── ACEEXE -- ACE 自解压包
├── NSIS -- NSIS 安装包
├── INNO -- INNO安装包(InnoSetup)
├── 7ZEXE -- 7Zip自解压包
├── CABSETEXE -- CABSET自解压包
├── SEAEXE -- SEA 自解压包
├── WISE -- WISE安装包
├── WISEA -- WISE安装包
├── AUTOIT -- AUTOIT脚本编译后的可执行文件
├── ELANG -- 易语言编译的程序
├── GENTEE -- GENTEE脚本编译后的程序
├── PAQUET -- PaquetBuilder产生的安装包
├── INSTYLER -- instyler SmartSetup制作的安装包
├── SFACTORY -- Setup Factory制作的安装包
├── QUICKBATCH -- QuickBatch制作的可执行程序
├── DOC -- Microsoft Windows复合文档
├── MDB -- Microsoft Access数据库
├── HTA -- Windows HTML Application文件
├── REG -- Windows注册表文件
├── VBSENC -- Windows加密VBScript文件
├── JSENC -- Windows加密JScript文件
├── XOFFICE -- Microsoft Office 2003+ 文档
├── JPEG -- JPEG/JPG图片文件
├── SWF -- Flash 文件
├── SWC -- Flash 文件(带压缩的)
├── PDF -- Adobe PDF文档
├── MP3 -- MP3 音频文件
├── LNK -- Windows 快捷方式文件
├── TTF -- True Type Font 字体文件
├── ELF -- Linux 可执行文件
├── CLASS -- Java 子节码文件
├── BMP -- Windows Bitmap位图文件
├── GIF -- GIF图片文件
├── PNG -- PNG图片文件
├── ICO -- 图标文件
├── CDR -- CorelDraw 产生的文件
├── MIDI -- MIDI音频文件
├── MPEG -- MPEG视频文件
├── HLP -- Windows帮助文件
├── WPG -- Word Perfect产生的矢量图文件
├── CPT -- FineReport产生的报表文件
├── ISU -- 安装程序产生的文件
├── WBM -- Macromedia Fireworks File
├── CAT -- Windows数字签名编录文件
├── MBX -- Microsoft Outlook保存电子邮件格式；Eudora邮箱
├── WAV -- Windows波形声形
├── WDL -- 北京华康公司开发的一种电子读物文件格式
├── DBF -- dBASE文件
├── REALPLER -- RealPlayer媒体文件
├── RPM -- RedHat包管理器包(用于Linux)
├── IDB -- MSDev中间层文件
├── PDB -- Windows调试符号文件
├── AU3SCRIPT -- AutoIt3 脚本
├── DOSCOM -- DOS COM文件
├── TEXT -- 文本文件
├── VBS -- VBScript文件
├── HTML -- 网页文件
├── BAT -- 批处理文件
├── JS -- JScript文件
├── NS -- NSIS脚本文件
├── EML -- 电子邮件文件
├── IRC -- IRC脚本
├── PERL -- Perl脚本
├── SHELL -- Bash shell脚本
├── VHD -- 微软虚拟磁盘
├── VMDK -- VMWare虚拟磁盘
├── RTF -- RTF文档
├── PST -- Outlook 2003 + 邮箱文件
├── UCS16LE -- Unicode 16LE File
├── OUTLOOKEX_V5 -- Outlook Express 5 邮箱文件
├── OUTLOOKEX_V4 -- Outlook Express 4 邮箱文件
├── FOXMAIL_V3 -- Foxmail 3 邮箱文件
├── NETSCAPE_V4 -- Netscape 4 邮箱文件
├── BASE64 -- Base64 编码后的PE文件
├── NWS -- Windows Live Mail的( WLM)创建新闻组的文件
├── MHT -- 聚合HTML文档
├── MSG -- 邮件
├── BOX -- 邮箱文件
├── IND -- 邮箱索引文件
├── JAR -- Java归档文件
├── NTFS -- NTFS文件系统
├── FAT -- FAT文件系统
├── LOTUSPIC -- Lotus使用的图片文件
├── VBVDI -- VirtualBox 虚拟磁盘
├── SQLITE -- SQLite 数据库文件
├── LIBA -- lib文件
├── TIFF -- TIFF图片文件
├── FAS -- AutoCAD FAS文件
├── LSP -- AutoCAD LSP文件
├── XDP -- XML表达的PDF文件
├── WSF -- XML表达的Windows脚本文件
├── EOT -- Embedded Open Type 字体文件
├── ASF -- 高级串流媒体文件
├── RIFF -- 资源互换文件格式(ResourcesInterchange FileFormat)，媒体文件
├── QTFF -- QuickTime媒体文件
├── TORRENT -- BT种子文件
├── WMF -- Windows WMF 图像文件
├── JSENC -- 加密JS脚本
├── COM -- DOS├──COM文件
├── INI -- Windows的系统配置文件
├── HTM -- HTML网页文件
├── PPT -- MsOffice PowerPoint幻灯片文件
├── PHP -- PHP网页文件
├── MACH_O -- MacOS可执行文件
├── FLV -- FLASH VIDEO 流媒体文件
├── IPA -- 苹果IOS应用文件
├── SMARTINSTALLER -- Smart Installer制作的安装包文件
├── MSO -- 微软ActiveMine文件
├── PS -- Microsoft Powershell 脚本文件
├── EPS -- Adobe PostScript 文件
├── WSF -- 含有可扩展标记语言 (XML)代码的Windows 脚本文件
├── INF -- 系统自动化脚本
├── MSCFB -- Microsoft Compound File Binary (CFB)
├── ODF -- OpenOffice文件
├── OXML -- OpenOffice文件(xml)
├── ISO -- 符合ISO 9660标准的光盘镜像文件格式
├── DEB -- Linux软件包(debian)
├── CPIO -- Linux文件包(cpio命令创建)
├── CRAMFS -- 专门针对闪存设计的只读压缩的文件系统
```


## 授权文件生成

1. 授权文件licence.json是在与用户达成销售协议后，用户在客户端使用lame-libtools下mcgen工具通过命令行执行，生成一个字符串序列号；

2. 用户通过[https://ame-exp.rising.com.cn/](https://ame-exp.rising.com.cn/)并传递参数apikey、rskey、mcode来获取licence.json.其中apikey和rskey是由瑞星公司提供的，mcode是第一步有mcgen生成出来的序列号


## 适应语言

C、C++、Java、C#、.Net、.Net core、nodejs、python、go、php


## 操作系统

Windows系列、RedHat系列、ubuntu系列、centos系列、Debian系列、Arm系列及各国产操作系统等


## 性能参数

### 查杀速度

- 参考硬件环境：内存2G以上，CPU在I5以上，硬盘7200转/秒以上
- 按流量：20M/秒左右
- 按文件数：50个文件/秒左右

### 异步多线程查杀

- 可支持
- 单引擎支持最大并发数100(具体并发数需参考硬件参数)


## 目录结构

```lua
SDK
├── lame-connector -- 各个语言的SDK接口封装接口及示例代码
├── lame-libtools -- SDK的病毒库升级工具和序列号生成工具
├── lame -- SDK的二进制文件(包括windows/linux下的32位/64位)
├── resource -- markdown的图片资源
```


## 产品升级

### 升级方式

- 引擎SDK模块文件采取全文件覆盖升级
- 病毒库可以用覆盖升级也可用增量升级方式，同时还支持通过中心升级的方式

### 病毒库增量升级

直接使用lame-libtools下面对应操作系统的工具libup进行升级，注意此升级方式为热升级，即可以在软件运行的过程中进行升级操作

工具的使用方法参见 [**lame-libtools**](lame-libtools/README.md)

### 中心升级方法

   此方法使用工具包中的homeup、libup工具进行升级，这种方法相当于把瑞星的升级服务器移植到您的本地服务器上，客户端可连接本地服务器进行增量升级操作

   具体操作步骤如下：

#### 1. 确认文件列表

   升级前确认授权文件和liblame.so、librxavx.so、libup、homeup在同一目录下.如下图所示

   ![结构图示](resource/ll.png)

#### 2. 使用homeup对本地服务器进行升级(需要互联网)

   命令如下所示：

   ./homeup –cfg http://40.73.87.180/viruslib/vlco/cfg/vlco.cfg virus-server (其中virus-server是病毒库数据文件的存放目录)

   ![homeup升级图示](resource/homeup.png)

   执行后virus-server的目录结构如下图所示：

   ![virus-server结构图](resource/tree.png)

#### 3. 客户端使用libup连接本地服务器进行升级(需要局域网)

   命令如下所示：

   ./libup -cfg http://192.168.0.111/virus-server/vlco/cfg/lastest.cfg (其中lastest.cfg是最新的病毒库配置文件，也可指定其他的配置文件)

   ![libup升级图示](resource/libup.png)

#### 4. 确认升级成功后，病毒库是否可正常使用

   命令如下所示：

   ./lame.scanner -version

   ![版本图示](resource/version.png)
