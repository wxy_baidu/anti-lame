# V4版接口说明

***

## 1. RameCreateRuntime

| 方法 | 描述 |
|---|---|
| 原型 | `HRESULT RAMEAPI RameCreateRuntime(OUT IRXRunTime ** ppRuntime);` |
| 功能 | 创建引擎运行时Com对象,并返回其指针 |
| 参数 | ppRuntime:用来接收指向Com对象接口地址的指针变量,若成功则此指针需要释放资源 |
| 返回值 | 成功:大于等于0;失败:小于0 |

## 2. RameCreateInstance

| 方法 | 描述 |
|---|---|
| 原型 | `HRESULT RAMEAPI RameCreateInstance(IN IRXRunTime * runtime OPTIONAL, IN ECID clsid, IN REFIID riid, OUT void ** ppv);` |
| 功能 | 根据clsid创建引擎所需要的Com对象,并返回其指针 |
| 参数 | runtime:引擎运行时Com对象,可为空 |
|      | clsid:引擎组件的ID值,参考ECID定义|
|      | riid:创建的Com对象的接口标识符 |
|      | ppv:用来接收指向Com对象接口地址的指针变量,若成功则此指针需要释放资源 |
| 返回值 | 成功:大于等于0;失败:小于0 |

## 3. RameSignatureOpen

| 方法 | 描述 |
|---|---|
| 原型 | `HRESULT RAMEAPI RameSignatureOpen(IN IRXParameters * lpParam, IN REFCXSTRP rFileName, IN LPCSTR aObNamePrefix, OUT SignatureFile * lpLibFile);` |
| 功能 | 根据clsid创建引擎所需要的Com对象,并返回其指针 |
| 参数 | lpParam:引擎参数Com对象 |
|      | rFileName:包含病毒库路径的XSTRP引用,参考XSTRP定义|
|      | aObNamePrefix:病毒库路径标识,可为空 |
|      | lpLibFile:用来接收指向病毒库对象接口地址的指针变量,若成功则需要使用RameSignatureClose释放资源 |
| 返回值 | 成功:大于等于0;失败:小于0 |

## 4. RameSignatureClose

| 方法 | 描述 |
|---|---|
| 原型 | `HRESULT RAMEAPI RameSignatureClose(IN SignatureFile * lpLibFile);` |
| 功能 | 释放创建的病毒库对象 |
| 参数 | lpLibFile:病毒库对象指针,可参考SignatureFile |
| 返回值 | 成功:大于等于0;失败:小于0 |

## 5. RameEngineCreate

| 方法 | 描述 |
|---|---|
| 原型 | `HRESULT RAMEAPI RameEngineCreate(IN EngineInitParam * params, IN OUT EngineInstance * engine);` |
| 功能 | 根据参数创建引擎Com对象,并初始化返回其指针 |
| 参数 | params:引擎参数Com对象 |
|      | ppv:用来接收指向引擎Com对象接口地址的指针变量,若成功则此指针需要释放资源 |
| 返回值 | 成功:大于等于0;失败:小于0 |

## 6. RameEngineScan

| 方法 | 描述 |
|---|---|
| 原型 | `HRESULT RAMEAPI RameEngineScan(IN EngineInstance * engine, IUnknown * pstm, IRXScanCallback * lpCallback);` |
| 功能 | 根据clsid创建引擎所需要的Com对象,并返回其指针 |
| 参数 | engine:引擎Com对象 |
|      | pstm:文件对象指针 |
|      | lpCallback:回调函数Com对象的接口指针 |
| 返回值 | 成功:大于等于0;失败:小于0 |

## 7. RameEngineClose

| 方法 | 描述 |
|---|---|
| 原型 | `HRESULT RAMEAPI RameEngineClose(IN EngineInstance * engine);` |
| 功能 | 根据clsid创建引擎所需要的Com对象,并返回其指针 |
| 参数 | engine:引擎Com对象 |
| 返回值 | 成功:大于等于0;失败:小于0 |
