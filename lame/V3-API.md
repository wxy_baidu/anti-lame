# V3版接口说明

- [文件类](#文件类)
- [内存类](#内存类)
- [信息类](#信息类)
- [扫描类](#扫描类)
- [云扫描类](#云扫描类)

***

## 文件类

### 1. lam_file_open

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C lam_file_t	lam_file_open(bool wcs, const void * file_path);` |
| 功能 | 根据传入的路径参数打开文件,并返回其指针(此对象只能lame使用) |
| 参数 | wcs:file_path是否为uncoide编码的标记,ture:uicode,false:ansii |
|      | file_path:包含文件路径的字符串指针 |
| 返回值 | 成功:文件对象指针;失败:NULL |

### 2. lam_file_wrap

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C lam_file_t lam_file_wrap(lam_view_t view);` |
| 功能 | 将内存块封装成lame可直接使用的文件对象,并返回其指针(此对象只能lame使用) |
| 参数 | view:内存块指针 |
| 返回值 | 成功:文件对象指针;失败:NULL |

### 3. lam_file_close

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lam_file_close(lam_file_t file);` |
| 功能 | 关闭lame文件对象 |
| 参数 | file:由lam_file_open或lam_file_wrap创建的文件对象 |
| 返回值 | 无 |


## 内存类

### 1. lam_mem_new

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C lam_ptr_t lam_mem_new(size_t size);` |
| 功能 | 申请大小为size的内存空间 |
| 参数 | size:申请的内存空间大小 |
| 返回值 | 成功:内存指针;失败:NULL |

### 2. lam_mem_free

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lam_mem_free(lam_ptr_t ptr);` |
| 功能 | 释放内存空间 |
| 参数 | ptr:由lam_mem_new申请的内存指针 |
| 返回值 | 无 |

### 3. lam_mem_dup

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C lam_ptr_t lam_mem_dup(const void* data,size_t len);` |
| 功能 | 申请内存并复制内存中的数据 |
| 参数 | data:内存空间指针 |
|      | len:内存空间大小 |
| 返回值 | 成功:内存指针(需要lam_mem_free释放);失败:NULL |


## 信息类

### 1. lam_version

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_version(lam_version_t* info);` |
| 功能 | 获取lame的版本信息 |
| 参数 | info:指向版本信息的指针,参见lam_version_t |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 2. lam_licence_get

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_licence_get(lam_lisence_t* info);` |
| 功能 | 返回lame的授权文件信息 |
| 参数 | info:指向lam_lisence_t结构的指针,存储返回的授权文件的信息 |
| 返回值 | 成功:大于等于0;失败:小于0 |


## 扫描类

### 1. lam_virdb_open

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C lam_mwdb_t lam_virdb_open(mbsptr_t vlibf);` |
| 功能 | 根据传入的路径参数打开病毒库,并返回其指针 |
| 参数 | vlibf:字符串指针,可为空(默认加载当前路径下的病毒库) |
| 返回值 | 成功:病毒库对象指针;失败:NULL |

### 2. lam_virdb_close

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lam_virdb_close(lam_mwdb_t vdb);` |
| 功能 | 关闭病毒库,释放病毒库资源 |
| 参数 | vdb:lam_virdb_open创建的对象 |
| 返回值 | 无 |

### 3. lam_engine_new

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C lam_engn_t lam_engine_new(lam_mwdb_t vdb);` |
| 功能 | 创建引擎对象 |
| 参数 | vdb:由lam_virdb_open打开的病毒库对象指针 |
| 返回值 | 成功:引擎对象指针;失败:NULL |

### 4. lam_engine_set_param

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_engine_set_param(lam_engn_t engine, mbsptr_t param);` |
| 功能 | 根据传入的字符串配置引擎参数 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针 |
|      | param:指向参数配置项和值的字符串指针 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 5. lam_engine_init

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_engine_init(lam_engn_t engine);` |
| 功能 | 引擎初始化 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 6. lam_engine_init_2

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_engine_init_2(lam_engn_t engine, lam_callbacks_t * callbacks, void * usr_data);` |
| 功能 | 根据用户设置的参数,初始化引擎 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针 |
|      | callbacks:扫描病毒时需要回调函数 |
|      | usr_data:用户数据,lame对此不做任何处理 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 7. lam_engine_fork

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C lam_engn_t lam_engine_fork(lam_engn_t engine);` |
| 功能 | 根据初始化好的引擎实例fork一个新的引擎 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针 |
| 返回值 | 成功:引擎对象指针;失败:NULL |

### 8. lam_engine_scan

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_engine_scan(lam_engn_t engine, lam_file_t file, lam_scan_result_t* result);` |
| 功能 | 扫描文件并返回扫描结果 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针,初始化后的引擎实例,如果引擎是通过lam_engine_init初始化,扫描到病毒就会终止扫描流程;如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程 |
|      | file:由lam_file_open打开的文件lame内部文件句柄 |
|      | result:指向扫描结果的指针,参见lam_scan_result_t |
| 返回值 | 有病毒:大于等于0;无毒:小于0 |

### 9. lam_engine_scan_a

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_engine_scan_a(lam_engn_t engine, mbsptr_t file_path, lam_scan_result_t* result);` |
| 功能 | 扫描文件并返回扫描结果 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针,初始化后的引擎实例,如果引擎是通过lam_engine_init初始化,扫描到病毒就会终止扫描流程;如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程 |
|      | file_path:需要扫描的文件路径 |
|      | result:指向扫描结果的指针,参见lam_scan_result_t |
| 返回值 | 有病毒:大于等于0;无毒:小于0 |

### 10. lam_engine_scan_w

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_engine_scan_w(lam_engn_t engine, wcsptr_t file_path, lam_scan_result_t* result);` |
| 功能 | 扫描文件并返回扫描结果 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针,初始化后的引擎实例,如果引擎是通过lam_engine_init初始化,扫描到病毒就会终止扫描流程;如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程 |
|      | file_path:需要扫描的文件unicode路径 |
|      | result:指向扫描结果的指针,参见lam_scan_result_t |
| 返回值 | 有病毒:大于等于0;无毒:小于0 |

### 11. lam_engine_scan_m

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lam_engine_scan_m(lam_engn_t engine, lam_view_t file_view, lam_scan_result_t* result);` |
| 功能 | 扫描文件并返回扫描结果 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针,初始化后的引擎实例,如果引擎是通过lam_engine_init初始化,扫描到病毒就会终止扫描流程;如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程 |
|      | file_view:需要扫描的文件内存 |
|      | result:指向扫描结果的指针,参见lam_scan_result_t |
| 返回值 | 有病毒:大于等于0;无毒:小于0 |

### 12. lam_engine_close

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lam_engine_close(lam_engn_t engine);` |
| 功能 | 销毁引擎对象 |
| 参数 | engine:由lam_engine_new创建的引擎对象指针 |
| 返回值 | 无 |


## 云扫描类

### 1. cam_summary_fetch

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long cam_summary_fetch(lam_file_t file, uint32_t summary_mask, lam_ptr_t * json);` |
| 功能 | 提取文件的云hash |
| 参数 | file:使用lam_file_open打开的lame内部文件句柄 |
|      | summary_mask:用于控制提取某些Hash,例:CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1 |
|      | json:json格式的云hash,需要使用lam_mem_free释放 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 2. cam_summary_fetch_a

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long cam_summary_fetch_a(mbsptr_t file_path, uint32_t summary_mask, lam_ptr_t * json);` |
| 功能 | 提取文件的云hash |
| 参数 | file_path:需要提取的文件名 |
|      | summary_mask:用于控制提取某些Hash,例:CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1 |
|      | json:json格式的云hash,需要使用lam_mem_free释放 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 3. cam_summary_fetch_w

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long cam_summary_fetch_w(wcsptr_t file_path, uint32_t summary_mask, lam_ptr_t * json);` |
| 功能 | 提取文件的云hash |
| 参数 | file_path:需要提取的文件名 |
|      | summary_mask:用于控制提取某些Hash,例:CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1 |
|      | json:json格式的云hash,需要使用lam_mem_free释放 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 4. cam_summary_fetch_m

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long cam_summary_fetch_m(lam_view_t file_view, uint32_t summary_mask, lam_ptr_t * json);` |
| 功能 | 提取文件的云hash |
| 参数 | file_view:需要提取的文件内存 |
|      | summary_mask:用于控制提取某些Hash,例:CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1 |
|      | json:json格式的云hash,需要使用lam_mem_free释放 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 5. cam_result_convert

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long cam_result_convert(cam_ret_t * cresult, lam_scan_result_t * lresult, lam_mwdb_t vdb);` |
| 功能 | 将云结果转成lame引擎的结果 |
| 参数 | cresult:云结果 |
|      | lresult:lame结果 |
|      | vdb:使用lam_virdb_open打开的病毒库句柄(可选) |
| 返回值 | 成功:大于等于0;失败:小于0 |
