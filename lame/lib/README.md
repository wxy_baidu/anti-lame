# 示例程序编译库
---

   Windows x86/x64下,C/C++示例代码编译所需要的lib库.

## curl.lib

   curl静态库,用于文件上传和下载.

## json.lib

   json静态库,是一种轻量级的数据交换格式,便于解析和传输.

## lame.lib

   lame静态库,SDK接口库.

## libcrypto.lib

   crypto静态库,密码类库,供openssl/curl编译使用.

## libssl.lib

   ssl静态库,用于安全通信,供openssl/curl编译使用.
