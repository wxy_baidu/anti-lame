
# SDK

   此目录中包含有SDK的bin文件、头文件、示例代码及windows下编译需要的lib文件.
   
- [目录说明](#目录说明)
- [安装步骤](#安装步骤)
- [接口说明](#接口说明)
- [参数说明](#参数说明)

***

## 目录说明

- bin: 二进制文件

- include: 头文件

- lib: windows静态库


## 安装步骤

1. 根据您使用的操作系统从lame/bin下文件取出所需要的bin文件压缩包;

2. 解压bin压缩包到任意路径(此路径需要您添加到程序的默认搜索路径中,以免在调用时候出现找不到动态库的问题);

3. 如果调用程序和解压后的文件不在同一目录下,需要编写配置文件rame.cfg,文件内容如下:

   PATH:/root/lib/lame

   其中PATH后面是lame当前的绝对路径,并把rame.cfg文件放到调用程序所在的目录下.


## 接口说明

- [V2版](V2-API.md)

- [V3版](V3-API.md)

- [V4版](V4-API.md)


## 参数说明

| 参数设置 | 描述 |
|---|---|
| precise-format=1/0 | 开启/关闭精确扫描（默认开启，扫描的记录会少一些） |
| max-treat-times=500 | 杀毒次数(默认500) |
| max-compound-size=-1 | 最大扫描复合文档的大小(单位byte,默认uint64(-1)) |
| max-nest-depth=50 | 压缩脱壳类文件最大扫描深度(默认50) |
| fast=0/1 | 杀毒模式下,使用能快速退出当前扫描文件(默认关闭) |
| share-mem-mode=1/0 | 共享内存模式加载病毒库(默认开启) |
| kill | 进入杀毒模式 |
| license-path=c:\\licence.json | 设置licence文件的路径,在linux下需要在路径前加# |

下列参数都以字符串形式作为参数通过API接口进行设置.例如:
	
```c
	lame_param_set(lame, "max-nest-depth=50");
```
