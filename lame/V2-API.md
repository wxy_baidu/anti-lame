# V2版接口说明

- [扫描类](#扫描类)
- [信息类](#信息类)
- [文件解压类](#文件解压类)
- [病毒库升级类](#病毒库升级类)

***

## 扫描类

### 1. lame_open_vdb

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void* lame_open_vdb(const char* vlibf);` |
| 功能 | 根据传入的路径参数打开病毒库,并返回其指针 |
| 参数 | vlibf:字符串指针,可为空(默认加载当前路径下的病毒库) |
| 返回值 | 成功:病毒库对象指针;失败:NULL |

### 2. lame_close_vdb

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lame_close_vdb(void* vdb);` |
| 功能 | 关闭病毒库,释放病毒库资源 |
| 参数 | vdb:lame_open_vdb创建的对象 |
| 返回值 | 无 |

### 3. lame_create

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void* lame_create(void* vdb);` |
| 功能 | 创建引擎对象 |
| 参数 | vdb:由lame_open_vdb打开的病毒库对象指针 |
| 返回值 | 成功:引擎对象指针;失败:NULL |

### 4. lame_destory

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lame_destroy(void* lame);` |
| 功能 | 释放引擎对象 |
| 参数 | lame:由lame_create创建的引擎对象指针 |
| 返回值 | 无 |

### 5. lame_param_set

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_param_set(void* lame, const char* param);` |
| 功能 | 根据传入的字符串配置引擎参数 |
| 参数 | lame:由lame_create创建的引擎对象指针 |
|      | param:指向参数配置项和值的字符串指针 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 6. lame_init

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_init(void* lame);` |
| 功能 | 引擎初始化 |
| 参数 | lame:由lame_create创建的引擎对象指针 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 7. lame_scan_file

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_scan_file(void* lame, const char* fname, rx_scan_result* pResult);` |
| 功能 | 扫描文件并返回扫描结果 |
| 参数 | lame:由lame_create创建的引擎对象指针 |
|      | fname:被扫描的文件路径 |
|      | pResult:指向扫描结果的指针,参见rx_scan_result |
| 返回值 | 有病毒:大于等于0;无毒:小于0 |

### 8. lame_scan_mem

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_scan_mem(void* lame, uint8_t* data, uint32_t size, rx_scan_result* pResult);` |
| 功能 | 扫描文件内容流并返回扫描结果 |
| 参数 | lame:由lame_create创建的引擎对象指针 |
|      | data:指向文件内容的内存指针 |
|      | size:指向文件内容的内存长度 |
|      | pResult:指向扫描结果的指针,参见rx_scan_result |
| 返回值 | 有病毒:大于等于0;无毒:小于0 |


## 信息类

### 1. lame_get_version

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_get_version(lame_info* info);` |
| 功能 | 获取引擎及病毒库的版本 |
| 参数 | info:指向版本信息的指针,参见lame_info |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 2. lame_get_version_ex

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_get_version_ex(const char* vlibf, lame_info* info);` |
| 功能 | 获取引擎及病毒库的版本 |
| 参数 | vlibf:病毒库的路径,以'\0'结尾 |
|      | info:指向版本信息的指针,参见lame_info |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 3. lame_get_version_by_cfg

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_get_version_by_cfg(char* vdbv, int vdb_len, const char* vlcfgpath);` |
| 功能 | 根据病毒库cfg配置文件获取病毒库的版本 |
| 参数 | vdbv:病毒库版本的输出内存空间 |
|      | vdb_len:病毒库版本的输出内存空间大小 |
|      | vlcfgpath:病毒库cfg配置文件的路径,以'\0'结尾 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 4. lame_get_licence_info

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_get_licence_info(rx_licence_info* info);` |
| 功能 | 返回lame的授权文件信息 |
| 参数 | info:指向rx_licence_info结构的指针,存储返回的授权文件的信息 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 5. lame_get_licence_info_ex

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_get_licence_info_ex(const char* licence, rx_licence_info* info);` |
| 功能 | 返回lame的授权文件信息 |
| 参数 | licence:授权文件的路径,以'\0'结尾 |
|      | info:指向rx_licence_info结构的指针,存储返回的授权文件的信息 |
| 返回值 | 成功:大于等于0;失败:小于0 |


## 文件解压类

### 1. lame_extract_file

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_extract_file(void* lame, const char* fname, const char* password, lame_extract_feedback* cb, void* userdata);` |
| 功能 | 解压缩包格式的文件,可通过回调函数进行文件dump |
| 参数 | lame:由lame_create创建的引擎对象指针 |
|      | fname:被扫描的文件路径 |
|      | password:压缩包密码 |
|      | cb:指向lame_extract_|
|      | feedback的回调函数结构体指针 |
|      | userdata:用户自定义数据,供回调函数使用 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 2. lame_file_get_size

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_file_get_size(void* handle);` |
| 功能 | 在lame_extract_file或lame_extract_file_w回调函数中根据返回的句柄获取当前子文件大小 |
| 参数 | handle:子文件对象句柄 |
| 返回值 | 成功:大于0;失败:等于0 |

### 3. lame_file_seek

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_file_seek(void* handle, int32_t offset, LSMETHOD method);` |
| 功能 | 在lame_extract_file或lame_extract_file_w回调函数中根据返回的句柄设定当前子文件的偏移 |
| 参数 | handle:子文件对象句柄 |
|      | offset:文件偏移 |
|      | method:偏移位置类型 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 4. lame_file_tell

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_file_tell(void* handle);` |
| 功能 | 在lame_extract_file或lame_extract_file_w回调函数中根据返回的句柄获取当前子文件的偏移 |
| 参数 | handle:子文件对象句柄 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 5. lame_file_read

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_file_read(void* handle, uint8_t* buf, uint32_t size);` |
| 功能 | 在lame_extract_file或lame_extract_file_w回调函数中根据返回的句柄在当前子文件的偏移处读取文件内容,大小是size |
| 参数 | handle:子文件对象句柄 |
|      | buf:uint8_t类型的指针 |
|      | size:读取文件的大小 |
| 返回值 | 成功:读取文件的字节数;失败:等于0 |


## 病毒库升级类

### 1. lame_vlibup_create

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void* lame_vlibup_create();` |
| 功能 | 创建病毒库升级对象 |
| 参数 | 无 |
| 返回值 | 成功:返回升级对象句柄;失败:返回0 |

### 2. lame_vlibup_getcfg

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long	lame_vlibup_getcfg(const char* local_xml, lame_vlibup_download pfn_download, void* user_data);` |
| 功能 | 根据病毒库导航文件获取配置文件的链接地址 |
| 参数 | local_xml:导航文件的本地路径 |
|      | pfn_download:病毒库升级回调函数,可通过此函数把升级所需要的病毒库文件下载到本地 |
|      | user_data:用户自定义数据 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 3. lame_vlibup_init

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_vlibup_init(void* upper, const char* vlib_home, const char* local_cfg, lame_vlibup_download pfn_download, void* user_data);` |
| 功能 | 升级对象初始化 |
| 参数 | upper:由lame_vlibup_create创建的病毒库升级对象 |
|      | vlib_home:病毒库升级目录 |
|      | local_cfg:病毒库升级配置文件路径 |
|      | pfn_download:病毒库升级回调函数,可通过此函数把升级所需要的病毒库文件下载到本地 |
|      | user_data:用户自定义数据 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 4. lame_vlibup_init2

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_vlibup_init2(void* upper, const char* vlib_home, const char* local_cfg, const char* local_licence, lame_vlibup_download pfn_download, void* user_data);` |
| 功能 | 升级对象初始化 |
| 参数 | upper:由lame_vlibup_create创建的病毒库升级对象 |
|      | vlib_home:病毒库升级目录 |
|      | local_cfg:病毒库升级配置文件路径 |
|      | local_licence:授权文件路径 |
|      | pfn_download:病毒库升级回调函数,可通过此函数把升级所需要的病毒库文件下载到本地 |
|      | user_data:用户自定义数据 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 5. lame_vlibup_update

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_vlibup_update(void* upper, const char* want_home);` |
| 功能 | 病毒库升级更新 |
| 参数 | upper:由lame_vlibup_create创建的病毒库升级对象 |
|      | want_home:病毒库升级所需文件所在的目录 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 6. lame_vlibup_free

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lame_vlibup_free(void* upper);` |
| 功能 | 释放病毒库升级对象 |
| 参数 | upper:由lame_vlibup_create创建的病毒库升级对象 |
| 返回值 | 无 |

### 7. lame_vhomeup_create

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void* lame_vhomeup_create();` |
| 功能 | 创建病毒库中心升级对象 |
| 参数 | 无 |
| 返回值 | 成功:返回升级对象句柄;失败:返回0 |

### 8. lame_vhomeup_getcfg

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long	lame_vhomeup_getcfg(const char* local_xml, lame_vhomeup_download pfn_download, void* user_data);` |
| 功能 | 根据病毒库导航文件获取配置文件的链接地址 |
| 参数 | local_xml:导航文件的本地路径 |
|      | pfn_download:病毒库升级回调函数,可通过此函数把升级所需要的病毒库文件下载到本地 |
|      | user_data:用户自定义数据 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 9. lame_vhomeup_update

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C long lame_vhomeup_update(void* upper, const char* local_cfg, const char* want_home, lame_vhomeup_download pfn_download, void* user_data);` |
| 功能 | 病毒库升级更新 |
| 参数 | upper:由lame_vhomeup_create创建的病毒库升级对象 |
|      | local_cfg:病毒库升级配置文件路径 |
|      | want_home:病毒库升级所需文件所在的目录 |
|      | pfn_download:病毒库升级回调函数,可通过此函数把升级所需要的病毒库文件下载到本地 |
|      | user_data:用户自定义数据 |
| 返回值 | 成功:大于等于0;失败:小于0 |

### 10. lame_vhomeup_free

| 方法 | 描述 |
|---|---|
| 原型 | `EXTERN_C void lame_vhomeup_free(void* upper);` |
| 功能 | 释放病毒库中心升级对象 |
| 参数 | upper:由lame_vhomeup_create创建的病毒库升级对象 |
| 返回值 | 无 |
