#ifndef __LAME_EXTRACT_H__
#define __LAME_EXTRACT_H__
#include "sdk.types.h"


typedef LXCT  (LAMESTDCALLTYPE *lame_extract_enter_file)(const char* file_name , const char* format , uint32_t depth , uint64_t FCLS ,void* hanlde , void* userdata);
typedef void  (LAMESTDCALLTYPE *lame_extract_leave_file)(const char* file_name , const char* format , uint32_t depth , uint64_t FCLS ,void* hanlde , void* userdata , LXLVT l);

typedef struct lame_extract_feedback_
{
	lame_extract_enter_file enter_file;
	lame_extract_leave_file leave_file;
}lame_extract_feedback;




EXTERN_C long	lame_extract_file(void* lame , const char* fname , const char* password  , lame_extract_feedback* cb , void* userdata);
EXTERN_C long	lame_extract_file_w(void* lame , const wchar_t* fname , const wchar_t* password , lame_extract_feedback* cb , void* userdata);
EXTERN_C long	lame_file_get_size(void* handle);
EXTERN_C long	lame_file_seek(void* handle , int32_t offset , LSMETHOD method);
EXTERN_C long	lame_file_tell(void* handle);
EXTERN_C long	lame_file_read(void* handle , uint8_t* buf , uint32_t size);




////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
typedef struct extr_enter_file_info_t_
{
	uint32_t this_size;
	const char* file_name;
	const char* format;
	uint32_t depth ;
	uint64_t FCLS;

}extr_enter_file_info_t;

typedef struct extr_leave_file_info_t_
{
	uint32_t this_size;
	const char* file_name;
	const char* format;
	uint32_t depth ;
	uint64_t FCLS;
	LXLVT leave_case;

}extr_leave_file_info_t;

/**
 * @brief 
 * 
 */
typedef LXCT  (LAMESTDCALLTYPE *lame_extract_enter_file_cb)(extr_enter_file_info_t* info , void* hanlde , void* userdata);
typedef void  (LAMESTDCALLTYPE *lame_extract_leave_file_cb)(extr_leave_file_info_t* info ,void* hanlde , void* userdata);



typedef struct lame_extract_feedback3_
{
	lame_extract_enter_file_cb enter_file;
	lame_extract_leave_file_cb leave_file;
}lame_extract_feedback3;


EXTERN_C long	lame_extract_file_v3(void* lame , const char* fname , const char* password  , lame_extract_feedback3* cb , void* userdata);
EXTERN_C long	lame_extract_file_v3_w(void* lame , const wchar_t* fname , const wchar_t* password , lame_extract_feedback3* cb , void* userdata);

#endif