#pragma once

#include "hy-scan-interface.h"

namespace mp {

    typedef int             PlainId;
    typedef UINT32          LibStrId;
    typedef hy::ScanResult  ScanResult;
    typedef hy::FLOW_CTRL   FLOW_CTRL;

    const FLOW_CTRL FC_CONTINUE = hy::FC_CONTINUE;
    const FLOW_CTRL FC_BREAK = hy::FC_BREAK;

    struct LibStrView
    {
        LPCSTR Data;
        UINT32 Length;
    };

    static const LibStrId   STR_ID_INVALID = -1;
    static const PlainId    PLAIN_ID_INVALID = -1;

    static const HRESULT    S_CONINUE = S_OK;
    static const HRESULT    S_RULE_HIT = S_FALSE;

    struct LinInfo
    {
        UINT32  Version;
        UINT32  StringCount;
        UINT32  PlainCount;
        UINT32  RuleCount;
        UINT32  PatternCount;
        UINT32  FormatCount;
        FFMTID* Formats;
    };

    class IScanSite {
    public:
        STDMETHOD_(FLOW_CTRL, SetAlaram)(const ScanResult* lpResult) PURE;
        STDMETHOD(CheckStringVariant)(LibStrId dwFieldStrId, LibStrId dwValueStrId) PURE;
        STDMETHOD(CheckRightFormat)( FFMTID fmtid ) PURE;
    };

    // 
    class IScanSession : public IUnknown
    {
    public:
        RE_DECLARE_IID;
    public:
        /**
         * @brief 推送数据给扫描引擎。
         * lpBuffer == 0 && nSize == 0 为 结束标记，相当于 Finsh
         *
         * @param dwPlainId 平面ID，通过QueryPlainId()获取.
         * @param lpBuffer 被扫描的数据指针.
         * @param nSize 被扫描数据的大小.
         * @param lpResult 扫描结果.
         * @return HRESULT 状态码, < 0 : 出现错误; S_CONINUE: 继续; S_RULE_HIT: 有结果(lpResult)
         */
        virtual FLOW_CTRL Push(PlainId dwPlainId, CONST VOID* lpBuffer, SIZE_T nSize) PURE;

        /**
         * @brief 结束某个平面的扫描
         *
         * @param dwPlainId 平面ID，通过QueryPlainId()获取.如果 dwPlainId == PLAIN_ID_INVALID， 则结束所有平面
         * @param lpResult 扫描结果.
         * @return HRESULT 状态码, < 0 : 出现错误; S_CONINUE: 继续; S_RULE_HIT: 有结果(lpResult)
         */
        virtual FLOW_CTRL Finsh(PlainId dwPlainId) PURE;

        /**
         * @brief 重置扫描器内部状态
         *
         * @return HRESULT 状态码
         */
        virtual void Reset() PURE;
    };

    // {5780945E-4714-4D5B-A137-73CDC2A6B000}
    RE_DEFINE_IID(IScanSession, "{5780945E-4714-4D5B-A137-73CDC2A6B000}",
        0x5780945e, 0x4714, 0x4d5b, 0xa1, 0x37, 0x73, 0xcd, 0xc2, 0xa6, 0xb0, 0x0);

    // 
    class IScanMachine : public IUnknown {
    public:
        RE_DECLARE_IID;

        STDMETHOD(Open)(IUnknown* lpTable) PURE;

        STDMETHOD(TestFormat)(FFMTID fmtid) PURE;

        // 查询字符串ID
        STDMETHOD(EncodeString)(LPCSTR lpString, LibStrId* pdwStrId, SIZE_T nLen = 0) PURE;

        // 查询字符串指针
        STDMETHOD(DecodeString)(LibStrId dwStrId IN, LibStrView* lpLibStr OUT) PURE;

        // 获取平面数
        STDMETHOD_(SIZE_T, GetPlainCount)() PURE;

        // 查询平面ID
        STDMETHOD(QueryPlainIDs)(LPCSTR* lpNames IN, PlainId* lpIds OUT, SIZE_T nCount) PURE;

        // 创建一个扫描会话
        STDMETHOD(CreateScanSession)(IScanSite* lpSite, IScanSession** ppSession) PURE;
    };

    // {EE7370EA-3AA7-4517-B916-616EC362F01B}
    RE_DEFINE_IID(IScanMachine, "{EE7370EA-3AA7-4517-B916-616EC362F01B}",
        0xee7370ea, 0x3aa7, 0x4517, 0xb9, 0x16, 0x61, 0x6e, 0xc3, 0x62, 0xf0, 0x1b);

    class ScanMachine;
};
