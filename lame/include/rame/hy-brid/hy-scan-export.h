#pragma once

#include "hy-scan-interface.h"

namespace hy
{
    class Runtime;

    class HybirdScanContext;

    class LegacyFormatRecognizer;

    class FormatList;

    class FilePump;

    namespace topis 
    {
        class Engine;
    };

    namespace mp13
    {
        class Engine;
    };

	class OfficeScan;
	class ScriptScan;
	class NormalScan;

};



