#pragma once

#include "hy-scan-export.h"

namespace hy
{
    using namespace UTIL;

    //struct OnReadScan : public TIoHook
    //{
    //public:
    //    IScanContext& hys;
    //    bool trap_in_hook_;
    //    com_ptr<IIoHookable> hooked_;
    //protected:
    //    struct out_of_hook_t {
    //        bool& in_hook_;
    //        out_of_hook_t(bool& f) : in_hook_(f) {
    //            in_hook_ = true;
    //        }
    //        ~out_of_hook_t() {
    //            in_hook_ = false;
    //        }
    //    };
    //    struct restore_fpos_t {
    //        IRXStream& stm;
    //        uint64_t fpos;
    //        restore_fpos_t(IRXStream* pstm, uint64_t pos)
    //            : stm(*pstm)
    //            , fpos(pos)
    //        {
    //        }
    //        ~restore_fpos_t() {
    //            stm.Seek64(fpos, SEEK_SET);
    //        }
    //    };
    //public:
    //    OnReadScan(HybirdScanContext& hs, bool enable) : hys(hs), trap_in_hook_(false)
    //    {
    //        if (!enable || !hys.IsModeAvaildable(re_uuidof(IBlockScanMode)))
    //            return;

    //        hooked_ = hys.Stream();
    //        if (!hooked_) return;
    //        hooked_->Install(this);
    //    }
    //    ~OnReadScan() {
    //        if (hooked_) hooked_->Uninstall(this);
    //    }

    //    STDMETHOD_(BOOL, IsEnable)() {
    //        return !trap_in_hook_;
    //    };
    //    STDMETHOD(OnRead)(IRXStream* pstm, LPVOID lpBuffer, DWORD cbBytes)
    //    {
    //        if (trap_in_hook_) return S_OK;

    //        out_of_hook_t trap_in_hook(trap_in_hook_);

    //        UINT64 fpos;
    //        RFAILED(pstm->Tell64(fpos));

    //        uint64_t scanned = hys.HasScanned();

    //        if (scanned >= fpos)
    //            return S_OK;

    //        uint64_t delta = fpos - scanned;
    //        if (delta <= cbBytes) {
    //            const uint8_t* data = (const uint8_t*)lpBuffer;
    //            data += cbBytes - delta;
    //            RFAILED(ScanBlock(data, delta));
    //        }
    //        else
    //        {
    //            restore_fpos_t restore_pos(*pstm, fpos);
    //            RFAILED(ScanFileFromTo(pstm, fpos - cbBytes));
    //            RFAILED(ScanBlock(lpBuffer, cbBytes));
    //        }
    //        return S_OK;
    //    }
    //    STDMETHOD(OnWrite)(IRXStream*, LPCVOID, DWORD) {
    //        return S_OK;
    //    }
    //    STDMETHOD(OnFinsh)(IRXStream*) {
    //        if (trap_in_hook_) return S_OK;
    //        out_of_hook_t trap_in_hook(trap_in_hook_);
    //        return ScanFinsh();
    //    }
    //protected:
    //    HRESULT ScanFinsh()
    //    {
    //        hys.Finsh();
    //        return hys.LastUserDecision() == FC_CONTINUE ? S_OK : E_ACCESSDENIED;
    //    }
    //    HRESULT ScanBlock(LPVOID lpBuffer, DWORD cbBytes)
    //    {
    //        hys.Scan(lpBuffer, cbBytes);
    //        return hys.LastUserDecision() == FC_CONTINUE ? S_OK : E_ACCESSDENIED;
    //    }
    //    HRESULT ScanFileFromTo(IRXStream* pstm, uint64_t start_pos, uint64_t end_pos)
    //    {
    //        const DWORD FRAME_SIZE = 0x400;
    //        std::vector<uint8_t> buffer(FRAME_SIZE);
    //        while (start_pos < end_pos)
    //        {
    //            uint64_t rest = end_pos - start_pos;
    //            DWORD dwRead = 0;
    //            DWORD toRead = FRAME_SIZE < rest ? FRAME_SIZE : (DWORD)rest;
    //            RFAILED(pstm->Readp(start_pos, buffer.data(), toRead, &dwRead));
    //            if (!dwRead) break;
    //            start_pos += dwRead;
    //            RFAILED(ScanBlock(buffer.data(), dwRead));
    //        }
    //        return S_OK;
    //    }
    //public:
    //    HRESULT ScanRest()
    //    {
    //        if (!hooked_) return S_FALSE;
    //        com_ptr<IRXStream> stream(hooked_);
    //        UINT64 file_size = 0;
    //        RFAILED(stream->GetSize64(file_size));
    //        size_t scanned = hys.HasScanned();
    //        RFAILED(ScanFileFromTo(stream, scanned, file_size));
    //        return ScanFinsh();
    //    }
    //};

    //class FileWriter
    //{
    //protected:
    //    com_ptr<IScanContext>   context_;
    //    com_ptr<IBlockScanMode> block_scan_;
    //    com_ptr<IRXStream>      file_;
    //public:
    //    FileWriter(IRuntime* runtime, IRXStream* pstm)
    //    {
    //        CreateInstance<HybirdScanContext>(context_, runtime);
    //        block_scan_ = context_;
    //        file_ = pstm;
    //    }
    //    ~FileWriter()
    //    {

    //    }
    //    FLOW_CTRL Write(LPCVOID lpBuffer, DWORD cbBytes)
    //    {
    //        if (block_scan_) {
    //            RBREAK(block_scan_->Scan(lpBuffer, cbBytes));
    //        }
    //        DWORD dwWritten = 0;
    //        RFAILED_(file_->Write(lpBuffer, cbBytes, &dwWritten), FC_BREAK);
    //        return dwWritten == cbBytes ? FC_CONTINUE : FC_BREAK;
    //    }
    //    FLOW_CTRL Commit()
    //    {
    //        if (block_scan_) {
    //            RBREAK(block_scan_->Finsh());
    //        }
    //        com_ptr<IFileScanMode> file_scan_(context_);
    //        if (file_scan_) {
    //            RFAILED_(file_scan_->SetStream(file_), FC_BREAK);
    //            return context_->Collect();
    //        }
    //        return FC_CONTINUE;;
    //    }
    //};

    template < BOOL bCreateIfNotExist>
    static HRESULT GetStreamFormatList(com_ptr<IREFormatList> & rList, IUnknown * pstm)
    {
        com_ptr<IREProperty> prop(pstm);
        if (!prop) return E_NOINTERFACE;

        com_ptr<IREFormatList> tmp;

        const PROPVARIANT* pvar = 0;
        HRESULT hr = prop->GetProperty(PID_File_FormatList, &pvar);
#if (CPP_COMPILER==CC_GCC) && (TARGET_OS==OS_WINDOWS)
        if (FAILED(hr) || (!pvar) || (pvar->vt != VT_UNKNOWN) || !(tmp = (IUnknown*)(pvar->pszVal)))
#else
        if (FAILED(hr) || (!pvar) || (pvar->vt != VT_UNKNOWN) || !(tmp = pvar->punkVal))
#endif
        {
            if (!bCreateIfNotExist) return E_FAIL;

            RFAILED(CreateInstance<FormatList>(tmp, 0));

            PROPVARIANT var; var.vt = VT_UNKNOWN;
#if (CPP_COMPILER==CC_GCC) && (TARGET_OS==OS_WINDOWS)
            var.pszVal = (LPSTR)tmp.m_p;
#else
            var.punkVal = tmp;
#endif

            RFAILED(prop->SetProperty(PID_File_FormatList, &var));
        }
        rList = tmp;
        return S_OK;
    }

};


