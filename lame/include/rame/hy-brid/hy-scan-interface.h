#pragma once


#if _WIN32
#define HYAPI_EXPORT    __declspec(dllexport)
#else
#define HYAPI_EXPORT
#endif
#define HYAPI           extern
#define MAYBE_NULL      

typedef UINT32  FFMTID;

namespace hy
{
    struct ObjectArray
    {
        IUnknown* lpObject;
        SIZE_T      nCount;
    };

    class IEngineContext2013 : public IUnknown
    {
    public:
        virtual HRESULT CreateInstance(TCLSIDREF rclsid, IUnknown* outer, REFIID riid, void** ppv, IUnknown* rot) PURE;
        virtual HRESULT CreateInstanceNCT(TCLSIDREF rcatid, DWORD idcls, IUnknown* outer, REFIID riid, void** ppv, IUnknown* rot) PURE;
    };

    ///////////////////////////////////////////////////////////////////////////////////

    struct ScanResult
    {
        UINT32  AreaId;
        UINT32  SignId;
        LPCSTR  NamePtr; MAYBE_NULL
        UINT32  NameLen; MAYBE_NULL
    };

    class IRuntime;

    enum FLOW_CTRL
    {
        FC_ABORT = -2,
        FC_BREAK = -1,
        FC_CONTINUE = 0,
    };

    static inline bool FLOW_CONTINUE(FLOW_CTRL fcc) {
        return fcc == FC_CONTINUE;
    }

#define RBREAK(fc)                  { FLOW_CTRL _fcc_ = fc; if( _fcc_ ) return _fcc_; };

    enum EV_ID
    {
        ON_NOP,                     // Nothing
        ON_FILE_IGNORED,            // 文件被忽略
        ON_FMT_ADD,
        ON_FMT_DEL,
        ON_FMT_NOMORE,
    };

    struct Event
    {
        EV_ID ev_id;
    };

    typedef const Event *     EventPtr;

    struct FormatEvent : Event {
        FFMTID fmtid;
        static const FormatEvent* from(EventPtr p) { return (const FormatEvent*)p; };
    };

    typedef const FormatEvent * FormatEventPtr;

    class TScanContext;

    // {7CD3A4E4-4789-48A4-85F7-97B412F38885}
    //RE_DEFINE_IID(IFileWriter, "{7CD3A4E4-4789-48A4-85F7-97B412F38885}",
    //    0x7cd3a4e4, 0x4789, 0x48a4, 0x85, 0xf7, 0x97, 0xb4, 0x12, 0xf3, 0x88, 0x85);

    class TUserCallback
    {
    public:
        virtual FLOW_CTRL OnWorking(TScanContext*) PURE;
        virtual FLOW_CTRL OnAlarm(TScanContext*, const ScanResult* lpResult) PURE;
        virtual FLOW_CTRL OnFileCollected(IUnknown* lpFileOrContext) PURE;
    };

    class IUserCallback : public IUnknown, public TUserCallback
    {
    public:
        RE_DECLARE_IID;
    };

    // {01C7A7D6-AB1A-42A5-97BD-B06E9F9CB714}
    RE_DEFINE_IID(IUserCallback, "{01C7A7D6-AB1A-42A5-97BD-B06E9F9CB714}",
        0x1c7a7d6, 0xab1a, 0x42a5, 0x97, 0xbd, 0xb0, 0x6e, 0x9f, 0x9c, 0xb7, 0x14);

    class TScanContext
    {
    public:
        //
        // 获取运行时
        //
        virtual const IRuntime* GetRuntime() PURE;
        //
        // 获取当前文件上下文绑定的文件
        //
        virtual IRXStream* MAYBE_NULL Stream() PURE;
        //
        // 设置查毒结果
        //
        virtual FLOW_CTRL SetAlarm(const ScanResult* lpResult) PURE;
        //
        // 收集自文件
        //
        virtual FLOW_CTRL Collect() PURE;
        //
        // 
        //
        virtual FLOW_CTRL LastUserDecision() PURE;
        //
        // 通知格式变化
        //
        virtual HRESULT emitEvent(EV_ID ev, const Event* info) PURE;
        //
        // 设置需要进行块扫描
        //
        virtual HRESULT EnableBlockScan(BOOL bEnable) PURE;
        //
        //
        //
        virtual SIZE_T  HasScanned() PURE;
        //
        //
        //
        virtual BOOL    MatchFormat( FFMTID fmtid ) PURE;
    };

    typedef TScanContext* TScanContextPtr;

    ////////////////////////////////////////////////////////////////////////////////////

    class IEventReactor : public IUnknown
    {
    public:
        // 失败: 屏蔽
        virtual HRESULT onEvent(TScanContext* ctx, EventPtr info, IEventReactor** updated) PURE;
        RE_DECLARE_IID;
    };

    // {6C3AF0DF-7A93-4FD6-8B50-D150B018CF03}
    RE_DEFINE_IID(IEventReactor, "{6C3AF0DF-7A93-4FD6-8B50-D150B018CF03}",
        0x6c3af0df, 0x7a93, 0x4fd6, 0x8b, 0x50, 0xd1, 0x50, 0xb0, 0x18, 0xcf, 0x3);

    class IFileScan : public IEventReactor
    {
    public:
        // 失败: 屏蔽
        virtual HRESULT Scan(TScanContext* context) PURE;
        RE_DECLARE_IID;
    };
    // {5523C248-DB1B-4049-8FE9-CB5310A2B777}
    RE_DEFINE_IID(IFileScan, "{5523C248-DB1B-4049-8FE9-CB5310A2B777}",
        0x5523c248, 0xdb1b, 0x4049, 0x8f, 0xe9, 0xcb, 0x53, 0x10, 0xa2, 0xb7, 0x77);

    ////////////////////////////////////////////////////////////////////////////////////

    class IBlockScan : public IEventReactor
    {
    public:
        // 失败: 屏蔽
        virtual HRESULT Scan(TScanContext* ctx, LPCVOID data, SIZE_T size) PURE;
        // 失败: 屏蔽
        virtual HRESULT Finsh(TScanContext* ctx) PURE;
        RE_DECLARE_IID;
    };

    // {A6394F1E-D2F2-4BC1-927E-0023F664D346}
    RE_DEFINE_IID(IBlockScan, "{A6394F1E-D2F2-4BC1-927E-0023F664D346}",
        0xa6394f1e, 0xd2f2, 0x4bc1, 0x92, 0x7e, 0x0, 0x23, 0xf6, 0x64, 0xd3, 0x46);

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // 内部实现
    // 

    class IBlockScanMode : public IUnknown
    {
    public:
        virtual FLOW_CTRL Scan(LPCVOID data, SIZE_T size) PURE;
        virtual FLOW_CTRL Finsh() PURE;
        RE_DECLARE_IID;
    };
    // {6F58904B-5CC6-4E6B-8E54-CE38B2CA8BD2}
    RE_DEFINE_IID(IBlockScanMode, "{6F58904B-5CC6-4E6B-8E54-CE38B2CA8BD2}",
        0x6f58904b, 0x5cc6, 0x4e6b, 0x8e, 0x54, 0xce, 0x38, 0xb2, 0xca, 0x8b, 0xd2);

    class IFileScanMode : public IUnknown
    {
    public:
        virtual HRESULT SetStream(IRXStream* pstm) PURE;
        virtual FLOW_CTRL Scan(BOOL bTryUseOnReadScan = TRUE) PURE;
        RE_DECLARE_IID;
    };

    // {56CF6F4A-3CB7-476D-8625-4CD31EA88ACE}
    RE_DEFINE_IID(IFileScanMode, "{56CF6F4A-3CB7-476D-8625-4CD31EA88ACE}",
        0x56cf6f4a, 0x3cb7, 0x476d, 0x86, 0x25, 0x4c, 0xd3, 0x1e, 0xa8, 0x8a, 0xce);

    class IScanContext : public IUnknown, public TScanContext
    {
    public:
        virtual BOOL    IsModeAvaildable(REFIID riid) PURE;
        virtual HRESULT SetUserCallback(TUserCallback* lpCallback) PURE;
        RE_DECLARE_IID;
    };

    RE_DEFINE_IID(IScanContext, "{6C1CD74D-EDC8-40DD-86CE-EF8055C12A2B}",
        0x6c1cd74d, 0xedc8, 0x40dd, 0x86, 0xce, 0xef, 0x80, 0x55, 0xc1, 0x2a, 0x2b);

    ///////////////////////////////////////////////////////////////////////////////////

    //
    // 全局对象，必须是线程安全的!
    //
    enum RT_GLOBAL
    {
        RT_ENG2013_GATE,
        RT_LIB_MANAGER,
        RT_FILE_SYSTEM,
        RT_USER_CALLBACK,
        RT_NAME_DB13,           // IRxNameDB
		RT_ROT,
        RT_OBJ_MAX_ = 10,
    };

    class IRuntime : public IUnknown
    {
    public:

        virtual HRESULT PutGlobal(RT_GLOBAL gid, IUnknown* instance) PURE;

        virtual HRESULT GetGlobal(RT_GLOBAL gid, IUnknown** ppUnknown) const PURE;

        virtual HRESULT RegisterScan(IUnknown* scan, BOOL first) PURE;

        // 
        // query IBlockScan or IFileScan
        //
        virtual HRESULT QueryScanners(REFIID riid, IUnknown*** ppScanners, SIZE_T* lpCount) const PURE;

        RE_DECLARE_IID;

        template < typename _Interface>
        inline HRESULT QueryGlobal(UTIL::com_ptr<_Interface>& obj, RT_GLOBAL gid) const {
            UTIL::com_ptr<IUnknown> unknown;
            RFAILED(GetGlobal(gid, unknown.pp()));
            if (!unknown) return E_FAIL;
            obj = unknown;
            if (!obj) return E_NOINTERFACE;
            return S_OK;
        };
    };

    // {C1EC2C31-7EC0-497F-B8FA-6C96737816DA}
    RE_DEFINE_IID(IRuntime, "{C1EC2C31-7EC0-497F-B8FA-6C96737816DA}",
        0xc1ec2c31, 0x7ec0, 0x497f, 0xb8, 0xfa, 0x6c, 0x96, 0x73, 0x78, 0x16, 0xda);
};

template < typename _Class>
HYAPI_EXPORT HRESULT hyCreateInstance(hy::IRuntime* runtime, REFIID riid, void** ppv);

template <typename CLASS>
HRESULT __hyCreateInstance(hy::IRuntime* rt, REFIID riid, void** ppInstance) {
	CLASS* object = new CLASS();
    if (!object) return E_OUTOFMEMORY;
    HRESULT hr = object->init_class(rt);
	if (FAILED(hr)) {
		delete object;
		return hr;
	}
    return object->QueryInterface(riid, ppInstance);
}

#define HY_EXPORT_CLASS(ClassName)  \
template <>\
HYAPI_EXPORT HRESULT hyCreateInstance<ClassName>(hy::IRuntime* rt, REFIID riid, void** ppv)\
{\
    return __hyCreateInstance<ClassName>(rt, riid, ppv);\
}


namespace hy
{
    ///////////////////////////////////////////////////////////////////////////////////
    //
    // utils functions
    //
    template <typename _Class, typename _Interface>
    HRESULT CreateInstance(UTIL::com_ptr<_Interface>& out, IRuntime* dep) {
        UTIL::com_ptr<_Interface> object;
        HRESULT hr = hyCreateInstance<_Class>(dep, re_uuidof(_Interface), object.vpp());
        if (FAILED(hr)) return hr;
        out = object;
        return S_OK;
    };;
}
