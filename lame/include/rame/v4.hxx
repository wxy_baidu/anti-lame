#ifndef __RAME_INTERFACE_EXPORT__
#define __RAME_INTERFACE_EXPORT__

#ifndef RAME_SDK_API_IMPL

#include "cxx/common.hxx"
#include "cxx/iunknown.hxx"
#include "cxx/utility.hxx"
#include "cxx/propvariant.h"
#include "cxx/xvalue.hxx"



class IRERunningObjectTable : public IUnknown
{
public:
	STDMETHOD(Register)(const GUID& rpid, IUnknown *punk) = 0;
	STDMETHOD(Revoke)(const GUID& rpid) = 0;
	STDMETHOD(IsRunning)(const GUID& rpid) = 0;
	STDMETHOD(GetObject)(const GUID& rpid, IUnknown **ppunk) = 0;
public:
	RE_DECLARE_IID;
};
RE_DEFINE_IID(IRERunningObjectTable, "{F4447B2E-89C7-4180-A2C6-390A7E539065}",
			  0xf4447b2e, 0x89c7, 0x4180, 0xa2, 0xc6, 0x39, 0xa, 0x7e, 0x53, 0x90, 0x65);

class IRXRunTime : public IRERunningObjectTable
{
public:
	STDMETHOD(GetObjectEx)( REFGUID rpid, const GUID& iid, void **ppv) = 0;
	STDMETHOD(Dispose)() = 0;
public:
	RE_DECLARE_IID;
};

RE_DEFINE_IID(IRXRunTime, "{8482CD06-6DFC-479b-949B-848ABCECBE1B}",
			  0x8482cd06, 0x6dfc, 0x479b, 0x94, 0x9b, 0x84, 0x8a, 0xbc, 0xec, 0xbe, 0x1b);

//////////////////////////////////////////////////////////////////////////

/**
* @brief 参数集合操作接口
*
*/
class IRXParameters : public IUnknown {
public:
	/// 设置参数
	STDMETHOD(SetParam)(LPCSTR aParamPath, xv::Value &v) PURE;
	/// 获取参数
	STDMETHOD(GetParam)(LPCSTR aParamPath, xv::Value &v) PURE;
	/// 设置参数字符串（Key=Value）
	STDMETHOD(SetParamString)(LPCSTR aParamString) PURE;
	/// 清除所有值
	STDMETHOD(Clear)() PURE;
	RE_DECLARE_IID;
};
/// {26056B73-D5E5-437B-B4FE-D7F318F527DF}
RE_DEFINE_IID(IRXParameters, "{26056B73-D5E5-437B-B4FE-D7F318F527DF}",
			  0x26056b73, 0xd5e5, 0x437b, 0xb4, 0xfe, 0xd7, 0xf3, 0x18, 0xf5, 0x27, 0xdf);
/**
* @brief 参数集合操作接口2
*
*/
class IRXParameters2 : public IRXParameters {
public:
	/// 移除指定的值
	STDMETHOD(Remove)(LPCSTR aParamPath) PURE;
	RE_DECLARE_IID;
};

////////////////////////////////////////////////////////////////////////////////////////////
#include "cxx/ifileio.hxx"
#include "cxx/ilibup.hxx"

/**
* @brief 文档对象接口
* 文档对象是 文件格式解析 的封装
*/
class IREArchive : public IUnknown {
public:
	/**
	* @brief 打开目标对象
	* @param punk 目标对象(实现了IREStream)
	*/
	STDMETHOD(Open)(IUnknown *punk) PURE;
	/**
	* @brief 关闭
	*/
	STDMETHOD(Close)() PURE;
	/**
	* @brief 获取当前打开的对象
	* @param ppstm 出参,返回当前打开的文件
	*/
	STDMETHOD(GetStreamObject)(IREStream **ppstm) PURE;

public:
	RE_DECLARE_IID;
};
RE_DEFINE_IID(IREArchive, "{F0DE6717-1B00-4b9e-8E45-0212E029457B}", 0xf0de6717,
			  0x1b00, 0x4b9e, 0x8e, 0x45, 0x2, 0x12, 0xe0, 0x29, 0x45, 0x7b);

/**
* @brief 文档对象接口2
* 文档对象是 文件格式解析 的封装
*/
class IRXArchive : public IREArchive {
public:
	/**
	* @brief 按指定格式打开目标对象
	* @param punk 目标对象(实现了IREStream)
	* @param uFmtId 指定的文件格式ID
	*/
	STDMETHOD(Open2)(IUnknown *punk, ULONG uFmtId) PURE;
	/**
	* @brief 获取当前文档对象对应的文件格式ID
	* @param lpFmtId 出参,返回文件格式ID
	*/
	STDMETHOD(GetFormatID)(ULONG *lpFmtId) PURE;

public:
	RE_DECLARE_IID;
};

// {82CA12EF-1A94-4bb0-9959-416C7051FF26}
RE_DEFINE_IID(IRXArchive, "{82CA12EF-1A94-4bb0-9959-416C7051FF26}", 0x82ca12ef,
			  0x1a94, 0x4bb0, 0x99, 0x59, 0x41, 0x6c, 0x70, 0x51, 0xff, 0x26);

////////////////////////////////////////////////////////////////////////////////////////////

#pragma pack(push, 1)

/// 32位病毒记录ID
typedef UINT32 SGID32, *LPSGID32;
typedef UINT8 AREAID;
typedef UINT64 SGID64;

/// 40位病毒记录ID
typedef struct _LONG_SIGID {
	/// 区间ID
	AREAID AreaId;
	/// 记录ID
	SGID32 Id32;
} __attribute__((packed)) SGID40, *LPSGID40;
#pragma pack(pop)

namespace nam 
{
	enum MAIN_CLASS {
		kMalware = 0x10,
		kTrojan,
		kBackdoor,
		kWorm,
		kRootkit,
		kExploit,
		kHackTool,
		kAdware,
		kStealer,
		kSpammer,
		kSpyware,
		kVirus,
		kJoke,
		kJunk,
		kPUA,
		kDownloader,
		kDropper,
		/// 2015-12-21
		kRansom,   // 勒索软件
		kHoax,     //
		kRiskware, // 风险软件
		// 2016-6-7
		kUnwanted,  // 不需要的APP
		kMonetizer, // 套现类
		kDeceptor,  // 蒙骗类, 2017/1/9, AppEsteem

		kMobileBase = 0xC0,
		// Mobile
		//	摘自《移动互联网恶意代码描述规范》
		kPayment = kMobileBase, //	恶意扣费
		kPrivacy,               //	隐私窃取
		kRemote,                //	远程控制
		kSpread,                //	恶意传播
		kExpense,               //	资费消耗
		kSystem,                //	系统破坏
		kFraud,                 //	诱骗欺诈
		kRogue,                 //	流氓行为

		kTypeMax = 0x100,
	};
};


				

/**
* @brief 病毒名/病毒ID操作接口
*
*/
class IRXNameDB : public IUnknown {
public:
	RE_DECLARE_IID;
	/**
	* @brief 设置病毒库对象
	* @param lpMalLib 必须实现
	*/
	STDMETHOD(SetLib)(IUnknown *lpMalLib) PURE;
	/**
	* @brief 32位记录ID转40位记录ID
	* @param SId32 32位记录ID
	* @param lpLongSid 出参,转换完40位记录ID
	*/
	STDMETHOD(Id32To40)(SGID32 SId32, LPSGID40 lpLongSid) PURE;
	/**
	* @brief 32位记录ID转40位记录ID
	* @param SId32 32位记录ID
	* @param lpLongSid 出参,转换完40位记录ID
	*/
	STDMETHOD(Id40To32)(SGID40 SId40, LPSGID32 lpSId32) PURE;
	/**
	* @brief 根据32位记录ID获取病毒名
	* @param uVid 32位记录ID
	* @param lpName 出参,存放病毒名的缓冲区指针
	* @param cbName  存放病毒名的缓冲区大小
	*/
	STDMETHOD(GetNameById32)(SGID32 uVid, LPSTR lpName, SIZE_T cbName) PURE;
	/**
	* @brief 根据40位记录ID获取病毒名
	* @param uVid 40位记录ID
	* @param lpName 出参,存放病毒名的缓冲区指针
	* @param cbName  存放病毒名的缓冲区大小
	*/
	STDMETHOD(GetNameById40)(SGID40 uVid, LPSTR lpName, SIZE_T cbName) PURE;
	/**
	* @brief 从标准病毒名中解析出40位记录ID
	* @param aName 病毒名字符串指针
	* @param lpSId40 出参,解析获得的40位记录ID
	*/
	STDMETHOD(Id40FromName)(LPCSTR aName, LPSGID40 lpSId40) PURE;
	/**
	* @brief 直接构建病毒名(不建议使用）
	*/
	STDMETHOD(BuildNameFree)(LPSTR lpName, SIZE_T cbName, SGID40 uSid40, nam::MAIN_CLASS malClass, UINT32 uFamilyId) PURE;
};
// {214E797B-B94C-44C3-83BF-50C7772524D5}
RE_DEFINE_IID(IRXNameDB, "{214E797B-B94C-44C3-83BF-50C7772524D5}", 0x214e797b,
			  0xb94c, 0x44c3, 0x83, 0xbf, 0x50, 0xc7, 0x77, 0x25, 0x24, 0xd5);


namespace _13 {

	/**
	* @brief 扫描流程控制码
	*
	*/
	enum EFC {
		EFC_CONTINUE, ///< 继续查杀毒流程
		EFC_IGNORE,   ///< 忽略：忽略当前的扫描文件；
		EFC_ABORT,    ///< 终止：终止全部的扫描

		EFC_INTERNAL = 0x80000000, ///< 内部使用
		IEFC_RESCAN,               ///< 重新扫描,用于文件清除之后
	};
	/**
	* @brief 处理方式控制码
	*
	*/
	enum TEFC {
		TEFC_DEFAULT, ///< 默认方式（根据配置）
		TEFC_IGNORE,  ///< 忽略文件
		TEFC_CLEAN, ///< 清除文件（按照记录指定的方式进行处理,包含修复和删除）
		TEFC_REMOVE, ///< 删除文件（直接删除文件）
		TEFC_ABORT,  ///< 不做处理并终止整个过程
	};

	typedef enum FILE_LEAVE_CASE {
		FLC_NORMAL,             ///< 正常扫描完成
		FLC_IGNORED,            ///< 被忽略
		FLC_IGNORED_BY_WL,      ///< 被白名单忽略
		FLC_IGNORED_BY_FILTER,  ///< 被过滤器过滤
		FLC_IGNORED_BY_USER,    ///< 被控制码忽略
		FLC_IGNORED_BY_PARAM,   ///< 被参数忽略
		FLC_BREAK_TREAT_FAILED, ///< 处理失败导致的跳出

	} LEAVECASE;

	typedef enum TREAT_RESULT {
		TR_S_TREAT = 0, ///< 处理成功
		TR_S_IGNORE,    ///< 忽略成功
		TR_E_FAIL = -1, ///< 处理失败
		TR_E_REOPEN = -2, ///< 文件将被处理时，需要按写权限打开，打开失败，则会报此错误
		TR_E_KILL = -3,   ///< 杀毒失败,清理病毒或修复文件的过程失败
		TR_E_DELETE = -4, ///< 文件删除失败
		TR_E_MAX_COUNT = -5,       ///< 超过杀毒最大次数
		TR_E_ROLLBACK = -6,        ///< 回退到父对象失败
		TR_E_POST_TREAT = -7,      ///< 杀毒后处理失败
		TR_E_COMPOUND_UPDATE = -8, ///< 复合文档回写失败
	} TRESULT;


	enum _PROCOPTS {
		PROC_WILL_TREAT = 1,    ///< 检测并清除
		PROC_CC_BOOST = 2,      ///< 开启文件读缓存
		PROC_NO_MAPPING = 4,    ///< 关闭映射模式
		PROC_NO_SEH = 8,        ///< 关闭SEH
		PROC_ALRAM_PASS = 0x10, ///< 报毒后继续检测（其他子引擎）
		PROC_NO_PREREAD = 0x20, ///< 关闭预读模式,否则,被扫描文件会被预读2M
		PROC_LEGACY_FORMATS_MODE = 0x40, ///< 启用兼容10引擎的文件格式模式（会自动追加NORMAL等）
		PROC_NO_COW_STREAM = 0x80, ///< 不使用“写拷贝”流,清除时将直接改写文件
		PROC_FAST_OVER_ARCHIVE = 0x100, ///< 识别到一个病毒就跳出整个包的扫描
	};
	typedef SIZE_T PROCOPT;




	/**
	* @brief 使用者必须要实现的回调
	*
	*/
	class IRXCallback : public IUnknown {
	public:
		RE_DECLARE_IID;

		/**
		* @brief 获取本次扫描的扫描选项,参见 _PROCOPTS
		*/
		STDMETHOD_(PROCOPT, GetProcOptions)() PURE;

		/**
		* @brief 进入一个文件扫描过程
		* @param pstm 目标文件对象
		* @param uNest 嵌套层数
		* @return 流程控制码,参见 @enum EFC
		*/
		STDMETHOD_(EFC, EnterFile)(IRXStream *pstm, SIZE_T uNest) PURE;
		/**
		* @brief 离开当前文件扫描过程
		* @param pstm 目标文件对象
		* @param uNest 嵌套层数
		* @param uCase 结束这个文件扫描的原因,参考@enum LEAVECASE
		*/
		STDMETHOD_(VOID, LeaveFile)(IRXStream *pstm, SIZE_T uNest, LEAVECASE uCase) PURE;

		/**
		* @brief 进入一种格式扫描
		* @param arch 目标文档对象（文档对象）
		*/
		STDMETHOD_(VOID, EnterArchive)(IRXArchive *arch) PURE;
		/**
		* @brief 离开当前格式扫描
		* @param arch 目标文档对象（文档对象）
		* @param trRebuild
		* 当前文档对象重建的结果。一般用于符合文档（压缩包等）的修复时,报告文档的重建结果。
		*/
		STDMETHOD_(VOID, LeaveArchive)(IRXArchive *arch, TRESULT trRebuild) PURE;

		/**
		* @brief 当前被扫描的文件被报毒时回调
		* @param arch 目标文档对象（文件格式对象）
		* @param uSigId 目标文档对象（文件格式对象）
		* @return 处理方式控制码,参见 @enum TEFC
		*/
		STDMETHOD_(TEFC, OnAlarm)(IRXArchive *arch, SGID40 uSigId) PURE;
		/**
		* @brief 当前文件处理后的回调
		* @param arch 目标文档对象（文件格式对象）
		* @param uSigId 目标文档对象（文件格式对象）
		* @param trTreat 上一次处理的结果,参见 @enum TRESULT
		* @return 处理方式控制码,参见 @enum TEFC
		*/
		STDMETHOD_(TEFC, OnTreated)(IRXArchive *arch, SGID40 uSigId, TRESULT trTreat) PURE;

		/**
		* @brief 复杂回调（目前没有使用）
		*/
		STDMETHOD(OnComplexCallback)(xv::Value &inVar, xv::Value &outVar) PURE;

		/**
		* @brief 询问正在使用的病毒库文件是否存在更新的回调
		* @param aLibFileName 当前使用的病毒库文件
		* @param uCurrent 当前病毒库的内部版本号（发布号）
		* @param lpLastVer
		* 用于返回新的内部版本号。当与uCurrent不同时,会发生病毒库重加载。
		*/
		STDMETHOD(QueryLibFileVersion)(LPCSTR aLibFileName, UINT32 uCurrent, UINT32 *lpLastVer) PURE;

		/**
		* @brief 当前文件处理后的回调
		* 返回 E_NOTIMPL,将继续调用 IRXCallback::OnTreated
		* @param arch 目标文档对象（文件格式对象）
		* @param uSigId 目标文档对象（文件格式对象）
		* @param rHowTreat 处理方式控制码,参见 @enum TEFC.
		* 为引用，输入值为上一次的处理方式,函数返回时,需要填写为下一次的处理方式(处理成功除外).
		* @param trTreat 上一次处理的结果,参见 @enum TRESULT
		* @param uTreatCount 当前文件已经被处理的次数。
		*/
		STDMETHOD(OnTreatedEx)(IRXArchive *arch, SGID40 uSigId, TEFC &rHowTreat, TRESULT trTreat, SIZE_T uTreatCount) PURE;
	};

	/// {1B23B5B8-DC1F-4D9B-84F1-428EF620C72D}
	RE_DEFINE_IID(IRXCallback, "{1B23B5B8-DC1F-4D9B-84F1-428EF620C72D}", 0x1b23b5b8,
		0xdc1f, 0x4d9b, 0x84, 0xf1, 0x42, 0x8e, 0xf6, 0x20, 0xc7, 0x2d);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	typedef struct _DETECTR
	{
		nam::MAIN_CLASS	Class;
		SGID40		SigId;
	} DETECTR, *LPDETECTR;

	enum XDR_INDEX
	{
		XDR_VID32					= 0,	// lpData = &UINT32;	
		XDR_VID40_SGID40,					// lpData = &SGID40;
		XDR_MALWCLASS_8,					// lpData = &UINT32;
		XDR_NGINNAME_CPTR,					// lpData = &LPCSTR;	15  max
		XDR_MALWNAME_CPTR,					// lpData = &LPCSTR;	255 max
		XDR_SIGTAG_CPTR,					// lpData = &LPCSTR;	16  max

		XDR_INDEX_MAX = 32,
	};

	/**
	* @brief 复杂检测结果获取接口
	* 
	*/
	struct IExtDetectionResult 
	{
		STDMETHOD_(LPDETECTR, ResultV1Ptr)() PURE;
		STDMETHOD_(LPCSTR,	EngineName)()	PURE;
		STDMETHOD_(LPCSTR,	MalwareName)() PURE;
		STDMETHOD_(nam::MAIN_CLASS,	MalwareClass)() PURE;
		STDMETHOD_(UINT32,	MalwareId32)()	PURE;
		STDMETHOD_(SGID40,	MalwareId40)()	PURE;
		STDMETHOD_(LPCSTR,	SignatureTag)(SIZE_T * lpSize = NULL)	PURE;
		// return DataSize, <=0 is Failed;
		STDMETHOD_(LONG, GetValue)( XDR_INDEX uIndex, PVOID lpDataRecv ) PURE;
	};

	/**
	* @brief 增强型回调接口
	* 当实现该接口时,将抑制 IRXCallback::OnAlarm 和 IRXCallback::OnTreated(Ex)
	* 的调用.
	*
	*/
	class IRXCallbackX : public IRXCallback {
	public:
		RE_DECLARE_IID;
		/**
		* @brief 当前文件报毒时的回调(会取代IRXCallback::OnAlarm)
		* @param arch 目标文档对象（文件格式对象）
		* @param result 报毒信息
		* @return 处理方式控制码,参见 @enum TEFC
		*/
		STDMETHOD_(TEFC, OnAlarm)(IRXArchive *arch, IExtDetectionResult *result) PURE;
		/**
		* @brief 当前文件报毒时的回调(会取代IRXCallback::OnTreated/OnTreatedEx)
		* @param arch 目标文档对象（文件格式对象）
		* @param result 报毒信息
		* @param trTreat 病毒处理的结果,参见 @enum TRESULT
		* @return 处理方式控制码,参见 @enum TEFC
		*/
		STDMETHOD_(TEFC, OnTreated)(IRXArchive *arch, IExtDetectionResult *result, TRESULT trTreat) PURE;
	};

	/// {BA70DD5D-D617-4B3D-830C-A725D1179FDA}
	RE_DEFINE_IID(IRXCallbackX, "{BA70DD5D-D617-4B3D-830C-A725D1179FDA}",
		0xba70dd5d, 0xd617, 0x4b3d, 0x83, 0xc, 0xa7, 0x25, 0xd1, 0x17,
		0x9f, 0xda);

	/**
	* @brief 引擎使用接口
	* 通过该接口初始化和调用引擎扫描。
	*
	*/
	class IRxFrontEnd : public IUnknown {
	public:
		RE_DECLARE_IID;
		/**
		* @brief 初始化引擎
		* @param aLibPath 病毒库路径
		* @param lpParam 引擎参数对象
		* @return HRESULT
		*/
		STDMETHOD(Init)(LPCSTR aLibPath, IRXParameters *lpParam) PURE;

		/**
		* @brief 处理一个文件
		* @param lpStream 被扫描的目标对象
		* @param lpCallback 回调对象,参考 IRXCallback
		* @return HRESULT
		*/
		/// if >, see FILE_LEAVE_CASE
		STDMETHOD(Process)(IUnknown *lpStream, IRXCallback *lpCallback) PURE;

		/**
		* @brief 清理资源的接口
		*/
		STDMETHOD_(VOID, Clear)() PURE;

		/**
		* @brief 初始化引擎2
		* @param opLibLoader 传入病毒库对象
		* @param lpParam 引擎参数对象
		* @return HRESULT
		*/
		STDMETHOD(Init2)(IUnknown *opLibLoader, IRXParameters *lpParam) PURE;
	};

	/// {98764591-5B98-4367-8647-5DA83BDB6549}
	RE_DEFINE_IID(IRxFrontEnd, "{98764591-5B98-4367-8647-5DA83BDB6549}", 0x98764591,
		0x5b98, 0x4367, 0x86, 0x47, 0x5d, 0xa8, 0x3b, 0xdb, 0x65, 0x49);

	///////////////////////////////////////////////////////////////////////////////////////////

	/// 是否扫描复合文档，整型
	static LPCSTR PARAM_PROCESS_COMPOUND = "scan-compound";
	/// 要处理的最大复合文档文件大小，按比特计算，整型。
	static LPCSTR PARAM_MAX_COMPOUND_SIZE = "max-compound-size";
	/// 引擎扫描时的最大嵌套深度,整型
	static LPCSTR PARAM_MAX_NEST_DEPTH = "max-nest-depth";
	/// 一个文件的最大处理次数,整型
	static LPCSTR PARAM_MAX_RETREAT_COUNT = "max-treat-times";
	/// 是否使用精确格式模式,整型
	static LPCSTR PARAM_PRECISE_FORMATS = "precise-format";
	/// 是否添加NORMAL格式,整型
	static LPCSTR PARAM_NORMAL_FORMAT = "normal";
	///
	static LPCSTR PARAM_FILE_MAX_COUNT = "file-max-count";

};

#else

#define RAMEAPI				DLLEXPORT

#endif

#define IRXEngineAgent		_13::IRxFrontEnd
#define IRXScanCallback		_13::IRXCallbackX
#define IRXScanCallback0	_13::IRXCallback

namespace v4 
{
	typedef enum EXPORTED_CLASS_ID 
	{
		ECID_FileSystem,
		ECID_Parameter,
		ECID_FrontEnd,
		ECID_HandleFile,
		ECID_VirtualFile,
		ECID_LibUpdater,
		ECID_LibUpdaterW,
		ECID_NameDB
	} ECID;

	struct EngineInitParam 
	{
		IUnknown *						SignatureFile;
		IRXParameters *					Parameters;
		IRXFileSystem *					FileSystem OPTIONAL;
	};

	struct SignatureFile 
    {
		IRXRunTime*						Runtime;
		IUnknown*						Signature;
	};

	struct EngineInstance
	{
		IRXRunTime *					Runtime;
		IRXFileSystem*					FileSystem;
		IRXEngineAgent*					Agent;
	};

	HRESULT RAMEAPI RameCreateRuntime( OUT IRXRunTime ** ppRuntime );

	HRESULT RAMEAPI RameCreateInstance( IN IRXRunTime * runtime OPTIONAL, IN ECID clsid, IN REFIID riid, OUT void ** ppv );

	template <typename I, ECID classid>
	static inline UTIL::com_ptr<I> RameNewInstance( IN IRXRunTime * runtime OPTIONAL ) {
		UTIL::com_ptr<I> ptr;
		RameCreateInstance(runtime, classid, re_uuidof(I), ptr.vpp());
		return ptr;
	}

	HRESULT RAMEAPI RameSignatureOpen( IN IRXParameters * lpParam, IN REFCXSTRP rFileName, IN LPCSTR aObNamePrefix, OUT SignatureFile * lpLibFile );
	HRESULT RAMEAPI RameSignatureClose( IN SignatureFile * lpLibFile );

	HRESULT RAMEAPI RameEngineCreate( IN EngineInitParam * params, IN OUT EngineInstance * engine );
	HRESULT RAMEAPI RameEngineScan( IN EngineInstance * engine, IUnknown * pstm, IRXScanCallback * lpCallback );
	HRESULT RAMEAPI RameEngineClose( IN EngineInstance * engine );

}; // namespace v4


#endif