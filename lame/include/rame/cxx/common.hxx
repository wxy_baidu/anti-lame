#ifndef __RAME_SDK_COMMON__
#define __RAME_SDK_COMMON__

#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif

#pragma warning (disable: 4996)

#include "platform.hxx"

#if (TARGET_OS == OS_WINDOWS )

#	include "windef.win32.h"

#else

#	include "win32.error.h"

#	if (TARGET_OS == OS_NATIVE )
#		include "windef.native.h"
#	else
#		include "windef.posix.h"
#	endif

#endif

#ifdef RAME_SDK_API_IMPL
#define RAMEAPI		DLLEXPORT
#else
#define RAMEAPI		DLLIMPORT
#endif

#endif
