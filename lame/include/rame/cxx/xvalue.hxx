#ifndef __SDK_XVALUE_DEF__
#define __SDK_XVALUE_DEF__

#ifndef RAME_SDK_API_IMPL

#include "common.hxx"
#include "iunknown.hxx"
#include "refable.hxx"
#include "blob.hxx"

namespace xv
{
#pragma pack(push, 1)
	struct type_byte
	{
		UINT8		type:5;
		UINT8		bsiz:3;
	} __attribute__((packed));
#pragma pack(pop)

#define TP(t,s)		((t)|((s)<<5))

	enum value_size
	{
		_8			=	0,
		_16			=	1,
		_32			=	2,
		_64			=	3,
		_128		=	4,
	};

	enum value_type
	{
		dNull		=	0x0,
		dInt		=	0x1,
		dReal		=	0x2,
		dLocalStr	=	0x3,
		dUtf8Str	=	0x4,
		dUniStr		=	0x5,
		dBuffer		=	0x6,
		dArray		=	0x7,
		dObject		=	0x8,
		dUnknown	=	0x9,
		dBoolean	=	0x10,
	};

	typedef value_type	ValueType;

	struct Value;

	struct Output
	{
		virtual bool write( const void * p, size_t cb ) = 0;
	};

	struct vComplex : IRefable
	{
		virtual size_t	length() = 0;
		virtual bool	clear() = 0;
		virtual bool	clone( vComplex ** ppo ) = 0;
		virtual bool	cloneTo( vComplex * po ) = 0;
	};

	struct vArray : vComplex
	{
		virtual xv::Value&	ref( size_t i ) = 0;
		virtual bool 		set( size_t i, const xv::Value & item ) = 0;
		virtual bool 		get( size_t i, xv::Value & item ) = 0;
		virtual bool 		erase( size_t i ) = 0;
		typedef bool 		( * _enum_cb_ptr )( size_t name, xv::Value & item, bool last, void * context );
		virtual bool 		enum_it( _enum_cb_ptr fcb, void * context ) = 0;
		virtual bool 		append( xv::Value & item ) = 0;
		virtual bool 		pop( xv::Value & item ) = 0;
		virtual bool		resize( size_t c ) = 0;
		virtual bool		concat( xv::Value & item ) = 0;
	};

	// max key length is 255
	struct vObject : vComplex
	{
		virtual xv::Value&	ref( const char * name ) = 0;
		virtual bool 		set( const char * name, const xv::Value & item ) = 0;
		virtual bool 		get( const char * name, xv::Value & item ) = 0;
		virtual bool 		erase( const char * name ) = 0;
		typedef bool 		( * _enum_cb_ptr )( const char * name, xv::Value & item, bool last, void * context );
		virtual bool 		enum_it( _enum_cb_ptr fcb, void * context ) = 0;
	};

	struct vBuffer : vComplex
	{
		virtual bool			assign( const void * p, size_t cb ) = 0;
		virtual UINT8 *		    base() = 0;
		virtual bool			resize( UINT32 size ) = 0;
	};

	struct vStringA : vComplex
	{
		virtual bool			assign( const char * str ) = 0;
		virtual const char *	c_str() = 0;
	};

	struct vStringW : vComplex
	{
		virtual bool			assign( const wchar_t * str ) = 0;
		virtual const wchar_t *	c_str() = 0;
	};

	typedef RX_BLOB		chunk_t;

	enum { MAX_KEY_LEN = 255 };

#pragma pack(push, 4)
	struct Value
	{
		type_byte		_type;
		union
		{
			INT64		integer;
			double		real;
			vComplex *	complex;
			IUnknown*	punk;
			bool		vbool;
			INT64       anyval;
		};
	} __attribute__((packed)) ;
#pragma pack(pop)


};

#endif

namespace xv 
{

	//////////////////////////////////////////////////////////////////////////
	//
	//
	//	Basic APIs
	//
	//
	//////////////////////////////////////////////////////////////////////////

	typedef Value		XVALUE;
	typedef value_type	XVTYPE;
	typedef value_size	XVSIZE;

	BOOL RAMEAPI ValInit( XVALUE* );
	BOOL RAMEAPI ValClear( XVALUE* );
	BOOL RAMEAPI ValSetType( XVALUE* , XVTYPE );
	BOOL RAMEAPI ValSetType2( XVALUE* , XVTYPE, XVSIZE);
	BOOL RAMEAPI ValClone( const XVALUE*, XVALUE* );
	BOOL RAMEAPI ValSet( RS_CONST_DEF XVALUE*, XVALUE* );
	BOOL RAMEAPI ValCheckType( RS_CONST_DEF XVALUE*, XVTYPE );
	BOOL RAMEAPI ValCheckType2( const XVALUE*, XVTYPE, XVSIZE);
	BOOL RAMEAPI ValCheckTypes(const XVALUE*, UINT32 );
	BOOL RAMEAPI ValLoad( XVALUE*, const void *, size_t );
	BOOL RAMEAPI ValSave( XVALUE*, xv::Output * );
	BOOL RAMEAPI ValToJson( xv::Value* vp, xv::Output * wo );
	BOOL RAMEAPI ValFromJson( xv::Value* vp, LPCSTR json_string, SIZE_T );
};

#endif
