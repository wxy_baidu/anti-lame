#ifndef __RX_STD_COM_H__
#define __RX_STD_COM_H__

//////////////////////////////////////////////////////////////////////////

inline bool operator < (const GUID& left, const GUID& right)
{
	return memcmp(&left, &right, sizeof(GUID)) < 0;
}

#ifdef PLATFORM_TYPE_POSIX
inline bool operator == (const GUID& left, const GUID& right)
{
	return !memcmp(&left, &right, sizeof(GUID));
}

inline bool operator != (const GUID& left, const GUID& right)
{
	return !(left == right);
}
#endif

//////////////////////////////////////////////////////////////////////////

#if TARGET_OS != OS_WINDOWS

#if (defined RS_BIG_ENDIAN) || (__GNUC__ < 4) || (__GNUC__ == 4 && __GNUC_MINOR__ < 1) || (__GNUC__ == 4 && __GNUC_MINOR__ == 1 && __GNUC_PATCHLEVEL__ < 2)

#warning ("not support sync series")

static FORCEINLINE LONG WINAPI InterlockedIncrement( LONG volatile *dest )
{
	pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_lock(&lock);
	LONG r = ++(*dest);
	pthread_mutex_unlock(&lock);
	return r;
}
static FORCEINLINE LONG WINAPI InterlockedDecrement( LONG volatile *dest )
{
	pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_lock(&lock);
	LONG r = --(*dest);
	pthread_mutex_unlock(&lock);
	return r;
}

#else

static FORCEINLINE LONG WINAPI InterlockedExchangeAdd( LONG volatile *dest, LONG incr )
{
	return __sync_fetch_and_add( dest, incr );
}
static FORCEINLINE LONG WINAPI InterlockedIncrement( LONG volatile *dest )
{
	return __sync_add_and_fetch( dest, 1 );
}
static FORCEINLINE LONG WINAPI InterlockedDecrement( LONG volatile *dest )
{
	return __sync_sub_and_fetch( dest, 1 );
}

#endif //defined RS_BIG_ENDIAN ...

#endif //TARGET_OS != OS_WINDOWS

//////////////////////////////////////////////////////////////////////////


class CUnknownImp
{
public:
	ULONG m_RefCount;
	CUnknownImp(): m_RefCount(0) {}
};


#define QIBEGIN	\
	STDMETHOD(QueryInterface)(REFGUID riid, void **ppv) {

#define QIUNKNOWN	\
	if(re_uuidof(IUnknown) == riid) { *ppv = static_cast<IUnknown*>(this); AddRef(); return S_OK; }

#define QIUNKNOWN_(icast)	\
	if(re_uuidof(IUnknown) == riid) { *ppv = static_cast<IUnknown*>(static_cast<icast*>(this)); AddRef(); return S_OK; }

#define QIENTRY(iface)	\
	if(re_uuidof(iface) == riid) { *ppv = static_cast<iface*>(this); AddRef(); return S_OK; }

#define QIENTRY_(iface, icast)	\
	if(re_uuidof(iface) == riid) { *ppv = static_cast<iface*>(static_cast<icast*>(this)); AddRef(); return S_OK; }

#define QIEND \
	return E_NOINTERFACE; }


#define ADDREF		STDMETHOD_(ULONG, AddRef)()	\
					{ InterlockedIncrement( (PLONG)&m_RefCount ); return m_RefCount; }

#define RELEASE		STDMETHOD_(ULONG, Release )() {	\
						LONG lRet = InterlockedDecrement( (PLONG)&m_RefCount );\
						if( lRet == 0 ) delete this;\
						return lRet;\
					}

#define UNKNOWN_IMP_SPEC(entrys) \
	QIBEGIN  QIUNKNOWN  entrys  QIEND  ADDREF  RELEASE

#define UNKNOWN_IMP_SPEC_(icast, entrys) \
	QIBEGIN  QIUNKNOWN_(icast)  entrys  QIEND  ADDREF  RELEASE

#define UNKNOWN_IMP0() \
	UNKNOWN_IMP_SPEC( ; )
#define UNKNOWN_IMP1(i1) \
	UNKNOWN_IMP_SPEC( QIENTRY(i1) )
#define UNKNOWN_IMP2(i1, i2) \
	UNKNOWN_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) )
#define UNKNOWN_IMP3(i1, i2, i3) \
	UNKNOWN_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) )
#define UNKNOWN_IMP4(i1, i2, i3, i4) \
	UNKNOWN_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) )
#define UNKNOWN_IMP5(i1, i2, i3, i4, i5) \
	UNKNOWN_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) )
#define UNKNOWN_IMP6(i1, i2, i3, i4, i5, i6) \
	UNKNOWN_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) )
#define UNKNOWN_IMP7(i1, i2, i3, i4, i5, i6, i7) \
	UNKNOWN_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) QIENTRY(i7) )


#define UNKNOWN_IMP1_(i1) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) )
#define UNKNOWN_IMP2_(i1, i2) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) )
#define UNKNOWN_IMP3_(i1, i2, i3) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) )
#define UNKNOWN_IMP4_(i1, i2, i3, i4) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) )
#define UNKNOWN_IMP5_(i1, i2, i3, i4, i5) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) )
#define UNKNOWN_IMP6_(i1, i2, i3, i4, i5, i6) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) )
#define UNKNOWN_IMP7_(i1, i2, i3, i4, i5, i6, i7) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) QIENTRY(i7) )
#define UNKNOWN_IMP8_(i1, i2, i3, i4, i5, i6, i7, i8) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) QIENTRY(i7)  QIENTRY(i8) )
#define UNKNOWN_IMP9_(i1, i2, i3, i4, i5, i6, i7, i8, i9 ) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) QIENTRY(i7)  QIENTRY(i8) QIENTRY(i9) )
#define UNKNOWN_IMP10_(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10 ) \
	UNKNOWN_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) QIENTRY(i7)  QIENTRY(i8) QIENTRY(i9) QIENTRY(i10) )
//////////////////////////////////////////////////////////////////////////


class IUnknown_Nondelegate
{
public:
	STDMETHOD(QueryInterface_Nondelegate)(REFGUID riid, void **ppv) = 0;
	STDMETHOD_(ULONG, AddRef_Nondelegate)() = 0;
	STDMETHOD_(ULONG, Release_Nondelegate)() = 0;
};


class CUnknownImp_Inner
{
public:
	ULONG m_RefCount;
	CUnknownImp_Inner(): m_RefCount(0), m_punkOuter(0) {}
public:
	IUnknown *m_punkOuter;
	HRESULT init_class_inner(IUnknown *punkOuter)
	{
		m_punkOuter = punkOuter;
		return S_OK;
	}
};


#define QIBEGIN_NONDELEGATE	\
	STDMETHOD(QueryInterface_Nondelegate)(REFGUID riid, void **ppv) {

#define QIUNKNOWN_NONDELEGATE	\
	if(re_uuidof(IUnknown) == riid) { *ppv = static_cast<IUnknown_Nondelegate*>(this); AddRef_Nondelegate(); return S_OK; }

#define QIUNKNOWN_NONDELEGATE_(icast)	\
	if(re_uuidof(IUnknown) == riid) { *ppv = static_cast<IUnknown_Nondelegate*>(static_cast<icast*>(this)); AddRef_Nondelegate(); return S_OK; }

#define ADDREF_NONDELEGATE	\
	STDMETHOD_(ULONG, AddRef_Nondelegate)()	\
	{ InterlockedIncrement( (PLONG)&m_RefCount ); return m_RefCount; }

#define RELEASE_NONDELEGATE	\
	STDMETHOD_(ULONG, Release_Nondelegate)()	\
	{ LONG lRet = InterlockedDecrement( (PLONG)&m_RefCount ); if( lRet == 0 ) delete this; return lRet; }

#define QI_INNER	\
	STDMETHOD(QueryInterface)(REFGUID riid, void **ppv)	\
	{ if(this->m_punkOuter) { return this->m_punkOuter->QueryInterface(riid, ppv); } else { return QueryInterface_Nondelegate(riid, ppv); } }

#define ADDREF_INNER	\
	STDMETHOD_(ULONG, AddRef)()	\
	{ if(this->m_punkOuter) { return this->m_punkOuter->AddRef(); } else { return AddRef_Nondelegate(); } }

#define RELEASE_INNER	\
	STDMETHOD_(ULONG, Release)()	\
	{ if(this->m_punkOuter) { return this->m_punkOuter->Release(); } else { return Release_Nondelegate(); } }

#define UNKNOWN_INNER_IMP_SPEC(entrys) \
	QIBEGIN_NONDELEGATE  QIUNKNOWN_NONDELEGATE  entrys  QIEND  ADDREF_NONDELEGATE  RELEASE_NONDELEGATE  QI_INNER  ADDREF_INNER  RELEASE_INNER

#define UNKNOWN_INNER_IMP_SPEC_(icast, entrys) \
	QIBEGIN_NONDELEGATE  QIUNKNOWN_NONDELEGATE_(icast)  entrys  QIEND  ADDREF_NONDELEGATE  RELEASE_NONDELEGATE  QI_INNER  ADDREF_INNER  RELEASE_INNER

#define UNKNOWN_INNER_IMP1(i) \
	UNKNOWN_INNER_IMP_SPEC( QIENTRY(i) )
#define UNKNOWN_INNER_IMP2(i1, i2) \
	UNKNOWN_INNER_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) )
#define UNKNOWN_INNER_IMP3(i1, i2, i3) \
	UNKNOWN_INNER_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) )
#define UNKNOWN_INNER_IMP4(i1, i2, i3, i4) \
	UNKNOWN_INNER_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) )
#define UNKNOWN_INNER_IMP5(i1, i2, i3, i4, i5) \
	UNKNOWN_INNER_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) )
#define UNKNOWN_INNER_IMP6(i1, i2, i3, i4, i5, i6) \
	UNKNOWN_INNER_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) )
#define UNKNOWN_INNER_IMP7(i1, i2, i3, i4, i5, i6, i7) \
	UNKNOWN_INNER_IMP_SPEC( QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) QIENTRY(i7) )

#define UNKNOWN_INNER_IMP2_(i1, i2) \
	UNKNOWN_INNER_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) )
#define UNKNOWN_INNER_IMP3_(i1, i2, i3) \
	UNKNOWN_INNER_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) )
#define UNKNOWN_INNER_IMP4_(i1, i2, i3, i4) \
	UNKNOWN_INNER_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) )
#define UNKNOWN_INNER_IMP5_(i1, i2, i3, i4, i5) \
	UNKNOWN_INNER_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) )
#define UNKNOWN_INNER_IMP6_(i1, i2, i3, i4, i5, i6) \
	UNKNOWN_INNER_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) )
#define UNKNOWN_INNER_IMP7_(i1, i2, i3, i4, i5, i6, i7) \
	UNKNOWN_INNER_IMP_SPEC_(i1, QIENTRY(i1) QIENTRY(i2) QIENTRY(i3) QIENTRY(i4) QIENTRY(i5) QIENTRY(i6) QIENTRY(i7) )





//////////////////////////////////////////////////////////////////////////


#ifndef GUID_NULL
DEFINE_GUID(GUID_NULL,
			0x00000000, 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
#endif

#define		DEFAULT_INIT_CLASS()	HRESULT init_class( IUnknown * , IUnknown * ) { return S_OK;}

#endif // duplicate inclusion protection
