#ifndef __LAME_V2_H__
#define __LAME_V2_H__

#include "sdk.types.h"


// ansi interface
typedef LSCT  (LAMESTDCALLTYPE *lame_scan_enter_file)(const char* file_name , uint32_t depth , void* usr_data);
typedef void  (LAMESTDCALLTYPE *lame_scan_leave_file)(const char* file_name , uint32_t depth ,void* usr_data , LXLVT l);
typedef LSCT  (LAMESTDCALLTYPE *lame_scan_alarm)(const char* file_name , lame_scan_result* info,void* usr_data);

typedef struct lame_scan_feedback_
{
	lame_scan_enter_file enter_file;
	lame_scan_leave_file leave_file;
	lame_scan_alarm		 alarm;
}lame_scan_feedback;


// unicode interface
typedef LSCT  (LAMESTDCALLTYPE *lame_scan_enter_file_w)(const wchar_t* file_name , uint32_t depth , void* usr_data);
typedef void  (LAMESTDCALLTYPE *lame_scan_leave_file_w)(const wchar_t* file_name , uint32_t depth ,void* usr_data , LXLVT l);
typedef LSCT  (LAMESTDCALLTYPE *lame_scan_alarm_w)(const wchar_t* file_name , lame_scan_result* info,void* usr_data);

typedef struct lame_scan_feedback_w_
{
	lame_scan_enter_file_w enter_file;
	lame_scan_leave_file_w leave_file;
	lame_scan_alarm_w		 alarm;
}lame_scan_feedback_w;





EXTERN_C void*	lame_open_vdb(const char* vlibf);
EXTERN_C void	lame_close_vdb(void* vdb);

EXTERN_C void*	lame_create(void* vdb);
EXTERN_C void	lame_destroy(void* lame);
EXTERN_C void*	lame_fork(void* lame);

EXTERN_C long	lame_param_set(void* lame , const char*  param);


EXTERN_C long	lame_init(void* lame);
EXTERN_C long	lame_scan_file(void* lame , const char* fname , lame_scan_result* pResult) ;
EXTERN_C long	lame_scan_file_w(void* lame , const wchar_t* fname , lame_scan_result* pResult) ;
EXTERN_C long	lame_scan_mem(void* lame , uint8_t* data , uint32_t size , lame_scan_result* pResult);


EXTERN_C long	lame_scan_file_with_callback(void* lame , const char* fname , lame_scan_feedback* cbs , void* user_data) ;
EXTERN_C long	lame_scan_file_with_callback_w(void* lame , const wchar_t* fname , lame_scan_feedback_w* cbs , void* user_data) ;
EXTERN_C long	lame_scan_mem_with_callback(void* lame , uint8_t* data , uint32_t size , lame_scan_feedback* cbs , void* user_data);

EXTERN_C long	lame_get_version(lame_info* info);
EXTERN_C long	lame_get_licence_info(rx_licence_info* info);
EXTERN_C long	lame_get_version_by_cfg(char* vdbv, int vdb_len, const char* vlcfgpath);
EXTERN_C long	lame_get_version_ex(const char* vlibf, lame_info* info);
EXTERN_C long	lame_get_licence_info_ex(const char* licence, rx_licence_info* info);

EXTERN_C long	lame_vfs_param_set(long default_available_memory, long default_resident_memory, long max_available_memory);

////////////////////////////////////////////////////////////////////////////////////
typedef long (*clame_fetch_callback)(void* user_data , size_t index , const void* packet , size_t size);

//cloud
EXTERN_C void	clame_mem_free(void* p);
EXTERN_C long	clame_cloud_signatue_file(const char* json_user_info , const char** fnames , const uint32_t count, const uint8_t** results ,  uint32_t* rsize) ;
EXTERN_C long	clame_cloud_signatue_file_w(const char* json_user_info , const wchar_t** fnames , const uint32_t count , const uint8_t** results ,  uint32_t* rsize);


EXTERN_C long	clame_fetch_batch(uint32_t flags , const char** fnames , size_t count , clame_fetch_callback pcb , void* user_data);
EXTERN_C long	clame_fetch_batch_w(uint32_t flags , const wchar_t** fnames , size_t count , clame_fetch_callback pcb , void* user_data);
////////////////////////////////////////////////////////////////////////////////////


typedef long (LAMESTDCALLTYPE *lame_vlibup_download)(void* user_data, const char* src_url);
typedef long (LAMESTDCALLTYPE *lame_vlibup_download_w)(void* user_data, const wchar_t* src_url);

EXTERN_C void*	lame_vlibup_create();
EXTERN_C long	lame_vlibup_getcfg(const char* local_xml, lame_vlibup_download pfn_download, void* user_data);
EXTERN_C long	lame_vlibup_init(void* upper, const char* vlib_home, const char* local_cfg, lame_vlibup_download pfn_download, void* user_data);
EXTERN_C long	lame_vlibup_init_ex(void* upper, const char* vlib_home, const char* local_cfg, const char* local_licence, lame_vlibup_download pfn_download, void* user_data);
EXTERN_C long	lame_vlibup_update(void* upper, const char* want_home);
EXTERN_C void	lame_vlibup_free(void* upper);

EXTERN_C void*	lame_vlibup_create_w();
EXTERN_C long	lame_vlibup_getcfg_w(const wchar_t* local_xml, lame_vlibup_download_w pfn_download, void* user_data);
EXTERN_C long	lame_vlibup_init_w(void* upper, const wchar_t* vlib_home, const wchar_t* local_cfg, lame_vlibup_download_w pfn_download, void* user_data);
EXTERN_C long	lame_vlibup_init_ex_w(void* upper, const wchar_t* vlib_home, const wchar_t* local_cfg, const wchar_t* local_licence, lame_vlibup_download_w pfn_download, void* user_data);
EXTERN_C long	lame_vlibup_update_w(void* upper, const wchar_t* want_home);
EXTERN_C void	lame_vlibup_free_w(void* upper);


typedef long (LAMESTDCALLTYPE *lame_vhomeup_download)(void* user_data, const char* src_url, int need_download);

EXTERN_C void*	lame_vhomeup_create();
EXTERN_C long	lame_vhomeup_update(void* upper, const char* local_cfg, const char* want_home, lame_vhomeup_download pfn_download, void* user_data);
EXTERN_C long	lame_vhomeup_getcfg(const char* local_xml, lame_vhomeup_download pfn_download, void* user_data);
EXTERN_C void	lame_vhomeup_free(void* upper);


#endif