#ifndef __LAME_V3_H__
#define __LAME_V3_H__

#include "sdk.types.h"
#include <stdint.h>

#define	STDCALL		LAMESTDCALLTYPE
typedef rx_mk_t lam_mcls_t;
typedef lame_scan_result lam_scan_result_t;
typedef lame_info lam_version_t;
typedef rx_licence_info lam_lisence_t;
typedef const char * mbsptr_t;
typedef const wchar_t * wcsptr_t;

typedef uint32_t leave_t;
typedef void *  lam_ptr_t;


// sdk.types.h
typedef struct lam_view_ {
	const void * data;
	size_t size;
} lam_view_t;

#pragma  pack(1)
typedef struct enter_file_info_t_
{
	uint32_t info_size;		//扩展当前结构时使用
    uint32_t scan_depth;    //扫描深度，即包的层数
    mbsptr_t disk_file_path; //物理文件名
	mbsptr_t embed_file_path;//内嵌文件名
}enter_file_info_t;

typedef struct leave_file_info_t_
{
	uint32_t info_size;		//扩展当前结构时使用
    uint32_t scan_depth;	//扫描深度，即包的层数
    mbsptr_t disk_file_path;     //物理文件名
	mbsptr_t embed_file_path;    //内嵌文件名
	LXLVT leave_case;			 //扫描中断原因
	rx_trc_t treat;
}leave_file_info_t;


typedef struct alarm_info_t_
{
	uint32_t    info_size;	//扩展当前结构时使用
	mbsptr_t    disk_file_path;	//物理文件名
	mbsptr_t    embed_file_path; //内嵌文件名
	lam_scan_result_t * result;	 //病毒信息
}alarm_info_t;


typedef struct cam_ret_ {
	char 		nature;						// _.r 
    uint32_t 	vid_32;						// _.v
    uint8_t  	area_id;					// _.a
    uint32_t 	rule_id;					// _.i
    lam_mcls_t 	malw_class;					// _.m
	char		malw_name[VIRUS_LENGTH];	// _.n
	char		hit_tag[64];				// _.n
} cam_ret_t;

typedef struct cam_query_info_t_
{
	uint32_t    info_size;		//扩展当前结构时使用
    mbsptr_t    disk_file_path; //物理文件名
	mbsptr_t    embed_file_path;  //内嵌文件名
	mbsptr_t    cam_summary;	 //可上云查询的hash 
	// 
    cam_ret_t  	cam_result;      //云结果信息，需调用者设置
} cam_query_info_t;


#pragma  pack()

/**
 * @brief  : 引擎扫描文件(物理文件、内嵌文件)前回调函数
 * 
 * @param info     : 文件信息(包括物理文件名、内嵌文件名等信息)
 * @param usr_data : 用户自己设置的用户数据
 * @return LSCT    : 可用于控制引擎是否继续扫描
 */
typedef LSCT  (STDCALL *on_enter_file_t)(enter_file_info_t* info, void* usr_data);

/**
 * @brief  : 引擎扫描(物理文件、内嵌文件)完成后回到函数
 * 
 * @param info     : 文件信息(包括物理文件名、内嵌文件名等信息)
 * @param usr_data : 用户自己设置的用户数据
 * @return void    : 无
 */
typedef void  (STDCALL *on_leave_file_t)(leave_file_info_t* info, void* usr_data);

/**
 * @brief  : 引擎报毒回调函数
 * 
 * @param info    : 病毒相关信息(包括物理文件名、内嵌文件名、病毒信息等信息)
 * @param usr_data : 用户自己设置的用户数据
 * @return LSCT   : 可用于控制引擎是否继续扫描
 */
typedef LSCT  (STDCALL *on_alarm_t)(alarm_info_t* info , void* usr_data);

/**
 * @brief  : 云查回调函数，用于将本地引擎和云引擎整合到一个流程中，具体的云查询需要用户自己实现。
 *           查询成功后，需要用户设置cam_result结构
 * 
 * @param info     : 云hash信息(包括物理文件名、内嵌文件名、json格式的云hash)
 * @param usr_data : 用户自己设置的用户数据
 * @return long    : 成功：>=0  失败: < 0 
 */
typedef long  (STDCALL *on_cam_query_t)(cam_query_info_t* info , void* usr_data);

/**
 * @brief  : 询问密码回调函数，主要用于压缩包
 * 
 * @param pwd      : 密码
 * @param usr_data : 用户自己设置的用户数据
 * @return long    : 成功：>=0  失败: < 0 
 */
typedef long  (STDCALL *on_password_t)(const char * pwd, void* usr_data);


typedef struct lam_callbacks_
{
	on_enter_file_t   	enter_file;
	on_leave_file_t   	leave_file;
	on_alarm_t			alarm;
	on_cam_query_t		cloud_query;
	on_password_t		query_password;
} lam_callbacks_t;

typedef void * lam_engn_t;
typedef void * lam_mwdb_t;
typedef void * lam_file_t;
typedef void * lame_xhandle_t;


#if defined(__cplusplus)

typedef struct lam_xfile_face
{
	 virtual ~lam_xfile_face(){}
	 virtual long read(LPVOID lpBuffer, DWORD cbBytesToRead, LPDWORD lpBytesRead) = 0;
	 virtual long write(LPCVOID lpBuffer, DWORD cbBytesToWrite,LPDWORD lpBytesWritten) = 0;
	 virtual long flush() = 0;
	 virtual long seek(LONG lDistanceToMove,PLONG lpDistanceToMoveHigh,DWORD dwMoveMethod) = 0;
	 virtual long tell(LPDWORD lpOffsetLow,LPDWORD lpOffsetHigh) = 0;
	 virtual long get_size(LPDWORD lpSizeLow,LPDWORD lpSizeHigh) = 0;
	 virtual long set_size(DWORD dwSizeLow, LPDWORD lpSizeHigh) = 0;
	 virtual long remove() = 0;
	 virtual const char* get_name() = 0;
	 virtual const wchar_t* get_name_w() = 0;
} lam_xfile_face;


#else


typedef long (STDCALL *lam_file_read)(lame_xhandle_t handle , LPVOID lpBuffer, DWORD cbBytesToRead, LPDWORD lpBytesRead);
typedef long (STDCALL *lam_file_write)(lame_xhandle_t handle , LPCVOID lpBuffer, DWORD cbBytesToWrite,LPDWORD lpBytesWritten);
typedef long (STDCALL *lam_file_flush)(lame_xhandle_t handle);
typedef long (STDCALL *lam_file_seek)(lame_xhandle_t handle , LONG lDistanceToMove,PLONG lpDistanceToMoveHigh,DWORD dwMoveMethod);
typedef long (STDCALL *lam_file_tell)(lame_xhandle_t handle , LPDWORD lpOffsetLow,LPDWORD lpOffsetHigh);
typedef long (STDCALL *lam_file_get_size)(lame_xhandle_t handle , LPDWORD lpSizeLow,LPDWORD lpSizeHigh);
typedef long (STDCALL *lam_file_set_size)(lame_xhandle_t handle , DWORD dwSizeLow, LPDWORD lpSizeHigh);
typedef long (STDCALL *lam_file_remove)(lame_xhandle_t handle);
typedef const char* (STDCALL *lam_file_get_name)(lame_xhandle_t handle);
typedef const wchar_t* (STDCALL *lam_file_get_name_w)(lame_xhandle_t handle);

typedef struct lam_xfile_t_
{
	lame_xhandle_t handle;
	lam_file_read read;
	lam_file_write write;
	lam_file_flush flush;
	lam_file_seek seek;
	lam_file_tell tell;
	lam_file_get_size get_size;
	lam_file_set_size set_size;
	lam_file_remove remove;
	lam_file_get_name get_name;
	lam_file_get_name_w get_name_w;

}lam_xfile_t , *lam_xfile_handle;

#endif


///////////////////////////////////引擎参数////////////////////////////////////////////////

/**
 * @brief: 用于设置引擎是否扫描未识别的文件格式, 1:不扫描 , 0:扫描
 */
static const char* kpPreciseFormat = "precise-format";

/**
 * @brief : 是否扫描复合文件, 1:不扫描 , 0:扫描
 * 
 */
static const char* kpScanComponment = "scan-compound";

/**
 * @brief : 用于指定杀毒次数,默认500
 * 
 */
static const char* kpMaxTreatTimes = "max-treat-times";

/**
 * @brief: 最大扫描复合文档的大小,单位：byte ，默认不限制大小
 * 
 */
static const char* kpMaxCompoundCize = "max-compound-size";

/**
 * @brief : 压缩脱壳类文件最大扫描深度, 默认50
 * 
 */
static const char* kpMaxNestDepth = "max-nest-depth";

/**
 * @brief : 杀毒模式下，使用能快速退出当前扫描文件, 1:快速模式，0：普通模式，默认
 * 
 */
static const char* kpFast = "fast";

/**
 * @brief : 共享内存模式加载病毒库, 1:共享模式(默认)，1:内存模式
 * 
 */
static const char* kpShareMemMode = "share-mem-mode";

/**
 * @brief : 杀毒模式
 * 
 */
static const char* kpKill = "kill";

/**
 * @brief 云引擎提取的摘要数据
 * 
 */
static const char* kpCamUseMask = "cam-use-mask";
enum {
	CAM_PATH		= 'P',	// 
	CAM_MD5 		= 'M',
	CAM_SHA1 		= 'S',
	CAM_SHA256 		= 'H',
	/////////////////////
	CAM_TJ64 		= 't',	// 
	CAM_SPOTS		= 'p',	// TFE for PE/ELF Malware
	CAM_MMC			= 'm',	// ASG for Text Malware
	CAM_RDMK		= 'k',
	CAM_RDMV		= 'v',	// RDMV include RMDK
};

/////////////////////////// 参数设置事例 ///////////////////////////
/**
 * 
 * lam_engine_set_param(engine ,"precise-format=1")
 * lam_engine_set_param(engine ,"kill")
 * lam_engine_set_param(engine ,"cam-use-mask=PMSHtp")
 */

////////////////////////////////////////////////////////////////////////////////////

EXTERN_C void		lam_file_attach( int fd );

/**
 * @brief : 打开文件，此对象只能被lame使用
 * 
 * @param wcs         : file_path是否为uncoide编码的标记，ture：uicode
 * @param file_path   : 带全路径的文件名
 * @return lam_file_t : lame内部文件对象句柄
 */
EXTERN_C lam_file_t	lam_file_open( bool wcs, const void * file_path );

/**
 * @brief : 将内存块封装成lame可直接使用的文件对象
 * 
 * @param view        : 内存块
 * @return lam_file_t : lame内部文件对象句柄
 */
EXTERN_C lam_file_t lam_file_wrap( lam_view_t view );

/**
 * @brief :关闭lame文件对象
 * 
 * @param file  : 文件对象，此文件必须由lam_file_open或lam_file_wrap创建
 * @return void : 
 */
EXTERN_C void		lam_file_close( lam_file_t file );


/**
 * @brief : 申请内存
 * 
 * @param size       : 内存大小
 * @return lam_ptr_t : 成功:内存指针,失败: 0 
 */
EXTERN_C lam_ptr_t	lam_mem_new( size_t size );
/**
 * @brief :释放内存
 * 
 * @param ptr   ：由lam_mem_new申请的内存指针
 * @return void : 无 
 */
EXTERN_C void	    lam_mem_free( lam_ptr_t ptr );
/**
 * @brief : 申请内存并复制内存中的数据
 * 
 * @param data       : 需要复制的内存指针
 * @param len        ：需要复制的内存大小
 * @return lam_ptr_t ：新申请的内存指针，此内存需要用lam_mem_free释放
 */
EXTERN_C lam_ptr_t	lam_mem_dup( const void * data, size_t len );


/**
 * @brief :加载病毒特征库
 * 
 * @param vlibf : 病毒特征库全路径
 * @return lam_mwdb_t : lame内部使用的病毒特征库句柄,此病毒库句柄可以被多份引擎实例使用
 */
EXTERN_C lam_mwdb_t lam_virdb_open(mbsptr_t vlibf);

/**
 * @brief : 关闭病毒特征库
 * 
 * @param vdb : 用lam_virdb_open打开的病毒库特征库句柄
 * @return  void
 */
EXTERN_C void       lam_virdb_close(lam_mwdb_t vdb);


/**
 * @brief : 创建反病毒引擎
 * 
 * @param vdb : 使用lam_virdb_open打开的病毒库句柄
 * @return lam_engn_t : 创建成功，返回反病毒引擎实例；失败,返回 0
 */
EXTERN_C lam_engn_t lam_engine_new(lam_mwdb_t vdb);
/**
 * @brief : 设置引擎参数
 * 
 * @param engine : 通过lam_engine_new创建的反病毒引擎实例句柄
 * @param param  : 引擎参数
 * @return long  : 成功：>=0  失败: < 0 
 */
EXTERN_C long       lam_engine_set_param(lam_engn_t engine, mbsptr_t param);
/**
 * @brief : 根据用户设置的参数，初始化反病毒引擎
 * 
 * @param engine : 反病毒引擎实例的句柄
 * @return long  : 成功：>=0  失败: < 0 
 */
EXTERN_C long       lam_engine_init(lam_engn_t engine);

/**
 * @brief : 根据用户设置的参数，初始化带回调的反病毒引擎
 * 
 * @param engine 	: 通过lam_engine_new创建的反病毒引擎实例句柄
 * @param callbacks : 扫描病毒时需要回调函数
 * @param usr_data  : 传给回调函数的用户数据，lame对此数据不做任何处理
 * @return long 	: 成功：>=0  失败: < 0
 */
EXTERN_C long       lam_engine_init_2(lam_engn_t engine, lam_callbacks_t * callbacks, void * usr_data );

/**
 * @brief : 根据初始化好的引擎实例fork一个新的引擎
 * 
 * @param engine 		: 引擎实例
 * @return lam_engn_t 	：成功：新引擎实例，失败:0
 */
EXTERN_C lam_engn_t lam_engine_fork(lam_engn_t engine);

/**
 * @brief : 病毒扫描
 * 
 * @param engine : 初始化后的引擎实例,
 * 				   1> 如果引擎是通过lam_engine_init初始化, 扫描到病毒就会终止扫描流程
 * 				   2> 如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程
 * @param file   : 调用lam_file_open打开的文件lame内部文件句柄
 * @param result : 病毒结果信息
 * @return long  : 扫描到病毒：>=0  未扫描到: < 0
 */
EXTERN_C long       lam_engine_scan(lam_engn_t engine, lam_file_t file, lam_scan_result_t* result);

/**
 * @brief :病毒扫描
 * 
 * @param engine 	: 初始化后的引擎实例,
 * 				   	   1> 如果引擎是通过lam_engine_init初始化, 扫描到病毒就会终止扫描流程
 * 				       2> 如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程
 * @param file_path : 需要扫描的的文件名
 * @param result    : 病毒结果信息
 * @return long     : 扫描到病毒：>=0  未扫描到: < 0
 */
EXTERN_C long       lam_engine_scan_a(lam_engn_t engine, mbsptr_t file_path, lam_scan_result_t* result);

/**
 * @brief :病毒扫描
 * 
 * @param engine    : 初始化后的引擎实例,
 * 				   	   1> 如果引擎是通过lam_engine_init初始化, 扫描到病毒就会终止扫描流程
 * 				       2> 如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程
 * @param file_path : 需要扫描的的unicode文件名 
 * @param result    : 病毒结果信息
 * @return long     : 扫描到病毒：>=0  未扫描到: < 0
 */
EXTERN_C long       lam_engine_scan_w(lam_engn_t engine, wcsptr_t file_path, lam_scan_result_t* result) ;

EXTERN_C long       lam_engine_scan_x(lam_engn_t engine, lam_xfile_face* file, lam_scan_result_t* result);
/**
 * @brief  :病毒扫描
 * 
 * @param engine     : 初始化后的引擎实例,
 * 				   	   1> 如果引擎是通过lam_engine_init初始化, 扫描到病毒就会终止扫描流程
 * 				       2> 如果引擎是通过lam_engine_init_2初始化的,会通过回调函数上报病毒信息并继续扫描流程
 * @param file_view  : 需要扫描的文件内存
 * @param result 	 : 病毒结果新
 * @return long      : 扫描到病毒：>=0  未扫描到: < 0
 */
EXTERN_C long       lam_engine_scan_m(lam_engn_t engine, lam_view_t file_view, lam_scan_result_t* result);

/**
 * @brief :销毁引擎实例
 * 
 * @param engine : 使用lam_engine_new创建的引擎实例
 * @return void  : 无
 */
EXTERN_C void       lam_engine_close(lam_engn_t engine);



/**
 * @brief : 获取lame的版本信息
 * 
 * @param info  : 需要填充的lam_version_t对象
 * @return long : 成功：>=0  失败: < 0
 */
EXTERN_C long       lam_version(lam_version_t* info);


/**
 * @brief : 获取lame的版本信息
 * 
 * @param path  : 病毒特征库全路径
 * @param info  : 需要填充的lam_version_t对象
 * @return long : 成功：>=0  失败: < 0
 */
EXTERN_C long       lam_version2(mbsptr_t path , lam_version_t* info);


/**
 * @brief : 获取licence信息
 * 
 * @param info  : lam_lisence_t
 * @return long : 成功：>=0  失败: < 0
 */
EXTERN_C long       lam_licence_get(lam_lisence_t* info);

enum {
	CAM_USE_TJ64 	= 1<<1,
	CAM_USE_MD5 	= 1<<2,
	CAM_USE_SHA1 	= 1<<3,
	CAM_USE_SHA256 	= 1<<4,
	CAM_USE_TFE		= 1<<5,
	CAM_USE_ASG		= 1<<6,
	CAM_USE_RDMK	= 1<<7,
	CAM_USE_RDMV	= 1<<8,	// RDMV include RMDK
	CAM_USE_PATH	= 1<<9,
};

/**
 * @brief : 提取云hash
 * 
 * @param file 			: 使用lam_file_open打开的lame内部文件句柄
 * @param summary_mask  ：用于控制提取某些Hash，例：CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1
 * @param json 			：json格式的云hash，需要使用lam_mem_free释放
 * @return long 		: 成功：>=0  失败: < 0
 */
EXTERN_C long	   	cam_summary_fetch( lam_file_t file, uint32_t summary_mask, lam_ptr_t * json );

/**
 * @brief : 提取云hash
 * 
 * @param file_path    : 需要提取的文件名
 * @param summary_mask ：用于控制提取某些Hash，例：CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1
 * @param json         ：json格式的云hash，需要使用lam_mem_free释放
 * @return long 	   : 成功：>=0  失败: < 0
 */
EXTERN_C long	   	cam_summary_fetch_a( mbsptr_t file_path, uint32_t summary_mask, lam_ptr_t * json );

/**
 * @brief : 提取云hash
 * 
 * @param file_path    : 需要提取的文件名
 * @param summary_mask ：用于控制提取某些Hash，例：CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1 
 * @param json         ：json格式的云hash，需要使用lam_mem_free释放
 * @return long 	   : 成功：>=0  失败: < 0
 */
EXTERN_C long	   	cam_summary_fetch_w( wcsptr_t file_path, uint32_t summary_mask, lam_ptr_t * json );
/**
 * @brief  : 提取云hash
 * 
 * @param file_view     : 需要提取的文件名
 * @param summary_mask  ：用于控制提取某些Hash，例：CAM_USE_TJ64|CAM_USE_MD5|CAM_USE_SHA1
 * @param json    		：json格式的云hash，需要使用lam_mem_free释放
 * @return long		    : 成功：>=0  失败: < 0
 */
EXTERN_C long	   	cam_summary_fetch_m( lam_view_t file_view, uint32_t summary_mask, lam_ptr_t * json );

/**
 * @brief   : 将云结果转成lame引擎的结果
 * 
 * @param cresult ： 云结果
 * @param lresult :  lame结果
 * @param vdb 	  :  使用lam_virdb_open打开的病毒库句柄,可选
 * @return long	  :  成功：>=0  失败: < 0
 */
EXTERN_C long	   	cam_result_convert( cam_ret_t * cresult, lam_scan_result_t * lresult,  lam_mwdb_t vdb );

#endif