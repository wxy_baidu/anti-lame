#ifndef __SDK_TYPES_H__
#define __SDK_TYPES_H__

#define _OPTIONAL_

#ifdef __cplusplus
#define EXTERN_C    extern "C"
#else
#define EXTERN_C    extern
typedef char bool;
#define true	1
#define false	0
#endif


#define LAMECALLTYPE      __cdecl
#define LAMESTDCALLTYPE      __stdcall

#ifndef _WIN32
#define __cdecl
#define __stdcall
#endif


#define LAME_SUCCEEDED(hr)   (((long)(hr)) >= 0)
#define LAME_FAILED(hr)      (((long)(hr)) < 0)

#include <stdint.h>


#define VIRUS_LENGTH 256
#define ENGID_LENGTH 32
#define HITAG_LENGTH 32
#define CLASS_NAME_LEN	0x10
#define ENGINE_VERSION_LEN	32
#define VIRUSDB_VERSIN_LEN	32

typedef enum rx_mk_t {
	//////内部使用///////
	mkTrait			= 0,
	mkHidden		= 1,
	mkHiddenFU		= 2,
	mkComplier		= 3,
	mkPacker		= 4,
	mkFormat		= 5,				// 文件格式
	mkAppType		= 6,
	mkTrusted		= 9,
	////////////////////////////

	mkMalware		= 0x10,				//恶意软件			40

	mkTrojan,							//木马				42
	mkBackdoor,							//后门远控			76
	mkWorm,								//蠕虫				72
	mkRootkit,							//恶意驱动			74
	mkExploit,							//漏洞利用			78
	mkHackTool,							//黑客工具			22
	mkAdware,							//恶意广告			26
	mkStealer,							//盗号木马			68
	mkSpammer,							//垃圾邮件发送器	66
	mkSpyware,							//间谍软件			70
	mkVirus,							//感染型病毒		100
	mkJoke,								//玩笑程序			4
	mkJunk,								//病毒僵尸			2
	mkPUA,								//不需要的应用		24
	mkDownloader,						//下载器			62
	mkDropper,							//释放器			64
	/// 2015-12-21
	mkRansom,							//勒索软件			80
	mkHoax,								//诈骗软件			60
	mkRiskware,							//灰色软件			28
	// 2016-6-7
	mkUnwanted,							//不需要的应用		24
	mkMonetizer,						//套现程序			50

	mkMobileBase		= 0xC0,
	// Mobile
	//	摘自《移动互联网恶意代码描述规范》
	mkPayment		= mkMobileBase,		//	恶意扣费		58
	mkPrivacy,							//	隐私窃取		56
	mkRemote,							//	远程控制		54
	mkSpread,							//	恶意传播		52
	mkExpense,							//	资费消耗		50
	mkSystem,							//	系统破坏		48
	mkFraud,							//	诱骗欺诈		46
	mkRogue,							//	流氓行为		44


	mkAttention		= 0xFE,				// 注意!			6
	mkTypeMax		= 0x100,
} rx_mk_t;



enum{
	COMPOUND_SIZE = 0,
	MEST_DEPTH,
	TREAT_TIMES,
	PRECISE_FORMAT,
	KILL,
	MALWARE_PATH,
};


typedef enum rx_scan_opt_t {
	OPT_NO_COW		= 1,
	OPT_TREAT		= 2,
} rx_scan_opt_t;

typedef enum rx_treat_result_code_t {
	TREAT_FAIL_FIX = -1,
	TREAT_OK_IGNORE	   = 0,
	TREAT_OK_DELETED = 1,
	TREAT_OK_FIXED = 2,
	
} rx_trc_t;

typedef struct rx_licence_info
{
	char			Version[16];
	char			Owner[128];
	char			Date[64];
	char			Authm[32];
	char			Data[2048];
} rx_licence_info;



typedef struct lame_scan_result
{
	rx_mk_t			mklass;
	char			kclass[CLASS_NAME_LEN];
	char			kclass_desc_a[CLASS_NAME_LEN];
	wchar_t			kclass_desc_w[CLASS_NAME_LEN];
	char			engid[ENGID_LENGTH];
	char			vname[VIRUS_LENGTH];
	uint32_t		vid32;
	uint64_t		vid40;
	char			hitag[HITAG_LENGTH];
	rx_trc_t		treat;
}lame_scan_result;


typedef enum LSMETHOD_ 
{
	LAME_SEEK_SET = 0,
	LMAE_SEEK_CUT = 1,
	LMAE_SEEK_TAIL =2
}LSMETHOD;


typedef enum LSCT_
{
	LSCT_CONTINUE = 0x01,
	LSCT_ABORT
}LSCT;


typedef enum LXCT_
{
	LCT_CONTINUE = 0x01,
	LCT_DO_EXTRACT,
	LCT_ABORT,

}LXCT;


typedef enum LXLVT_
{
	LXLVT_NORMAL = 0x01,
	LXLVT_ABORT  = 0x02,
	LXLVT_ERROR  = 0x03
}LXLVT;

typedef struct lame_info_
{
	char  engv[ENGINE_VERSION_LEN+1];
	char  vdbv[VIRUSDB_VERSIN_LEN+1];
}lame_info;



enum 
{
	FCLS_PROGRAM	= 0x00000010,	// DOSCOM/PE//ELF/JAVA
	FCLS_DOCUMENT   = 0x00000020,	// PDF/DOC/DOCX/rtf
	FCLS_PACKAGE	= 0x00000040,	// Zip/7z/...
	FCLS_MMEDIA		= 0x00000080,	// Image|Moive|Audio
	FCLS_SCRIPT		= 0x00000100,	// All script
	FCLS_EMAIL		= 0x00000200,	// E-Mail
	FCLS_DISKIMG	= 0x00000400,	// Disk in file, VHD, VMDK, ...
	FCLS_BOOT		= 0x00000800,	// Virtual Disk
};


enum F_METHOD
{
	F_MD5	= 0x00000001,
	F_SHA1	= 0x00000002,
	F_SHA2	= 0x00000004,
	F_SHA4	= 0x00000008,
	F_TFE	= 0x00000010,
	F_RDM	= 0x00000020,
	F_ASG	= 0x00000040,
	F_TJC64 = 0x00000080,
	F_AI	= 0x00000100,
};


#endif